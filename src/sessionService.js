//Closure
(function(ce) {
	'use strict';

	ce.session = function(){
		//loginData for databinding with login form
		this.loginData = {
			username : null,
			password : null
		};

		this.globalCreator = false;
		this.publicCreator = false;
		this.privateCreator = false;
		this.isCreator = false;

		this.userLoged = null;
		this.isLogged = false;
	};

	ce.session.prototype.login = function(userLoged, selectedClient){
		this.userLoged = userLoged;
		this.isLogged = true;
		if (selectedClient){
			var infoSecurity = ce.security.isUserCreatorForClient(this.userLoged, selectedClient);
			this.globalCreator = infoSecurity.globalCreator;
			this.privateCreator = infoSecurity.privateCreator;
			this.isCreator = infoSecurity.isCreator;
			this.publicCreator = infoSecurity.publicCreator;
		}
	};
	ce.session.prototype.logout = function(){
		document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		this.resetValues();
	};

	ce.session.prototype.resetValues = function(){
		this.loginData = {
			username : null,
			password : null
		};

		this.userLoged = null;
		this.isLogged = false;

		this.globalCreator = false;
		this.publicCreator = false;
		this.privateCreator = false;
		this.isCreator = false;
	};
})(window.ce);
