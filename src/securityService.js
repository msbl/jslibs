//Closure
(function(ce) {
	'use strict';

	ce.security = ce.security || {};

	ce.security.isUserCreatorForClient = function(user, selectedClient){
		var secureInfo = {
			isCreator:false, 
			globalCreator:false, 
			privateCreator:false, 
			publicCreator:false, 
			isAdmin:false,
			loyalty: false,
			loyaltyEmployee: false,
			loyaltyCustomerService:false
		};

		var isCreatorAssignSelectedClient = false;
		//check if thce.securityere is any user logged
		if (user && user.roles && user.clients){
			var roles = user.roles.split('--');
			var clientsAssigned = user.clients.split('--');
			var isPublicCreator = false;
			var isPrivateCreator = false;
			var isAdmin = false;

			//chech the user roles
			angular.forEach(roles, function(rol, i){
				if(rol === 'creator'){
					isPublicCreator = true;
					secureInfo.publicCreator = true;
				}else if (rol === 'privateCreator'){
					isPrivateCreator = true;
					secureInfo.privateCreator = true;
				} else if (rol === 'admin'){
					isAdmin = true;
				} else if (rol === 'loyalty'){
					secureInfo.loyalty = true;
				} else if (rol === 'loyaltyEmployee'){
					secureInfo.loyaltyEmployee = true;
				} else if (rol === 'loyaltyCustomerService'){
					secureInfo.loyaltyCustomerService = true;
				}
			});
			secureInfo.isAdmin = isAdmin;

			if (isPublicCreator || isPrivateCreator){
				//check it's assign to client selected, 0 is global
				angular.forEach(clientsAssigned, function(client, i){
					if(client === '0' && isPublicCreator){
						secureInfo.isCreator = true;
						secureInfo.isAdmin = true;
						secureInfo.globalCreator = true;
					}else if (client === ''+selectedClient.id){
						secureInfo.isCreator = true;
					}
				});
			}
		}
		return secureInfo;
	};


	ce.security.userCanModify = function(isGlobalCreator, publicCreator, privateCreator, userLoged, contentCE){
		if (!contentCE || !userLoged){
			return false;
		}
		//check if it's logged a global creator
		if (isGlobalCreator){
			return true;
		}

		//if the route if public, only content creator can modify element
		if (contentCE.type === 'ROUTE' && !contentCE.route.publicRoute){
			return (contentCE.idUser === userLoged.id);
		}else{
			if (privateCreator){
				if (contentCE.type === 'OFFER'){
					if (contentCE.offer && contentCE.offer.parent && contentCE.offer.parent.idUser){
						return contentCE.offer.parent.idUser === userLoged.id;
					}else{
						return false;
					}
				}else{
					return contentCE.idUser === userLoged.id;
				}
			}else{
				return publicCreator;
			}
		}
	};
})(window.ce);
