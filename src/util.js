//Closure
(function(ce) {
	'use strict';

	ce.util = ce.util || {};
	ce.util.lang = ce.util.lang || {};
	ce.util.array = ce.util.array || {};

	ce.util.regexForDecimals = new RegExp('^(-)?[0-9]+([\,\.][0-9]+)?$');

	ce.util.getClientGroupDomainFromLacation = function(host, absUrl){
		var clientGroupDomain = null;

		//get the client by the domain name
		var firstPoint = host.indexOf('.');

		if (host.indexOf('www.') === -1){
			//no www
			clientGroupDomain = host.substring(0, firstPoint);
		}else{
			//www
			var secondPoint = host.indexOf('.', firstPoint+1);
			clientGroupDomain = host.substring(firstPoint+1, secondPoint);
		}

		if (!clientGroupDomain || !isNaN(parseInt(clientGroupDomain, 10)) || ['int', 'pre', 'cityexperience'].indexOf(clientGroupDomain) > -1){
			clientGroupDomain = 'arahal';
		}

		return clientGroupDomain;
	};


	ce.util.getClientDomainFromLacation = function(host, absUrl){
		var clientDomain = null;

		if(/\/#!\/\w+\//.test(absUrl)){

			var splitted = absUrl.split('/');
			var indexHashtag = splitted.indexOf('#!');
			clientDomain = splitted[indexHashtag + 1];

		}else{
			//get the client by the domain name
			var firstPoint = host.indexOf('.');

			if (host.indexOf('www.') === -1){
				//no www
				clientDomain = host.substring(0, firstPoint);
			}else{
				//www
				var secondPoint = host.indexOf('.', firstPoint+1);
				clientDomain = host.substring(firstPoint+1, secondPoint);
			}
		}

		if (!clientDomain || !isNaN(parseInt(clientDomain, 10)) || ['int', 'pre', 'cityexperience'].indexOf(clientDomain) > -1){
			clientDomain = 'arahal';
		}

		return clientDomain;
	};

	ce.util.noCache = function () {
		return '?_=' + Math.random();
	};

	ce.util.getNextSunday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date;
			break;
		case 1:
			d = date.add(6).day();
			break;
		case 2:
			d = date.add(5).day();
			break;
		case 3:
			d = date.add(4).day();
			break;
		case 4:
			d = date.add(3).day();
			break;
		case 5:
			d = date.add(2).day();
			break;
		case 6:
			d = date.add(1).day();
			break;
		default:
			break;
		}

		return d;
	};
	ce.util.getWeekFriday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date.add(-2).day();
			break;
		case 1:
			d = date.add(4).day();
			break;
		case 2:
			d = date.add(3).day();
			break;
		case 3:
			d = date.add(2).day();
			break;
		case 4:
			d = date.add(1).day();
			break;
		case 5:
			d = date;
			break;
		case 6:
			d = date.add(-11).day();
			break;
		default:
			break;
		}

		return d;
	};
	ce.util.getWeekMonday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date.add(-6).day();
			break;
		case 1:
			d = date;
			break;
		case 2:
			d = date.add(-1).day();
			break;
		case 3:
			d = date.add(-2).day();
			break;
		case 4:
			d = date.add(-3).day();
			break;
		case 5:
			d = date.add(-4).day();
			break;
		case 6:
			d = date.add(-5).day();
			break;
		default:
			break;
		}

		return d;
	};

	ce.util.isSet = function(obj){
		return (obj !== undefined && obj !== null && obj !== '');
	};

	ce.util.calculateScheduleResume = function(schedule, $scope){
		var resume = '';
		if (schedule !== undefined && schedule !== null){
			if (schedule.monday){
				resume += $scope.msg('common.monday')+ ', ';
			}
			if (schedule.tuesday){
				resume += $scope.msg('common.tuesday')+ ', ';
			}
			if (schedule.wednesday){
				resume += $scope.msg('common.wednesday')+ ', ';
			}
			if (schedule.thursday){
				resume += $scope.msg('common.thrusday')+ ', ';
			}
			if (schedule.friday){
				resume += $scope.msg('common.friday')+ ', ';
			}
			if (schedule.saturday){
				resume += $scope.msg('common.saturday')+ ', ';
			}
			if (schedule.sunday){
				resume += $scope.msg('common.sunday')+ ', ';
			}
			if (resume.charAt(resume.length-2) === ','){
				resume = resume.substr(0, resume.length-2);
			}
		}
		schedule.resume = resume;
	};

	/**
	 * Return a string with two digits from a int number
	 */
	ce.util.twoDigitsString = function(number){
		if (number === undefined || number === null){
			return '';
		} else if ((number+'').length === 2){
			return ''+number;
		} else {
			return '0'+number;
		}
	};



	//LANG FUNCTIONS
	ce.util.lang.getCurrentLocale = function(absUrl, languages){
		var currentLanguage = '';
		var langParamPosition = absUrl.indexOf('lang=');
		if (langParamPosition !== -1){
			var lang = absUrl.substring(langParamPosition +5 ,langParamPosition +7);
			currentLanguage = languages[lang];
		}else{
			//get the browser language
			var browserLang = window.navigator.userLanguage || window.navigator.language;
			currentLanguage = languages[browserLang];
		}

		return currentLanguage;
	};

	ce.util.lang.getArrayLanguages = function(){
		var languages = [];
		languages['es'] = { name: 'Español', icon: 'assets/img/flags/flag-spain.png' , lang: 'es'};
		languages['en'] = { name: 'English', icon: 'assets/img/flags/flag-uk.png', lang: 'en' };
		languages['ca'] = { name: 'Català', icon: 'assets/img/flags/flag-catalonia.png', lang: 'ca' };
		languages['it'] = { name: 'Italiano', icon: 'assets/img/flags/flag-italy.png', lang: 'it' };
		languages['pt'] = { name: 'Português', icon: 'assets/img/flags/flag-brazil.png', lang: 'pt' };
		languages['ru'] = { name: 'Руccкий', icon: 'assets/img/flags/flag-russia.png', lang: 'ru' };
		languages['fr'] = { name: 'Français', icon: 'assets/img/flags/flag-france.png', lang: 'fr' };
		languages['de'] = { name: 'Deutsch', icon: 'assets/img/flags/flag-germany.png', lang: 'de' };
		languages['zh'] = { name: '中文', icon: 'assets/img/flags/flag-china.png', lang: 'zh' };
//		{ name: 'हिनॿदी', icon: 'assets/img/flags/flag-india.png' }, // Hindi
//		{ name: '日本語', icon: 'assets/img/flags/flag-japan.png' }, // Nihongo
//		{ name: 'Nederlands', icon: 'assets/img/flags/flag-netherlands.png' }, // Dutch
//		{ name: 'العربية', icon: 'assets/img/flags/flag-saudiarabia.png' } // Arabic
//		];
		return languages;
	};

	ce.util.lang.getLocalesAvailablesInClient = function(client, languages){
		var langs = [];
		if (client !== null && client.languages !== null){
			angular.forEach(client.languages, function(language, i){
				if (languages[language] !== undefined){
					langs.push(languages[language]);
				}
			});
		}
		return langs;
	};

	//Utility function to remove an element from an array
	ce.util.array.remove = function(array, object) {
		var i = array.indexOf(object);
		if (i >= 0) {
			array.splice(i, 1);
		}
	};

	// XHR helper
	// All request must to have follow structure: ce.xhr.method(url, data, successCallback, errorCallback)
	ce.xhr = (function() {
		var apiVersion = 'jaxrs/v2/';

		return {
			get: function xhrGet() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'GET', url: url, cache: false, params: args.params }, args);
			},
			post: function xhrPost() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'POST', url: url, data: args.data, cache: false, params: args.params }, args);
			},
			put: function xhrPut() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'PUT', url: url, data: args.data, cache: false, params: args.params }, args);
			},
			delete: function xhrDelete() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'DELETE', url: url, cache: false, params: args.params }, args);
			}
		};

		///////

		function xhrRequest(config, args) {
			this
				.http(config)
				.then(function resolve(res) {
					if (typeof args.successCallback === 'object') {
						var response = args.successCallback.fullResponse ? res : res.data;
						return args.successCallback.callback(response);
					}
					args.successCallback(res.data);
				})
				.catch(function reject(res) { throwError(args.errorCallback, res.data, res.status); });
		}

		function formatRequestArguments(requestArguments) {
			var args = [].slice.call(requestArguments);
			if (args.length < 2) { return {}; }

			return {
				url: (args[0] || []).join('/'),
				data: args[3],
				params: args[4],
				successCallback: args[1],
				errorCallback: args[2]
			};
		}

		function throwError(errorCallback, res) {
			if (res.status === 401) { return; }
			if (angular.isFunction(errorCallback)) { return errorCallback(res.data, res.status); }
			throw new Error(res);
		}

	})();

//	End Closure
})(window.ce);
