window.ce = window.ce || {};
//Closure
(function(ce) {
	'use strict';

	ce.services = function(http, pathRelative, api){
		this.http = http;
		this.pathRelative = pathRelative || '';
		this.api = api;

		this.HTTP_OK = 200;
		this.NO_RESPONSE = 204;
		this.UNAUTHORIZED = 402;
		this.NOT_FOUND = 404;
		this.INTERNAL_ERROR = 500;
		this.VALIDATION_ERROR = 400;
	};

	ce.services.ConfigurationService = function(http, pathRelative) {
		ce.services.call(this,http,pathRelative);

		this.categories = null;
		this.services = null;

	};
	ce.services.ConfigurationService.prototype = new ce.services();

	ce.services.ConfigurationService.prototype.getCategories = function(sucessFnCallback, errorFnCallback){
		var scope = this;

		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/categories', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data;
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getCategoriesForClient = function(clientId, sucessFnCallback, errorFnCallback){
		var scope = this;
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/categoriesAvailable';

		this.http({method: 'GET', url: url , withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.assignCategoryToClient = function(clientId, categoryId, assigned, sucessFnCallback, errorFnCallback){
		var scope = this;
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/categories/'+categoryId+'/assign';

		this.http({method: 'PUT', url: url, withCredentials: true, cache:false, data: assigned})
		.then(function(response) {
			if (response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};



	ce.services.ConfigurationService.prototype.getMessages = function(lang, sucessFnCallback, errorFnCallback){
		var serviceUrl = this.pathRelative +'jaxrs/messages';
		if (lang){
			serviceUrl += '?lang='+lang.lang;
		}

		this.http({method: 'GET', url: serviceUrl , withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.ConfigurationService.prototype.getConfiguration = function(clientId, lang, sucessFnCallback, errorFnCallback){
		// Load the configuration
		// all=true -> load all sub-categories and services
		var serviceUrl = this.pathRelative +'jaxrs/v2/configuration?all=true';
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		// clientId=n -> load only categories associated with the given client
		if (clientId !== null) {
			serviceUrl += '&clientId=' + clientId;
		}

		serviceUrl += '&_=' + Math.random();

		var scope = this;
		this.http({method: 'GET', url: serviceUrl, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data.categories;
				scope.services = response.data.services;
				scope.mapCategories = {};

				//init numResults categories to 0
				angular.forEach(scope.categories, function(category, i){
					category.numResults = 0;
					scope.mapCategories[category.id] = category;
				});
				//init numResults services to 0
				angular.forEach(scope.services, function(service, i){
					service.numResults = 0;
				});

				sucessFnCallback(scope.categories, scope.mapCategories, scope.services, response.data.initLanguage, response.data.clients, response.data.pricesTypes, response.data.user);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.searchCategory = function(idCategory, service){
		var category;
		var elements = this.categories;
		if (service){
			elements = this.services;
		}
		angular.forEach(elements, function(cat, i){
			if (cat.id === idCategory){
				category = cat;
			}
		});
		return category;
	};
	ce.services.ConfigurationService.prototype.searchSubCategory = function(parentCategory, idSubCategory, service){
		var subCategory = null;
		if (parentCategory){
			angular.forEach(parentCategory.subCategories, function(cat, i){
				if (cat.id === idSubCategory){
					subCategory = cat;
				}
			});
		}
		return subCategory;
	};

	ce.services.ConfigurationService.prototype.getClientGroups = function(sucessFnCallback, errorFnCallback){
		var scope = this;
		
		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/clientGroups', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data;
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getClientGroup = function(clientId, sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clientGroups/'+clientId, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.updateClientGroup = function(clientId, clientGroup, sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/clientGroups/'+clientId, withCredentials: true, cache:false, data: clientGroup})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getClientGroupByDomainName = function(domainName, sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clientGroups/domainname/'+domainName, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status === 204 || response.status === 304){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getDynamicType = function(dynamicTypeId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes/'+dynamicTypeId;
		
		this.http({method: 'GET', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getDocumentToDynamicType = function(clientId, collectionName, documentId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+ clientId +'/'+ collectionName + '/' + documentId;
		
		this.http({method: 'GET', url: url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.addDocumentToDynamicType = function(collectionName, clientId, document, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/'+collectionName;
		
		this.http({method: 'POST', url:  url, withCredentials: true, cache:false, data: document})
		.then(function(response) {
			if (response.status === 200 || response.status ===  204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.deleteDocumentToDynamicType = function(collectionName, clientId, documentId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/'+collectionName+'/'+documentId;
		
		this.http({method: 'DELETE', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status ===  204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.deleteDynamicType = function(dynamicTypeId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes/'+dynamicTypeId;
		
		this.http({method: 'DELETE', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 ||response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.getDynamicTypes = function(sucessFnCallback, errorFnCallback){		
		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/dynamicTypes', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.getDynamicTypesForClient = function(clientId, sucessFnCallback, errorFnCallback){		
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes?clientId='+clientId;
		this.http({method: 'GET', url: url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.addDynamicType = function(dynamicType, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes';
		
		this.http({method: 'POST', url: url, withCredentials: true, cache:false, data:dynamicType})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	//	End Closure
})(window.ce);
