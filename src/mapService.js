(function(ce){
	'use strict';

	ce.services.MapService = function() {
		var markers = [];
		var overlays = [];
		var routesPolylines = [];

		this.map = null;
		this.mapOptions = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControlOptions: { style: google.maps.ZoomControlStyle.SMALL },
			center: new google.maps.LatLng(37.2621077, -5.5455903),
			zoom: 15,
			styles: [
				{
					featureType: 'poi.business',
					stylers: [{ visibility: 'off' }]
				}
			]
		};

		this.createMarker = function(contentCE, onClickMarker, onDblclickMarker, onMouseOverMarker){
			if (markers.indexOf(contentCE) > -1){
				console.log('elemento ya añadido');
				return;
			}

			markers.push(contentCE);

			var latitude, longitude;

			if (contentCE.type === 'ROUTE' && contentCE.route){

				latitude = contentCE.route.latitude;
				longitude = contentCE.route.longitude;

			}else if (contentCE.type === 'ACTIVITY' && contentCE.activity &&
					contentCE.activity.attractionParent && contentCE.activity.attractionParent.attraction){

				latitude = contentCE.activity.attractionParent.attraction.latitude;
				longitude = parseFloat(contentCE.activity.attractionParent.attraction.longitude) + 0.00006;

			}else if (contentCE.type === 'OFFER' && contentCE.offer && contentCE.offer.parent){

				if(contentCE.offer.parent.attraction){
					latitude = contentCE.offer.parent.attraction.latitude;
					longitude = contentCE.offer.parent.attraction.longitude - 0.00006;

				}else if(contentCE.offer.parent.activity && contentCE.offer.parent.activity.attractionParent){
					latitude = contentCE.offer.parent.activity.attractionParent.attraction.latitude;
					longitude = contentCE.offer.parent.activity.attractionParent.attraction.longitude - 0.00006;

				}else{
					console.log('error en parent type for offer', contentCE.id, '- ', contentCE.offer.parentUrl);
				}
			}else if (contentCE.attraction){

				latitude = contentCE.attraction.latitude;
				longitude = contentCE.attraction.longitude;

			}else{
				console.log('No se ha encontrado coordenadas para el contenido', contentCE.id);
			}

			if(!latitude || !longitude) { return; }

			latitude = (''+latitude).split(',').join('');
			longitude = (''+longitude).split(',').join('');

			var marker = new google.maps.Marker({
				contentCE: contentCE,
				iconActive: contentCE.iconActive,
				iconInactive: contentCE.iconInactive,
				icon: contentCE.iconInactive,
				map: this.map,
				position: new google.maps.LatLng(latitude, longitude),
				title: contentCE.name
			});

			marker.setZIndex(contentCE.type === 'ROUTE' ? 1.5 : 0);
			overlays.push(marker);

			google.maps.event.addListener(marker, 'mouseover', onMouseOver);
			google.maps.event.addListener(marker, 'mouseout', onMouseOut);
			google.maps.event.addListener(marker, 'click', onClick);
			google.maps.event.addListener(marker, 'dblclick', onDblclick);

			var self = this;

			function onMouseOver() {
				onMouseOverMarker(marker.contentCE, true);
				marker.setIcon(marker.iconActive);

				//if it's a route, active its elements marker
				if (marker.contentCE.type === 'ROUTE' && 
					marker.contentCE.route.attractions &&
					marker.contentCE.route.attractions.length){

					marker.contentCE.route.attractions.forEach(function(attraction){
						self.setMarkerStatus(attraction, true);
					});
					//change polyline opacity
					var polyline = getPolyline(marker.contentCE);
					if (polyline){
						polyline.setOptions({strokeOpacity:1.0});
					}
				}
			}

			function onMouseOut() {
				onMouseOverMarker(marker.contentCE, false);
				marker.setIcon(marker.iconInactive);

				//if it's a route, desactive its elements marker
				if (marker.contentCE.type === 'ROUTE' && 
					marker.contentCE.route.attractions &&
					marker.contentCE.route.attractions.length){

					marker.contentCE.route.attractions.forEach(function(attraction){
						self.setMarkerStatus(attraction, false);
					});
					//change polyline opacity
					var polyline = getPolyline(marker.contentCE);
					if (polyline){
						polyline.setOptions({strokeOpacity:0.1});
					}
				}
			}

			function onClick() {
				onClickMarker(marker.contentCE);
			}

			function onDblclick(){
				onDblclickMarker(marker.contentCE);
			}
		};

		this.createRoute = function(contentCE, onClickPolyline, onDblclickPolyline, onMouseOverPolyline){
			var polyline;
			if (contentCE && contentCE.route && contentCE.route.overviewPolyline){
				//create polyline
				var decodedSets = google.maps.geometry.encoding.decodePath(contentCE.route.overviewPolyline);
				var iconsetngs = { path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW };
				polyline = new google.maps.Polyline({
					path:decodedSets, 
					strokeColor: '#FF0000', 
					strokeOpacity: 0.1, 
					strokeWeight: 2, 
					contentCE: contentCE,
					icons: [{icon: iconsetngs, repeat:'25%', offset: '100%'}]
				});

				polyline.setOptions({ strokeOpacity: 0.1 });
				routesPolylines.push(polyline);

				google.maps.event.addListener(polyline, 'mouseover', onMouseOver);
				google.maps.event.addListener(polyline, 'mouseout', onMouseOut);
				google.maps.event.addListener(polyline, 'click', onClick);
				google.maps.event.addListener(polyline, 'dblclick', onDblclick);
			}

			function onMouseOver(){
				onMouseOverPolyline(polyline.contentCE, true);
				polyline.setOptions({ strokeOpacity: 1.0 });
			}

			function onMouseOut(){
				onMouseOverPolyline(polyline.contentCE, false);
				polyline.setOptions({ strokeOpacity: 0.1 });
			}

			function onClick(){
				onClickPolyline(polyline.contentCE);
			}

			function onDblclick(){
				onDblclickPolyline(polyline.contentCE);
			}
		};

		this.clearMap = function(){
			overlays.forEach(function(marker){
				marker.setMap(null);
			});
			overlays.splice(0);
			markers.splice(0);

			routesPolylines.forEach(function(polyline){
				polyline.setMap(null);
			});
			routesPolylines.splice(0);
		};

		this.resizeMap = function(){
			google.maps.event.trigger(this.map, 'resize');
		};

		this.initMap = function(latitude, longitude, zoom, maxZoom){
			if (!this.map) { return; }

			var self = this;

			routesPolylines.forEach(function(polyline){
				polyline.setMap(self.map);
			});

			if(overlays.length){
				var bounds = new google.maps.LatLngBounds();
				overlays.forEach(function(marker){
					bounds.extend(marker.position);
				});
				this.map.fitBounds(bounds);

				google.maps.event.addListenerOnce(this.map, 'idle', function() {
					if (self.map.getZoom() > maxZoom){
						self.map.setZoom(maxZoom);
					}
				});
			}else if(latitude && longitude){
				this.map.setCenter(new google.maps.LatLng(latitude, longitude));
				this.map.setZoom(zoom);
			}
		};

		this.setMarkerStatus = function (contentCE, active){
			var marker = getMaker(contentCE);
			//change the marker status
			if (marker){
				marker.setIcon(active ? marker.iconActive : marker.iconInactive);
				marker.setZIndex(contentCE.type === 'ROUTE' ? 1.5 : (active ? 1 : 0));
			}
			//change the opacity of the polyline
			if (contentCE.type === 'ROUTE'){
				var polyline = getPolyline(contentCE);
				if (polyline){
					polyline.setOptions({strokeOpacity: active ? 1.0 : 0.1});
				}
				//change status its attractions
				if(contentCE.route.attractions && contentCE.route.attractions.length){
					contentCE.route.attractions.filter(function(attraction){
						var markerAttraction = getMaker(attraction);
						if (markerAttraction){
							markerAttraction.setIcon(active ? markerAttraction.iconActive : markerAttraction.iconInactive);
							markerAttraction.setZIndex(active ? 1 : 0);
						}
					});
				}
			}
		};

		function getMaker(contentCE){
			var marker = overlays.filter(function(marker){
				return marker.contentCE.id === contentCE.id && marker.contentCE.type === contentCE.type;
			});

			if(marker.length){ return marker[0]; }
		}

		function getPolyline(contentCE){
			var polyline = routesPolylines.filter(function(polyline){
				return polyline.contentCE.id === contentCE.id && polyline.contentCE.type === contentCE.type;
			});

			if(polyline.length){ return polyline[0]; }
		}
	};
})(window.ce);