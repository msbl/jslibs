//Closure
(function(ce) {
	'use strict';

	ce.repository = ce.repository || {};

	ce.repository.ContentRepository = function(contentServices, session, routeServices, CEConfiguration) {
		this.contentServices = contentServices;
		this.session = session;
		this.CEConfiguration = CEConfiguration;
		this.routeServices = routeServices;
		this.content = [];
		this.counter = {
			attractions : 0,
			shops: 0,
			activities : 0,
			offers : 0,
			routes : 0,
			today: 0,
			tomorrow: 0,
			thisWeekend : 0,
			thisMonth : 0,
			thisWeek: 0,
			building : 0,
			dynamic: 0
		};
		// Array of user routes
		this.userRoutes = [];
	};


	ce.repository.ContentRepository.prototype.getContent = function(showPrivateContent, showArchivedContent, language, successFnCallBack){
		var self = this;
		if (showPrivateContent && this.session.publicCreator){
			this.contentServices.getPrivateContent(this.CEConfiguration, language, this.session, function(data){
				self.initCounter();
				self.content = data;
			});
		}else if (showArchivedContent && this.session.publicCreator){
			this.contentServices.getArchivedContent(this.CEConfiguration, language, this.session, function(data){
				self.initCounter();
				self.content = data;
			});
		} else{
			this.initCounter();
			this.contentServices.getContent(this.CEConfiguration, language, this.session, this.counter, function(data){
				self.content = data;

				// get the user routes
				self.getUserRoutes(self.CEConfiguration.selectedClient);

				if (successFnCallBack){
					successFnCallBack();
				}
				
			}, function(data){
				console.log('Error searching content');
			});
		}

	};

	ce.repository.ContentRepository.prototype.getUserRoutes = function(selectedClient){
		this.userRoutes = [];
		if (this.session.isLogged){
			var self = this;
			this.routeServices.searchMyRoutes(this.session.userLoged.id, selectedClient.id, this.content, function(routes){
				self.userRoutes = routes;

				angular.forEach(self.userRoutes, function(route, i){
					if (!route.route.publicRoute){
						route.canModify = true;
					}else{
						route.canModify = false;
					}
				});

			}, function(data){
				console.log('Error searching user routes');
			});
		}
	};



	ce.repository.ContentRepository.prototype.initCounter = function(){
		this.counter = {
			attractions : 0,
			shops: 0,
			activities : 0,
			offers : 0,
			routes : 0,
			today: 0,
			tomorrow: 0,
			thisWeekend : 0,
			thisMonth : 0,
			thisWeek: 0,
			building : 0,
			dynamic: 0
		};
	};
})(window.ce);
