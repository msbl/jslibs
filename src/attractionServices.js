//Closure
(function(ce) {
	'use strict';

	ce.services.AttractionServices = function(http, pathRelative) {
		ce.services.call(this,http,pathRelative);
	};
	ce.services.AttractionServices.prototype = new ce.services();

	/**
	*
	*/
	ce.services.AttractionServices.prototype.deleteAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			if (response.status === 204) {
				successFnCallBack();
			} else {
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.AttractionServices.prototype.addAttraction = function(clientId, json, translate, publish, successFnCallBack, errorFnCallback){
		//search user routes
		this.http({method: 'POST', url: 'jaxrs/clients/'+ clientId +'/attractions?translate='+translate+'&publish='+publish, data:json, cache:false})
		.then(function(response) {
			//check if the status code responde is correct
			if (response.status === 200 && response.data.statusRequest === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.addAttractionToRoute = function(route, attractionId, successFnCallBack, errorFnCallback){
		var requestUrl = 'jaxrs/v2/user/routes/' + route.id + '/attractions/' + attractionId;
		this.http({method : 'PUT', url : requestUrl, data : route, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.deleteAttractionToRoute = function(routeId, attractionId, successFnCallBack, errorFnCallback){
		var requestUrl = 'jaxrs/v2/user/routes/' + routeId + '/attractions/' + attractionId;
		this.http({method : 'DELETE', url : requestUrl, cache:false})
		.then(function(response) {
			// check if the status code responde is
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.getAttractionCE = function(attractionId, clientId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : this.pathRelative + 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.AttractionServices.prototype.getAttractionEntity = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/'+ clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.acceptAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/accept', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.acceptDynamic = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/accept', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.rejectAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/reject', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.transferAttraction = function(clientId, attractionId, userId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId + '/transfer/' + userId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.getOfferEntity = function(offerId, clientId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/offers/'+offerId;

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.deleteOffer = function(offerId, attractionId, clientId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attractions/'+attractionId+'/offers/'+offerId;

		this.http({method: 'DELETE', url: url, cache:false})
		.then(function(response) {
			if (response.status === 204){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.addOffer = function(attractionId, clientId, offer, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attraction/'+attractionId+'/offers/';

		this.http({method: 'POST', url: url, data: offer, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveDynamic = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/archive', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveContent = function(clientId, contentType, contentId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/' + contentType + '/' + contentId + '/archive';

		this.http({method: 'PUT', url: url, data : {}, cache:false})
		.then(function(response) {
			if (response.status === 200){ return successFnCallBack(response.data); }
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		})
		.catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveOffer = function(clientId, attractionId, offerId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/offers/' + offerId + '/archive';

		this.http({method: 'PUT', url: url, data : {}, cache:false})
		.then(function(response) {
			if (response.status === 200){ return successFnCallBack(response.data); }
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		})
		.catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishOffer = function(clientId, attractionId, offerId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId + '/offers/' + offerId + '/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishDocument = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	//	End Closure
})(window.ce);
