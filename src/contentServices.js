(function(ce) {
	'use strict';

	ce.services.ContentServices = function(http, configurationServices,pathRelative, config) {
		ce.services.call(this,http, pathRelative);
		this.configurationServices = configurationServices;
		this.config = config;
	};
	ce.services.ContentServices.prototype = new ce.services();
	/**
	*
	*/
	ce.services.ContentServices.prototype.getContent = function(CEConfiguration, lang, session, counter, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;
		var privateCreator = session.privateCreator;
		var publicCreator = session.publicCreator;

		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/content';
		if (privateCreator === true){
			url += '?privateCreator=true';
		}else{
			url += '?privateCreator=false';
		}

		if (publicCreator || privateCreator){
			url += '&time='+new Date().getTime();
		}
		if (lang){
			url += '&lang='+lang.lang;
		}

		this.http({method: 'GET', url: this.pathRelative + url, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, privateCreator, publicCreator, counter, categories, CEConfiguration, session);

				var count = 0;
				CEConfiguration.mapCategories1 = {};
				CEConfiguration.mapCategories2 = {};


				var totalCategoriesNotEmpty = 0;
				for (var prop in categories) {
					if (categories[prop].numResults > 0){
						totalCategoriesNotEmpty++;
					}
				}
				var middle = Math.ceil(totalCategoriesNotEmpty / 2);

				for (var property in categories) {
					if (categories[property].numResults > 0){
						count++;
						if (totalCategoriesNotEmpty < 13 || (totalCategoriesNotEmpty >= 13 && count <= middle)){
							CEConfiguration.mapCategories1[property] = categories[property];
						}else{
							CEConfiguration.mapCategories2[property] = categories[property];
						}
					}
				}
				CEConfiguration.categoriesUsed = totalCategoriesNotEmpty;

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.ContentServices.prototype.getPrivateContent = function(CEConfiguration, lang, session, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;

		var serviceUrl =  'jaxrs/v2/clients/' + clientId + '/content/pending?date='+new Date().getTime();
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		var scope = this;
		this.http({method: 'GET', url:serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, false, true, null, null, CEConfiguration, session);
				angular.forEach(categories, function(category, i){
					category.numResults = 0;
					angular.forEach(resultSearchElements, function(contentCE, j){
						if (contentCE.idCategory === category.id){
							category.numResults = category.numResults +1;
						}
					});
				});

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.ContentServices.prototype.getArchivedContent = function(CEConfiguration, lang, session, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;

		var serviceUrl =  'jaxrs/v2/clients/' + clientId + '/content/archived?date='+new Date().getTime();
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		var scope = this;
		this.http({method: 'GET', url:serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, false, true, null, null, CEConfiguration, session);
				angular.forEach(categories, function(category, i){
					category.numResults = 0;
					angular.forEach(resultSearchElements, function(contentCE, j){
						if (contentCE.idCategory === category.id){
							category.numResults = category.numResults +1;
						}
					});
				});

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.ContentServices.prototype.getInfoSharedElement = function(userId, routeId, attractionId, successFnCallBack, errorFnCallback){
		var scope = this;
		var httpUrl = 'jaxrs/v2/shared';
		if (routeId) {
			httpUrl += '?routeId=' + routeId;
		} else if (attractionId){
			httpUrl += '?attractionId=' + attractionId;
		} else {
			return null;
		}
		if (userId) {
			httpUrl += '&userId=' + userId;
		}

		this.http({method: 'GET', url: httpUrl , cache:false})
		.then(function(response) {
			scope.setMarkers(response.data.sharedElement);
			if (response.data.sharedElement.type === 'ROUTE' && response.data.sharedElement.route.attractions && response.data.sharedElement.route.attractions.length > 0){
				angular.forEach(response.data.sharedElement.route.attractions, function(attraction, i){
					scope.setMarkers(attraction);
				});
			}
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	* Routes have an array with attractions id, replace then with an attractions array
	* Attractions have an array with activities id, replace them with activities array
	* Activities hava an parent attraction id, replace it for attraction entity
	*/
	ce.services.ContentServices.prototype.fillIdsbyElements = function(resultSearchElements, privateCreator, publicCreator, counter, categories, CEConfiguration, session){
		var today = Date.today();
		var tomorrow = Date.today().add(1).day();
		var afterTomorrow = Date.today().add(1).day();
		var sevenDays = Date.today().add(7).day();
		var oneMonth = Date.today().add({months: 1}).day();
		var friday = ce.util.getWeekFriday(today);
		var sunday = ce.util.getNextSunday(today);
		sunday.setHours(23);
		var monday = ce.util.getWeekMonday(today);
		var y = today.getFullYear(), m = today.getMonth();
		var firstDay = new Date(y, m, 1);
		var lastDay = new Date(y, m + 1, 0);

		//reset the categories value
		angular.forEach(categories, function(category){
			category.numResults = 0;
		});

		//iterate over result search elements
		var scope = this;
		angular.forEach(resultSearchElements, function(element) {
			//first, set the icon active, inactive and category icon
			scope.setMarkers(element);

			if(categories && element.idCategory !== undefined && element.status !== 5){
				if (categories[element.idCategory]){
					categories[element.idCategory].numResults = categories[element.idCategory].numResults + 1;
				}

			}
			if(categories && element.idCategory2 !== undefined && element.status !== 5 && element.idCategory !== element.idCategory2){
				if (categories[element.idCategory2]){
					categories[element.idCategory2].numResults = categories[element.idCategory2].numResults + 1;
				}
			}

			if (publicCreator){
				if (element.status === 3){
					element.statusIcon = 'watch';
				}else if (element.status === 5){
					element.statusIcon = 'tool';
					counter.building++;
				}else if (element.status === 6){
					element.statusIcon = 'folder';
				}
			}else if (privateCreator){
				if (element.status === 1){
					element.statusIcon = 'check';
				}else if (element.status === 3){
					element.statusIcon = 'watch';
				}else if (element.status === 5){
					element.statusIcon = 'tool';
					counter.building++;
				}else if (element.status === 6){
					element.statusIcon = 'folder';
				}
			}

			if(element.type === 'ATTRACTION'){
				//for attractions, replace activities ids for activity entity
				element.attraction.activities = scope.completeActivitiesElements(element.attraction.activitiesUrls, resultSearchElements);
				if (counter !== null && element.status === 1 && !element.attraction.comercio){
					counter.attractions++;
				}
				if (counter !== null && element.status === 1 && element.attraction.comercio){
					counter.shops++;
				}
				element.wrapperPriceRange = element.attraction.priceRange;
			}
		//});

		//angular.forEach(resultSearchElements, function(element) {
			if(element.type === 'ACTIVITY'){
				//for activities, replace parentAttractionId for Attraction entity
				var attractionParent = scope.completeAttractionElement(element.activity.attractionParentUrl, resultSearchElements, false);
				if (attractionParent){
					element.activity.attractionParent = attractionParent;
				}
				if (counter !== null && element.status === 1){
					counter.activities = counter.activities + 1;
				}

				var start = new Date(element.activity.startTime);
				start.setHours(0);
				var end = new Date(element.activity.endTime);
				end.setHours(0);

				element.today = today.between(start, end);
				element.tomorrow = tomorrow.between(start, end);
				element.thisWeekend = (start < sunday && end > friday);
				element.thisWeek = (start < sevenDays && end > today);
				element.thisMonth = (start < oneMonth && end > today);

				if (element.today){
					element.today = false;
					var i = 0;
					var dayToday = today.getDay();
					while(i < element.activity.schedules.length && !element.today){
						var schedule = element.activity.schedules[i];
						element.today = (dayToday === 0 && schedule.sunday || dayToday === 1 && schedule.monday || dayToday === 2 && schedule.tuesday ||
							dayToday === 3 && schedule.wednesday || dayToday === 4 && schedule.thursday || dayToday === 5 && schedule.friday || dayToday === 6 && schedule.saturday);
						i++;
					}
				}
				if (element.tomorrow){
					element.tomorrow = false;
					var j = 0;
					var dayTomorrow = tomorrow.getDay();
					while(j < element.activity.schedules.length && !element.tomorrow){
						var schedule1 = element.activity.schedules[j];
						element.tomorrow = (dayTomorrow === 0 && schedule1.sunday || dayTomorrow === 1 && schedule1.monday || dayTomorrow === 2 && schedule1.tuesday ||
							dayTomorrow === 3 && schedule1.wednesday || dayTomorrow === 4 && schedule1.thursday || dayTomorrow === 5 && schedule1.friday || dayTomorrow === 6 && schedule1.saturday);
						j++;
					}
				}
				if (element.thisWeekend){
					element.thisWeekend = false;
					var k = 0;
					var day = today.getDay();
					while(k < element.activity.schedules.length && !element.thisWeekend){
						var schedule2 = element.activity.schedules[k];
						element.thisWeekend = (schedule2.sunday || schedule2.saturday);
						k++;
					}
				}


				if (counter !== null && element.status === 1){
					if (element.today){
						counter.today++;
					}
					if (element.tomorrow){
						counter.tomorrow++;
					}
					if (element.thisWeekend){
						counter.thisWeekend++;
					}
					if (element.thisWeek){
						counter.thisWeek++;
					}
					if (element.thisMonth){
						counter.thisMonth++;
					}
				}

				element.wrapperPriceRange = element.activity.priceRange;
			}
		//});

		//angular.forEach(resultSearchElements, function(element) {
			if (element.type === 'ROUTE'){
				//for routes, replace attractions url for attraction entity
				element.route.attractions = scope.completeAttractionElements(element.route.attractionsUrls, resultSearchElements);

				if (counter !== null){
					counter.routes++;
				}
			}else if(element.type === 'OFFER' && element.offer.parentUrl !== null && element.offer.parentUrl !== undefined){
				if (element.offer.parentType === 'ATTRACTION'){
					var attractionId = element.offer.parentUrl.substring(element.offer.parentUrl.lastIndexOf('/')+1);
					element.offer.parentId = attractionId;
					element.offer.parent = scope.completeAttractionElement(element.offer.parentUrl, resultSearchElements, false);
					
					if (element.offer.parent){
						if (element.offer.parent.activity){
							//load the attraction parent for the activity
							var attractionParentForOffer = scope.completeAttractionElement(element.offer.parent.activity.attractionParentUrl, resultSearchElements, false);
							if (attractionParentForOffer){
								element.offer.parent.activity.attractionParent = attractionParentForOffer;
							}
						}
					}else{
						console.log('Cannot find parent for offer '+element.id + ' for parent ' + element.offer.parentUrl);
					}
					
				}/*else if (element.parentType === 'ROUTE'){
					//:TODO
				}*/
				if (counter !== null && element.status !== 5){
					counter.offers++;
				}
			}else if (element.type === 'DYNAMIC'){
				if (counter !== null){
					counter.dynamic++;
				}
			}

			element.canModify = ce.security.userCanModify(session.globalCreator, session.publicCreator, session.privateCreator, session.userLoged, element);
		});

		return resultSearchElements;
	};
	/**
	* Define default images
	*/
	ce.services.ContentServices.prototype.setImagesUrls = function(image, clientId){
		if (!ce.util.isSet(image)){
			image = {
				urls: {
					m_web : 'assets/img/defaultImages/neutral-image.jpg',
					l_web: 'assets/img/defaultImages/neutral-image.jpg'
				}
			};
		}else if (!ce.util.isSet(image.urls)){
			image.urls = {
				m_web : 'assets/img/defaultImages/neutral-image.jpg',
				l_web: 'assets/img/defaultImages/neutral-image.jpg'
			};
		}else{
			if (!ce.util.isSet(image.urls.m_web)){
				if (ce.util.isSet(image.urls.thumb2)){
					image.urls.m_web = image.urls.thumb2;
				}else{
					image.urls.m_web = 'assets/img/defaultImages/neutral-image.jpg';
				}
			}
			if (!ce.util.isSet(image.urls.l_web)){
				if (ce.util.isSet(image.urls.thumb1)){
					image.urls.l_web = image.urls.thumb1;
				}else{
					image.urls.l_web = 'assets/img/defaultImages/neutral-image.jpg';
				}
			}
		}
	};
	ce.services.ContentServices.prototype.setMarkers = function(contentCE){
		//define default images
		ce.services.ContentServices.prototype.setImagesUrls(contentCE.mainImage, contentCE.idClient);
		if (contentCE.images && contentCE.images.length > 0){
			angular.forEach(contentCE.images, function(img, i) {
				ce.services.ContentServices.prototype.setImagesUrls(img, contentCE.idClient);
			});
		}

		if (contentCE.type === 'ROUTE'){
			//select the route pin
			contentCE.iconActive = this.config.staticPath+'img/pins/pin-m-active-route.png';
			contentCE.iconInactive = this.config.staticPath+'img/pins/pin-m-inactive-route.png';
			contentCE.categoryIcon = 'assets/img/icon-route-green.png';
			contentCE.categoryName = '';
		} else if (contentCE.type === 'OFFER'){
			contentCE.iconActive = this.config.staticPath+'img/pins/pin-m-active-deal1.png';
			contentCE.iconInactive = this.config.staticPath+'img/pins/pin-m-inactive-deal1.png';
		}else{
			var icons = null;
			var category = this.configurationServices.searchCategory(contentCE.idCategory, contentCE.type==='SERVICE');
			if (category){
				if (contentCE.idSubCategory !== null && contentCE.idSubCategory !== ''){
					var subCategory = this.configurationServices.searchSubCategory(category, contentCE.idSubCategory, contentCE.type==='SERVICE');
					if (subCategory){
						icons = subCategory.icons;
					}else{
						//cannot found subcategory into category
						icons = category.icons;
					}
				}else{
					icons = category.icons;
				}
			}
			if (icons !== null){
				contentCE.iconActive = icons.pin_m_active;
				contentCE.iconInactive = icons.pin_m_inactive;
				contentCE.categoryIcon = icons.icon_m;
				contentCE.categoryName = category.name;
			}else{
				contentCE.iconActive = this.config.staticPath+'img/pins/default-icon.jpg';
				contentCE.iconInactive = this.config.staticPath+'img/pins/default-icon.jpg';
				contentCE.categoryIcon = null;
				contentCE.categoryName = '';
			}
		}
	};
	ce.services.ContentServices.prototype.completeOfferElements = function(offersUrls, resultSearchElements){
		var scope = this;
		if (offersUrls && offersUrls.length > 0){
			var offers = [];
			angular.forEach(offersUrls, function(offerUrl, i) {
				var offerId = offerUrl.substring(offerUrl.lastIndexOf('/')+1);
				//find attraction in resultSearchElements
				var offer = scope.findElementInResultSearch('OFFER', offerId, resultSearchElements);
				if (offer){
					offers.push(angular.copy(offer));
				}
			});
			//replace array of ids by attractions array
			return offers;
		}else{
			return [];
		}
	};
	ce.services.ContentServices.prototype.completeAttractionElements = function(attractionUrls, resultSearchElements){
		var scope = this;
		if (attractionUrls && attractionUrls.length > 0){
			var attractions = [];
			angular.forEach(attractionUrls, function(attractionUrl, i) {
				var attraction = scope.completeAttractionElement(attractionUrl, resultSearchElements, true);
				if (attraction){
					if (attraction.type === 'ACTIVITY'){
						var attractionParent = scope.completeAttractionElement(attraction.activity.attractionParentUrl, resultSearchElements, false);
						if (attractionParent){
							attraction.activity.attractionParent = attractionParent;
						}
					}
					scope.setMarkers(attraction);
					attractions.push(attraction);
				}
			});
			//replace array of ids by attractions array
			return attractions;
		}else{
			return [];
		}
	};
	/**
	* Replace activities urls for entities
	*/
	ce.services.ContentServices.prototype.completeActivitiesElements = function(activitiesUrls, resultSearchElements){
		var scope = this;
		if (activitiesUrls && activitiesUrls.length > 0){
			var activities = [];
			angular.forEach(activitiesUrls, function(activityUrl, i) {
				var activityId =  activityUrl.substring(activityUrl.lastIndexOf('/')+1);
				var activity = scope.findElementInResultSearch('ACTIVITY', activityId, resultSearchElements);
				if (activity){
					scope.setMarkers(activity);
					activities.push(angular.copy(activity));
				}
			});
			return activities;
		}else{
			return [];
		}
	};
	ce.services.ContentServices.prototype.completeAttractionElement = function(attractionUrl, resultSearchElements, loadDependencies){
		var scope = this;
		var attractionId = attractionUrl.substring(attractionUrl.lastIndexOf('/')+1);
		//find attraction in resultSearchElements
		var attraction = scope.findElementInResultSearch('ATTRACTION', attractionId, resultSearchElements);
		if (attraction){
			if (loadDependencies){
				attraction.attraction.activities = scope.completeActivitiesElements(attraction.attraction.activitiesUrls, resultSearchElements);
			}
			attraction = attraction;
		}else{
			var activity = scope.findElementInResultSearch('ACTIVITY', attractionId, resultSearchElements);
			if (activity){
				attraction = activity;
			}
		}
		return angular.copy(attraction);
	};
	/**
	* Search an attraction entity in result search elements by its id
	*/
	ce.services.ContentServices.prototype.findElementInResultSearch = function(type, id, resultSearchElements){
		var i = 0;
		var element = null;
		while (i < resultSearchElements.length && element === null) {
			if (resultSearchElements[i].type === type && resultSearchElements[i].id === parseInt(id)){
				element = resultSearchElements[i];
			}
			i = i+1;
		}
		return element;
	};
})(window.ce);
