//Closure
(function(ce) {
	'use strict';

	ce.services.PortalServices = function(http, contentServices, pathRelative, api) {
		ce.services.call(this,http, pathRelative, api);
		this.contentServices = contentServices;
	};
	ce.services.PortalServices.prototype = new ce.services();

	ce.services.PortalServices.throwError = function(errorFnCallback, data, status) {
		if (angular.isFunction(errorFnCallback) && status !== 401) {
			errorFnCallback(data, status);
		} else if(status !== 401) {
			throw new Error({data: data, status: status});
		}
	};

	ce.services.PortalServices.prototype.loginNative = function(userData, sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative + 'jaxrs/v2/user/loginNative', withCredentials: true, cache:false, data: userData})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				errorFnCallback();
			}
		}).catch(function(response) {
			if (angular.isFunction(errorFnCallback)) {
				errorFnCallback(response.data, response.status);
			} else if(response.status !== 401) {
				throw new Error(response.data);
			}
		});
	};



	/**
	*
	*/
	ce.services.PortalServices.prototype.login = function(sucessFnCallback, errorFnCallback, nologinCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/user?_=' + Math.random(), withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200) {
				if (angular.isDefined(sucessFnCallback)){
					sucessFnCallback(response.data);
				}
			} else if (response.status === 204){
				//do nothing
				document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
				if (nologinCallback){
					nologinCallback(response.data);
				}
			} else {
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.loginPing = function(sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: 'jaxrs/v2/user/ping?_=' + Math.random(), withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status !== 204) {
				throw new Error(response.data);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.logout = function(sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/user/logout', withCredentials: true, cache:false})
		.then(function(response) {
			if (angular.isDefined(sucessFnCallback)){
				sucessFnCallback(response.data);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getClient = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+clientId, cache:false}).then(function(response) {
			if (response.status === 200){
				if (response.data.dynamicTypes && response.data.dynamicTypes.length > 0){
					for(var i = 0; i<response.data.dynamicTypes.length; i++){
						response.data.dynamicTypes[i].jsonSchema = JSON.parse(response.data.dynamicTypes[i].jsonSchema);
						response.data.dynamicTypes[i].formDefinition = JSON.parse(response.data.dynamicTypes[i].formDefinition);
					}
				}
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClients = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/', cache:false}).then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.getAllClients = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/all', cache:false}).then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.getClientByDomainName = function(clientDomain, lang, successFnCallBack, errorFnCallback){
		var serviceUrl = this.pathRelative +'jaxrs/v2/clients/domainname/' + clientDomain;
		if (lang){
			serviceUrl += '?lang='+lang.lang;
		}

		this.http({method: 'GET', url:  serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				//$log.error('Error getting client by domain name');
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getContentCreators = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/contentCreators', cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};





	/**
	*
	*/
	ce.services.PortalServices.prototype.searchMyRoutes = function(userId, clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: 'jaxrs/v2/users/' + userId + '/routes?clientId='+ clientId, withCredentials : true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.printElement = function(contentUrl, successFnCallBack, errorFnCallback){
		this.http({method : 'GET',url : contentUrl + '/pdf', withCredentials : true, cache:false})
		.then(function(response) {

		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getRouteEntity = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.deleteRoute = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.removeRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.addRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.invalidateAll = function(successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/caches/invalidateAll', withCredentials : true, cache:false})
		.then(function(response) {
			successFnCallBack();
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.propagateImage = function(imageId, imageInfo, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: 'jaxrs/v3/images/'+ imageId + '/propagate', data: imageInfo, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClientContacts = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contacts', cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addClientContact = function(clientId, contact, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contacts', data: contact, withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.getClientContactsGroups = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contactGroups', withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addClientContactGroup = function(clientId, contactGroup, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contactGroups', data: contactGroup, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.deleteContact = function(clientId, contactId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : this.pathRelative +'jaxrs/v2/clients/' + clientId + '/contacts/' + contactId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.deleteContactGroup = function(clientId, groupId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : this.pathRelative +'jaxrs/v2/clients/' + clientId + '/contactGroups/' + groupId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClientUsers = function(clientId, successFnCallBack, errorFnCallback, params){
		params = params || {};
		params['_'] = Math.random();
		
		var paramsArray = [];
		for(var key in params){
			if(params.hasOwnProperty(key)) {
				paramsArray.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
			}
		}
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/users?' + paramsArray.join('&'), cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateClientsForUser = function(userId, clients, successFnCallBack, errorFnCallback){
		var data = {
			attractions: null, 
			clients: clients,
			roles:null
		};

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeClients', 
			data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateAttractionsForUser = function(userId, attractions, successFnCallBack, errorFnCallback){
		var data = {
			attractions: attractions, 
			clients: null,
			roles:null
		};

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAttractions', 
			data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateUserAllowEmail = function(userId, allowEmail, successFnCallBack, errorFnCallback){

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAllowEmail', 
			data: allowEmail, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.changeAttractions = function(userId, config, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAttractions', 
			data: config, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addUser = function(clientId, userNew, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/users/clients/'+ clientId, data: userNew, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
    ce.services.PortalServices.prototype.editUser = function(clientId, user, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/clients/'+ clientId, data: user, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.deleteUser = function(userId, successFnCallBack, errorFnCallback){
		this.http({method: 'DELETE', url: this.pathRelative +'jaxrs/v2/users/'+ userId, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.resetUserPassword = function(userId, clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+userId+'/clients/'+ clientId+'/resetPassword', cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.resetPassword = function(data, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/resetPassword', data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.changePassword = function(userId, data, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+userId+'/changePassword', data: data, withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.getUser = function(userId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/users/'+ userId, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.getLoyaltyCard = function(userId, successFnCallBack, errorFnCallback) {
		ce.xhr.get.call(this, ['users', userId, 'loyaltyCard'], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.getPrivateUsers = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId+'/privateUsers', withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.sendByEmail = function(type, clientId, elementId, mailInfo, successFnCallBack, errorFnCallback){
		var postUrl;
		if(type === 'ROUTE'){
			postUrl = 'jaxrs/v2/clients/'+ clientId +'/routes/'+ elementId +'/mail';
		}else{
			postUrl = 'jaxrs/v2/clients/'+ clientId +'/attractions/'+ elementId +'/mail';
		}

		this.http({method: 'POST', url: postUrl, data: mailInfo, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.sendNewsletter = function(clientId, mailInfo, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+ clientId +'/newsletter';

		//remove letters for contacts from elements
		var mailInfoCopied = angular.copy(mailInfo);
		angular.forEach(mailInfoCopied.selectedContacts,function(contact, i) {
			contact.id = parseInt(contact.id);
		});

		this.http({method: 'POST', url: postUrl, data: mailInfoCopied, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.saveReservation = function(reservation, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/reservations/';

		this.http({method: 'POST', url: postUrl, data: reservation, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getReservations = function(clientId, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/reservations/client/'+clientId;

		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateClient = function(clientId, clientCE, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId, data: clientCE, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.getImagesSliders = function(clientId, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+clientId+'/imageSlider';

		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateImagesSlider = function(clientId, imageSlider, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+clientId+'/imageSlider';
		this.http({method: 'POST', url: this.pathRelative + postUrl, data: imageSlider, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.createPayment = function(payment, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/payments';
		this.http({method: 'POST', url: this.pathRelative + postUrl, data: payment, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPayments = function(successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/payments';
		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.messagePush = function(clientGroupId, messagePush, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative + 'jaxrs/v2/clientGroups/'+ clientGroupId +'/pushMessages', data: messagePush, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addMarketingCampaign = function(clientId, marketingCampaign, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns', data: marketingCampaign, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getMarketingCampaigns = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns'})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getMarketingCampaign = function(clientId, mcId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns/'+ mcId})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getCaches = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/caches'})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getData = function(url, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: url})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getContentWithWeight = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/'+ clientId +'/contentWithWeight';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getParkings = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/'+ clientId +'/parkings';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) { successFnCallBack(response.data); })
		.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.getAllLoyalCustomers = function(clientId, successFnCallBack, errorFnCallback, resume){
		var params = { '_': Math.random(), resume: !!resume };
		ce.xhr.get.call(this, ['clients', clientId, 'loyalCustomers'], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.getLoyalCustomers = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getLoyalCustomer = function(clientId, attractionId, customerId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addLoyaltyAmount = function(clientId, attractionId, customerId, data, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId + '/loyalty';

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.reedem = function(clientId, attractionId, customerId, amount, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'/redeem';

		var data = {
			amount: amount
		};

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.loyalty = function(clientId, attractionId, customerId, amount, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'/loyalty';

		var data = {
			amount: amount
		};

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.loyaltyReminder = function(clientId, attractionId, customerIds, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/reminder';

		this.http({method: 'POST', url: url, data: customerIds, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.loyaltyResume = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/resume?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getLoyaltyAttactions = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/loyaltyAttractions?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.getEmployees = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/employee?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.addCustomer = function(clientId, attractionId, userNew, successFnCallBack, errorFnCallback){
		this.http({
            method: 'POST', 
            url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/customer', 
            data: userNew, 
            cache:false
        })
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.addEmployee = function(clientId, attractionId, userNew, successFnCallBack, errorFnCallback){
		this.http({
            method: 'POST', 
            url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/employees', 
            data: userNew, 
            cache:false
        })
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.deletePurchase = function(clientId, attractionId, customerId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'DELETE', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/loyalCustomers/'+customerId+'/purchases/'+purchaseId, 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPurchases = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPurchasesMonthReport = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchasesMonthReport?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.purchase = function(clientId, attractionId, purchase, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/', 
			data: purchase,
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.userPurchase = function(clientId, attractionId, purchaseId, userId, redeem, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/user/'+userId + '?redeem=' + !!redeem, 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.checkPurchase = function(clientId, attractionId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.confirmPurchase = function(clientId, attractionId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/confirm', 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getGroups = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addGroup = function(clientId, group, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups';

		this.http({method: 'POST', url: url, data: group, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateGroup = function(clientId, group, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups/' + group.id;

		this.http({method: 'PUT', url: url, data: group, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateConfiguration = function(clientId, attractionId, configuration, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/loyaltyConfiguration';

		this.http({method: 'PUT', url: url, data: configuration, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getConfiguration = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/loyaltyConfiguration?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.assignTicket = function(clientId, attractionId, purchaseId, ticket, successFnCallBack, errorFnCallback){

		var url = this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/ticket';

		this.http({method: 'PUT', url: url, data: ticket, cache: false})
			.then(function(response) { successFnCallBack(response.data); })
			.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.decodeQrToken = function(token, successFnCallBack, errorFnCallback){

		var url = this.pathRelative + 'jaxrs/v2/tokens/' + token;

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) { successFnCallBack(response.data); })
		.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.associateCard = function(userId, token, forceReassign, successFnCallBack, errorFnCallback){
		var params = { 'userId': userId, 'forceReassign': !!forceReassign };
		ce.xhr.put.call(this, ['tokens', token], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.unifyCustomers = function(idUserToMerge, idUserMerged, params, successFnCallBack, errorFnCallback){
		ce.xhr.put.call(this, ['users', idUserToMerge, 'merge', idUserMerged], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.getGiftVouchers = function(clientId, attractionId, successFnCallBack, errorFnCallback) {
		var params = { attractionId: attractionId, '_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'giftVouchers'], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.addGiftVoucher = function(clientId, data, successFnCallBack, errorFnCallback) {
		ce.xhr.post.call(this, ['clients', clientId, 'giftVouchers'], successFnCallBack, errorFnCallback, data);
	};

	ce.services.PortalServices.prototype.getGiftVoucherByCode = function(clientId, code, successFnCallBack, errorFnCallback) {
		var params = {'_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'giftVouchers', code], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.activateGiftVoucher = function(clientId, code, successFnCallBack, errorFnCallback) {
		ce.xhr.put.call(this, ['clients', clientId, 'giftVouchers', code, 'activate'], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.deactivateGiftVoucher = function(clientId, code, successFnCallBack, errorFnCallback) {
		ce.xhr.delete.call(this, ['clients', clientId, 'giftVouchers', code], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.assignGiftVoucher = function(clientId, code, userId, successFnCallBack, errorFnCallback) {
		ce.xhr.put.call(this, ['clients', clientId, 'giftVouchers', code, 'user', userId], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.simulateLoyalty = function(clientId, attractionId, userId, data, successFnCallBack, errorFnCallback) {
		ce.xhr.post.call(this, ['clients', clientId, 'attractions', attractionId, 'loyalCustomers', userId, 'loyalty', 'simulate'],
			successFnCallBack, errorFnCallback, data);
	};

	ce.services.PortalServices.prototype.getClientStatistics = function(clientId, successFnCallBack, errorFnCallback) {
		var params = {'_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'loyaltyStatistics'], successFnCallBack, errorFnCallback, null, params);
	};

	//	End Closure
})(window.ce);
