//Closure
(function(ce) {
	'use strict';

	ce.services.RouteServices = function(http, contentServices, pathRelative) {
		ce.services.call(this,http,pathRelative);
		this.contentServices = contentServices;
	};
	ce.services.RouteServices.prototype = new ce.services();


	/**
	*
	*/
	ce.services.RouteServices.prototype.searchMyRoutes = function(userId, clientId, resultSearch, successFnCallBack, errorFnCallback){
		var scope = this;
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/users/' + userId + '/routes?clientId='+ clientId, withCredentials : true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var userRoutesURL = response.data;

				var routes = [];
				//the ws return an route ids array, replace the ids for entities
				angular.forEach(userRoutesURL, function(routeUrlRoute, i) {
					var route = null;
					if (angular.isObject(routeUrlRoute)){
						route = routeUrlRoute;
					}else if(angular.isString(routeUrlRoute)){
						var routeId = routeUrlRoute.substring(routeUrlRoute.lastIndexOf('/')+1);
						route = scope.contentServices.findElementInResultSearch('ROUTE', routeId, resultSearch);
					}
					if (route){
						route.route.attractions = scope.contentServices.completeAttractionElements(route.route.attractionsUrls, resultSearch);
						scope.contentServices.setMarkers(route);
						routes.push(route);
					}
				});
				successFnCallBack(routes);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.getRouteEntity = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.deleteRoute = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.removeRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.addRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.getRouteCE = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET',url : this.pathRelative + 'jaxrs/v2/clients/'+ clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.addRoute = function(clientId, routeJson, translate, successFnCallBack, errorFnCallback){
		var request = 'jaxrs/clients/'+ clientId +'/routes?translate='+translate;
		this.http({method: 'POST', url: request, data:routeJson, cache:false})
		.then(function(response) {
			//check if the status code responde is correct
			if (response.status === 200){
				if (response.data.statusRequest === 200){
					successFnCallBack(response.data.element);
				}else if (response.data.statusRequest === 400){
					errorFnCallback('Errores de validacion');
				}else{
					errorFnCallback('Error creando una ruta');
				}
			}else{
				errorFnCallback('Error creando una ruta');
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	//	End Closure
})(window.ce);
