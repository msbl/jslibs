var matchdep = require('matchdep'),
tmp = require('temporary');

module.exports = function (grunt) {
	'use strict';

	matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	var tmpDir = new tmp.Dir();

	grunt.initConfig({

		/* project configuration */

		pkg: grunt.file.readJSON('package.json'),

		prj: {
			src: 'src',
			build: 'dist',
			vendor: 'bower_components',
			buildTemplates: 'build_templates',
			test: 'test',

			jsPattern: '**/*.js',

			files: {
				src: ['<%= prj.src %>/configurationService.js',
					'<%= prj.src %>/util.js',
					'<%= prj.src %>/generalServices.js',
					'<%= prj.src %>/contentServices.js',
					'<%= prj.src %>/mapService.js',
					'<%= prj.src %>/attractionServices.js',
					'<%= prj.src %>/routeServices.js',
					'<%= prj.src %>/securityService.js',
					'<%= prj.src %>/sessionService.js',
					'<%= prj.src %>/repositoryService.js'
				],
				srcTest: ['<%= prj.test %>/<%= prj.jsPattern %>']
			},

			dependencies: [
				'<%= prj.vendor %>/DateJS/build/production/date.min.js',
				'<%= prj.src %>/dirPagination.js'
			]
		},

		clean: {
			build: ['<%= prj.build %>']
		},

		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			scripts: {
				src: ['<%= prj.files.src %>']
			},
			tests: {
				src: ['<%= prj.files.srcTest %>']
			}
		},

		concat: {
			build: {
				src: [
					'<%= prj.dependencies %>',
					'<%= prj.files.src %>'
				],
				dest: '<%= prj.build %>/<%= pkg.name %>.js'
			},
			scripts: {
				src: ['<%= prj.files.src %>'],
				dest: '<%= prj.build %>/<%= pkg.name %>-<%= pkg.version %>.js'
			},
			release: {
				src: [
					'<%= prj.dependencies %>',
					'<%= concat.scripts.dest %>'
				],
				dest: '<%= concat.scripts.dest %>'
			}
		},

		uglify: {
			options: {
				mangle: true
			},
			sources: {
				src: ['<%= concat.scripts.dest %>'],
				dest: '<%= concat.scripts.dest %>'
			}
		},

		karma: {
			options: {
				frameworks: ['jasmine'],
				browsers: ['Chrome'],
				plugins: [
					'karma-jasmine',
					'karma-chrome-launcher'
				],
				singleRun: true
			},
			build: {
				files: {
					src: [
						'<%= prj.dependencies %>',
						'<%= prj.files.src %>',
						'<%= prj.files.srcTest %>'
					]
				}
			}
		}
	});

	/* main tasks */

	grunt.registerTask('release', [
		'concat:scripts',
		'uglify',
		'concat:release'
	]);

	grunt.registerTask('build', [
		'jshint',
		//'karma:build',
		'clean',
		'concat:build'
	]);

	grunt.registerTask('default', ['build', 'release']);
};
