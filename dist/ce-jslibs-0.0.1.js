/**
 * dirPagination - AngularJS module for paginating (almost) anything.
 *
 *
 * Credits
 * =======
 *
 * Daniel Tabuenca: https://groups.google.com/d/msg/angular/an9QpzqIYiM/r8v-3W1X5vcJ
 * for the idea on how to dynamically invoke the ng-repeat directive.
 *
 * I borrowed a couple of lines and a few attribute names from the AngularUI Bootstrap project:
 * https://github.com/angular-ui/bootstrap/blob/master/src/pagination/pagination.js
 *
 * Copyright 2014 Michael Bromley <michael@michaelbromley.co.uk>
 */

(function () {

	/**
	 * Config
	 */
	var moduleName = 'angularUtils.directives.dirPagination';
	var DEFAULT_ID = '__default';

	/**
	 * Module
	 */
	var module;
	try {
		module = angular.module(moduleName);
	} catch (err) {
		// named module does not exist, so create one
		module = angular.module(moduleName, []);
	}

	module
		.directive('dirPaginate', ['$compile', '$parse', 'paginationService', dirPaginateDirective])
		.directive('dirPaginateNoCompile', noCompileDirective)
		.directive('dirPaginationControls', ['paginationService', dirPaginationControlsDirective])
		.filter('itemsPerPage', ['paginationService', itemsPerPageFilter])
		.service('paginationService', paginationService);

	function dirPaginateDirective($compile, $parse, paginationService) {

		return {
			terminal: true,
			multiElement: true,
			compile: dirPaginationCompileFn
		};

		function dirPaginationCompileFn(tElement, tAttrs) {

			var expression = tAttrs.dirPaginate;
			// regex taken directly from https://github.com/angular/angular.js/blob/master/src/ng/directive/ngRepeat.js#L211
			var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

			var filterPattern = /\|\s*itemsPerPage\s*:[^|]*/;
			if (match[2].match(filterPattern) === null) {
				throw 'pagination directive: the \'itemsPerPage\' filter must be set.';
			}
			var itemsPerPageFilterRemoved = match[2].replace(filterPattern, '');
			var collectionGetter = $parse(itemsPerPageFilterRemoved);

			addNoCompileAttributes(tElement);

			// If any value is specified for paginationId, we register the un-evaluated expression at this stage for the benefit of any
			// dir-pagination-controls directives that may be looking for this ID.
			var rawId = tAttrs.paginationId || DEFAULT_ID;
			paginationService.registerInstance(rawId);

			return function dirPaginationLinkFn(scope, element, attrs) {

				// Now that we have access to the `scope` we can interpolate any expression given in the paginationId attribute and
				// potentially register a new ID if it evaluates to a different value than the rawId.
				var paginationId = $parse(attrs.paginationId)(scope) || attrs.paginationId || DEFAULT_ID;
				paginationService.registerInstance(paginationId);

				var repeatExpression = getRepeatExpression(expression, paginationId);
				addNgRepeatToElement(element, attrs, repeatExpression);

				removeTemporaryAttributes(element);
				var compiled = $compile(element);

				var currentPageGetter = makeCurrentPageGetterFn(scope, attrs, paginationId);
				paginationService.setCurrentPageParser(paginationId, currentPageGetter, scope);

				if (typeof attrs.totalItems !== 'undefined') {
					paginationService.setAsyncModeTrue(paginationId);
					scope.$watch(function () {
						return $parse(attrs.totalItems)(scope);
					}, function (result) {
						if (0 <= result) {
							paginationService.setCollectionLength(paginationId, result);
						}
					});
				} else {
					scope.$watchCollection(function () {
						return collectionGetter(scope);
					}, function (collection) {
						if (collection) {
							paginationService.setCollectionLength(paginationId, collection.length);
						}
					});
				}

				// Delegate to the link function returned by the new compilation of the ng-repeat
				compiled(scope);
			};
		}

		/**
		 * If a pagination id has been specified, we need to check that it is present as the second argument passed to
		 * the itemsPerPage filter. If it is not there, we add it and return the modified expression.
		 *
		 * @param expression
		 * @param paginationId
		 * @returns {*}
		 */
		function getRepeatExpression(expression, paginationId) {
			var repeatExpression,
				idDefinedInFilter = !!expression.match(/(\|\s*itemsPerPage\s*:[^|]*:[^|]*)/);

			if (paginationId !== DEFAULT_ID && !idDefinedInFilter) {
				repeatExpression = expression.replace(/(\|\s*itemsPerPage\s*:[^|]*)/, "$1 : '" + paginationId + "'");
			} else {
				repeatExpression = expression;
			}

			return repeatExpression;
		}

		/**
		 * Adds the ng-repeat directive to the element. In the case of multi-element (-start, -end) it adds the
		 * appropriate multi-element ng-repeat to the first and last element in the range.
		 * @param element
		 * @param attrs
		 * @param repeatExpression
		 */
		function addNgRepeatToElement(element, attrs, repeatExpression) {
			if (element[0].hasAttribute('dir-paginate-start') || element[0].hasAttribute('data-dir-paginate-start')) {
				// using multiElement mode (dir-paginate-start, dir-paginate-end)
				attrs.$set('ngRepeatStart', repeatExpression);
				element.eq(element.length - 1).attr('ng-repeat-end', true);
			} else {
				attrs.$set('ngRepeat', repeatExpression);
			}
		}

		/**
		 * Adds the dir-paginate-no-compile directive to each element in the tElement range.
		 * @param tElement
		 */
		function addNoCompileAttributes(tElement) {
			angular.forEach(tElement, function (el) {
				if (el.nodeType === Node.ELEMENT_NODE) {
					angular.element(el).attr('dir-paginate-no-compile', true);
				}
			});
		}

		/**
		 * Removes the variations on dir-paginate (data-, -start, -end) and the dir-paginate-no-compile directives.
		 * @param element
		 */
		function removeTemporaryAttributes(element) {
			angular.forEach(element, function (el) {
				if (el.nodeType === Node.ELEMENT_NODE) {
					angular.element(el).removeAttr('dir-paginate-no-compile');
				}
			});
			element.eq(0).removeAttr('dir-paginate-start').removeAttr('dir-paginate').removeAttr('data-dir-paginate-start').removeAttr('data-dir-paginate');
			element.eq(element.length - 1).removeAttr('dir-paginate-end').removeAttr('data-dir-paginate-end');
		}

		/**
		 * Creates a getter function for the current-page attribute, using the expression provided or a default value if
		 * no current-page expression was specified.
		 *
		 * @param scope
		 * @param attrs
		 * @param paginationId
		 * @returns {*}
		 */
		function makeCurrentPageGetterFn(scope, attrs, paginationId) {
			var currentPageGetter;
			if (attrs.currentPage) {
				currentPageGetter = $parse(attrs.currentPage);
			} else {
				// if the current-page attribute was not set, we'll make our own
				var defaultCurrentPage = paginationId + '__currentPage';
				scope[defaultCurrentPage] = 1;
				currentPageGetter = $parse(defaultCurrentPage);
			}
			return currentPageGetter;
		}
	}

	/**
	 * This is a helper directive that allows correct compilation when in multi-element mode (ie dir-paginate-start, dir-paginate-end).
	 * It is dynamically added to all elements in the dir-paginate compile function, and it prevents further compilation of
	 * any inner directives. It is then removed in the link function, and all inner directives are then manually compiled.
	 */
	function noCompileDirective() {
		return {
			priority: 5000,
			terminal: true
		};
	}

	function dirPaginationControlsDirective(paginationService) {

		var numberRegex = /^\d+$/;

		return {
			restrict: 'AE',
			templateUrl: function (elem, attrs) {
				return attrs.templateUrl;
			},
			scope: {
				maxSize: '=?',
				onPageChange: '&?',
				paginationId: '=?'
			},
			link: dirPaginationControlsLinkFn
		};

		function dirPaginationControlsLinkFn(scope, element, attrs) {

			// rawId is the un-interpolated value of the pagination-id attribute. This is only important when the corresponding dir-paginate directive has
			// not yet been linked (e.g. if it is inside an ng-if block), and in that case it prevents this controls directive from assuming that there is
			// no corresponding dir-paginate directive and wrongly throwing an exception.
			var rawId = attrs.paginationId || DEFAULT_ID;
			var paginationId = scope.paginationId || attrs.paginationId || DEFAULT_ID;

			if (!paginationService.isRegistered(paginationId) && !paginationService.isRegistered(rawId)) {
				var idMessage = (paginationId !== DEFAULT_ID) ? ' (id: ' + paginationId + ') ' : ' ';
				throw 'pagination directive: the pagination controls' + idMessage + 'cannot be used without the corresponding pagination directive.';
			}

			if (!scope.maxSize) { scope.maxSize = 9; }
			scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : true;
			scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : false;

			var paginationRange = Math.max(scope.maxSize, 5);
			scope.pages = [];
			scope.pagination = {
				last: 1,
				current: 1
			};
			scope.range = {
				lower: 1,
				upper: 1,
				total: 1
			};

			scope.$watch(function () {
				return (paginationService.getCollectionLength(paginationId) + 1) * paginationService.getItemsPerPage(paginationId);
			}, function (length) {
				if (0 < length) {
					generatePagination();
				}
			});

			scope.$watch(function () {
				return (paginationService.getItemsPerPage(paginationId));
			}, function (current, previous) {
				if (current != previous && typeof previous !== 'undefined') {
					goToPage(scope.pagination.current);
				}
			});

			scope.$watch(function () {
				return paginationService.getCurrentPage(paginationId);
			}, function (currentPage, previousPage) {
				if (currentPage != previousPage) {
					goToPage(currentPage);
				}
			});

			scope.setCurrent = function (num) {
				if (isValidPageNumber(num)) {
					num = parseInt(num, 10);
					paginationService.setCurrentPage(paginationId, num);
				}
			};

			function goToPage(num) {
				if (isValidPageNumber(num)) {
					scope.pages = generatePagesArray(num, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
					scope.pagination.current = num;
					updateRangeValues();

					// if a callback has been set, then call it with the page number as an argument
					if (scope.onPageChange) {
						scope.onPageChange({ newPageNumber: num });
					}
				}
			}

			function generatePagination() {
				var page = parseInt(paginationService.getCurrentPage(paginationId)) || 1;

				scope.pages = generatePagesArray(page, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
				scope.pagination.current = page;
				scope.pagination.last = scope.pages[scope.pages.length - 1];
				if (scope.pagination.last < scope.pagination.current) {
					scope.setCurrent(scope.pagination.last);
				} else {
					updateRangeValues();
				}
			}

			/**
			 * This function updates the values (lower, upper, total) of the `scope.range` object, which can be used in the pagination
			 * template to display the current page range, e.g. "showing 21 - 40 of 144 results";
			 */
			function updateRangeValues() {
				var currentPage = paginationService.getCurrentPage(paginationId),
					itemsPerPage = paginationService.getItemsPerPage(paginationId),
					totalItems = paginationService.getCollectionLength(paginationId);

				scope.range.lower = (currentPage - 1) * itemsPerPage + 1;
				scope.range.upper = Math.min(currentPage * itemsPerPage, totalItems);
				scope.range.total = totalItems;
			}

			function isValidPageNumber(num) {
				return (numberRegex.test(num) && (0 < num && num <= scope.pagination.last));
			}
		}

		/**
		 * Generate an array of page numbers (or the '...' string) which is used in an ng-repeat to generate the
		 * links used in pagination
		 *
		 * @param currentPage
		 * @param rowsPerPage
		 * @param paginationRange
		 * @param collectionLength
		 * @returns {Array}
		 */
		function generatePagesArray(currentPage, collectionLength, rowsPerPage, paginationRange) {
			var pages = [];
			var totalPages = Math.ceil(collectionLength / rowsPerPage);
			var halfWay = Math.ceil(paginationRange / 2);
			var position;

			if (currentPage <= halfWay) {
				position = 'start';
			} else if (totalPages - halfWay < currentPage) {
				position = 'end';
			} else {
				position = 'middle';
			}

			var ellipsesNeeded = paginationRange < totalPages;
			var i = 1;
			while (i <= totalPages && i <= paginationRange) {
				var pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);

				var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
				var closingEllipsesNeeded = (i === paginationRange - 1 && (position === 'middle' || position === 'start'));
				if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
					pages.push('...');
				} else {
					pages.push(pageNumber);
				}
				i++;
			}
			return pages;
		}

		/**
		 * Given the position in the sequence of pagination links [i], figure out what page number corresponds to that position.
		 *
		 * @param i
		 * @param currentPage
		 * @param paginationRange
		 * @param totalPages
		 * @returns {*}
		 */
		function calculatePageNumber(i, currentPage, paginationRange, totalPages) {
			var halfWay = Math.ceil(paginationRange / 2);
			if (i === paginationRange) {
				return totalPages;
			} else if (i === 1) {
				return i;
			} else if (paginationRange < totalPages) {
				if (totalPages - halfWay < currentPage) {
					return totalPages - paginationRange + i;
				} else if (halfWay < currentPage) {
					return currentPage - halfWay + i;
				} else {
					return i;
				}
			} else {
				return i;
			}
		}
	}

	/**
	 * This filter slices the collection into pages based on the current page number and number of items per page.
	 * @param paginationService
	 * @returns {Function}
	 */
	function itemsPerPageFilter(paginationService) {

		return function (collection, itemsPerPage, paginationId) {
			if (typeof (paginationId) === 'undefined') {
				paginationId = DEFAULT_ID;
			}
			if (!paginationService.isRegistered(paginationId)) {
				throw 'pagination directive: the itemsPerPage id argument (id: ' + paginationId + ') does not match a registered pagination-id.';
			}
			var end;
			var start;
			if (collection instanceof Array) {
				itemsPerPage = parseInt(itemsPerPage) || 9999999999;
				if (paginationService.isAsyncMode(paginationId)) {
					start = 0;
				} else {
					start = (paginationService.getCurrentPage(paginationId) - 1) * itemsPerPage;
				}
				end = start + itemsPerPage;
				paginationService.setItemsPerPage(paginationId, itemsPerPage);

				return collection.slice(start, end);
			} else {
				return collection;
			}
		};
	}

	/**
	 * This service allows the various parts of the module to communicate and stay in sync.
	 */
	function paginationService() {

		var instances = {};
		var lastRegisteredInstance;

		this.registerInstance = function (instanceId) {
			if (typeof instances[instanceId] === 'undefined') {
				instances[instanceId] = {
					asyncMode: false
				};
				lastRegisteredInstance = instanceId;
			}
		};

		this.isRegistered = function (instanceId) {
			return (typeof instances[instanceId] !== 'undefined');
		};

		this.getLastInstanceId = function () {
			return lastRegisteredInstance;
		};

		this.setCurrentPageParser = function (instanceId, val, scope) {
			instances[instanceId].currentPageParser = val;
			instances[instanceId].context = scope;
		};
		this.setCurrentPage = function (instanceId, val) {
			instances[instanceId].currentPageParser.assign(instances[instanceId].context, val);
		};
		this.getCurrentPage = function (instanceId) {
			var parser = instances[instanceId].currentPageParser;
			return parser ? parser(instances[instanceId].context) : 1;
		};

		this.setItemsPerPage = function (instanceId, val) {
			instances[instanceId].itemsPerPage = val;
		};
		this.getItemsPerPage = function (instanceId) {
			return instances[instanceId].itemsPerPage;
		};

		this.setCollectionLength = function (instanceId, val) {
			instances[instanceId].collectionLength = val;
		};
		this.getCollectionLength = function (instanceId) {
			return instances[instanceId].collectionLength;
		};

		this.setAsyncModeTrue = function (instanceId) {
			instances[instanceId].asyncMode = true;
		};

		this.isAsyncMode = function (instanceId) {
			return instances[instanceId].asyncMode;
		};
	}
})();

window.ce=window.ce||{},function(t){"use strict";t.services=function(t,e,r){this.http=t,this.pathRelative=e||"",this.api=r,this.HTTP_OK=200,this.NO_RESPONSE=204,this.UNAUTHORIZED=402,this.NOT_FOUND=404,this.INTERNAL_ERROR=500,this.VALIDATION_ERROR=400},t.services.ConfigurationService=function(e,r){t.services.call(this,e,r),this.categories=null,this.services=null},t.services.ConfigurationService.prototype=new t.services,t.services.ConfigurationService.prototype.getCategories=function(e,r){var a=this;this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/categories",withCredentials:!0,cache:!1}).then(function(s){200===s.status?(a.categories=s.data,e(s.data)):t.services.PortalServices.throwError(r,s.data,s.status)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.ConfigurationService.prototype.getCategoriesForClient=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/clients/"+e+"/categoriesAvailable";this.http({method:"GET",url:s,withCredentials:!0,cache:!1}).then(function(e){200===e.status||204===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.assignCategoryToClient=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+e+"/categories/"+r+"/assign";this.http({method:"PUT",url:o,withCredentials:!0,cache:!1,data:a}).then(function(e){204===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ConfigurationService.prototype.getMessages=function(e,r,a){var s=this.pathRelative+"jaxrs/messages";e&&(s+="?lang="+e.lang),this.http({method:"GET",url:s,withCredentials:!0,cache:!1}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.getConfiguration=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/configuration?all=true";r&&(i+="&lang="+r.lang),null!==e&&(i+="&clientId="+e),i+="&_="+Math.random();var o=this;this.http({method:"GET",url:i,withCredentials:!0,cache:!1}).then(function(e){200===e.status?(o.categories=e.data.categories,o.services=e.data.services,o.mapCategories={},angular.forEach(o.categories,function(t,e){t.numResults=0,o.mapCategories[t.id]=t}),angular.forEach(o.services,function(t,e){t.numResults=0}),a(o.categories,o.mapCategories,o.services,e.data.initLanguage,e.data.clients,e.data.pricesTypes,e.data.user)):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.ConfigurationService.prototype.searchCategory=function(t,e){var r,a=this.categories;return e&&(a=this.services),angular.forEach(a,function(e,a){e.id===t&&(r=e)}),r},t.services.ConfigurationService.prototype.searchSubCategory=function(t,e,r){var a=null;return t&&angular.forEach(t.subCategories,function(t,r){t.id===e&&(a=t)}),a},t.services.ConfigurationService.prototype.getClientGroups=function(e,r){var a=this;this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clientGroups",withCredentials:!0,cache:!1}).then(function(s){200===s.status?(a.categories=s.data,e(s.data)):t.services.PortalServices.throwError(r,s.data,s.status)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.ConfigurationService.prototype.getClientGroup=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clientGroups/"+e,withCredentials:!0,cache:!1}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.updateClientGroup=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/clientGroups/"+e,withCredentials:!0,cache:!1,data:r}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.ConfigurationService.prototype.getClientGroupByDomainName=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clientGroups/domainname/"+e,withCredentials:!0,cache:!1}).then(function(e){200===e.status||204===e.status||304===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.getDynamicType=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/dynamicTypes/"+e;this.http({method:"GET",url:s,withCredentials:!0,cache:!1}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.getDocumentToDynamicType=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+e+"/"+r+"/"+a;this.http({method:"GET",url:o,withCredentials:!0,cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ConfigurationService.prototype.addDocumentToDynamicType=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+r+"/"+e;this.http({method:"POST",url:o,withCredentials:!0,cache:!1,data:a}).then(function(e){200===e.status||204===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ConfigurationService.prototype.deleteDocumentToDynamicType=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+r+"/"+e+"/"+a;this.http({method:"DELETE",url:o,withCredentials:!0,cache:!1}).then(function(e){200===e.status||204===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ConfigurationService.prototype.deleteDynamicType=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/dynamicTypes/"+e;this.http({method:"DELETE",url:s,withCredentials:!0,cache:!1}).then(function(e){200===e.status||204===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.getDynamicTypes=function(e,r){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/dynamicTypes",withCredentials:!0,cache:!1}).then(function(a){200===a.status?e(a.data):t.services.PortalServices.throwError(r,a.data,a.status)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.ConfigurationService.prototype.getDynamicTypesForClient=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/dynamicTypes?clientId="+e;this.http({method:"GET",url:s,withCredentials:!0,cache:!1}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.ConfigurationService.prototype.addDynamicType=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/dynamicTypes";this.http({method:"POST",url:s,withCredentials:!0,cache:!1,data:e}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})}}(window.ce),function(t){"use strict";t.util=t.util||{},t.util.lang=t.util.lang||{},t.util.array=t.util.array||{},t.util.regexForDecimals=new RegExp("^(-)?[0-9]+([,.][0-9]+)?$"),t.util.getClientGroupDomainFromLacation=function(t,e){var r=null,a=t.indexOf(".");if(-1===t.indexOf("www."))r=t.substring(0,a);else{var s=t.indexOf(".",a+1);r=t.substring(a+1,s)}return(!r||!isNaN(parseInt(r,10))||["int","pre","cityexperience"].indexOf(r)>-1)&&(r="arahal"),r},t.util.getClientDomainFromLacation=function(t,e){var r=null;if(/\/#!\/\w+\//.test(e)){var a=e.split("/");r=a[a.indexOf("#!")+1]}else{var s=t.indexOf(".");if(-1===t.indexOf("www."))r=t.substring(0,s);else{var i=t.indexOf(".",s+1);r=t.substring(s+1,i)}}return(!r||!isNaN(parseInt(r,10))||["int","pre","cityexperience"].indexOf(r)>-1)&&(r="arahal"),r},t.util.noCache=function(){return"?_="+Math.random()},t.util.getNextSunday=function(t){var e=new Date(t.getTime()),r=e.getDay(),a=new Date;switch(a.setHours(0),a.setMinutes(0),a.setSeconds(0),r){case 0:a=e;break;case 1:a=e.add(6).day();break;case 2:a=e.add(5).day();break;case 3:a=e.add(4).day();break;case 4:a=e.add(3).day();break;case 5:a=e.add(2).day();break;case 6:a=e.add(1).day()}return a},t.util.getWeekFriday=function(t){var e=new Date(t.getTime()),r=e.getDay(),a=new Date;switch(a.setHours(0),a.setMinutes(0),a.setSeconds(0),r){case 0:a=e.add(-2).day();break;case 1:a=e.add(4).day();break;case 2:a=e.add(3).day();break;case 3:a=e.add(2).day();break;case 4:a=e.add(1).day();break;case 5:a=e;break;case 6:a=e.add(-11).day()}return a},t.util.getWeekMonday=function(t){var e=new Date(t.getTime()),r=e.getDay(),a=new Date;switch(a.setHours(0),a.setMinutes(0),a.setSeconds(0),r){case 0:a=e.add(-6).day();break;case 1:a=e;break;case 2:a=e.add(-1).day();break;case 3:a=e.add(-2).day();break;case 4:a=e.add(-3).day();break;case 5:a=e.add(-4).day();break;case 6:a=e.add(-5).day()}return a},t.util.isSet=function(t){return void 0!==t&&null!==t&&""!==t},t.util.calculateScheduleResume=function(t,e){var r="";void 0!==t&&null!==t&&(t.monday&&(r+=e.msg("common.monday")+", "),t.tuesday&&(r+=e.msg("common.tuesday")+", "),t.wednesday&&(r+=e.msg("common.wednesday")+", "),t.thursday&&(r+=e.msg("common.thrusday")+", "),t.friday&&(r+=e.msg("common.friday")+", "),t.saturday&&(r+=e.msg("common.saturday")+", "),t.sunday&&(r+=e.msg("common.sunday")+", "),","===r.charAt(r.length-2)&&(r=r.substr(0,r.length-2))),t.resume=r},t.util.twoDigitsString=function(t){return void 0===t||null===t?"":2===(t+"").length?""+t:"0"+t},t.util.lang.getCurrentLocale=function(t,e){var r=t.indexOf("lang=");return-1!==r?e[t.substring(r+5,r+7)]:e[window.navigator.userLanguage||window.navigator.language]},t.util.lang.getArrayLanguages=function(){var t=[];return t.es={name:"Español",icon:"assets/img/flags/flag-spain.png",lang:"es"},t.en={name:"English",icon:"assets/img/flags/flag-uk.png",lang:"en"},t.ca={name:"Català",icon:"assets/img/flags/flag-catalonia.png",lang:"ca"},t.it={name:"Italiano",icon:"assets/img/flags/flag-italy.png",lang:"it"},t.pt={name:"Português",icon:"assets/img/flags/flag-brazil.png",lang:"pt"},t.ru={name:"Руccкий",icon:"assets/img/flags/flag-russia.png",lang:"ru"},t.fr={name:"Français",icon:"assets/img/flags/flag-france.png",lang:"fr"},t.de={name:"Deutsch",icon:"assets/img/flags/flag-germany.png",lang:"de"},t.zh={name:"中文",icon:"assets/img/flags/flag-china.png",lang:"zh"},t},t.util.lang.getLocalesAvailablesInClient=function(t,e){var r=[];return null!==t&&null!==t.languages&&angular.forEach(t.languages,function(t,a){void 0!==e[t]&&r.push(e[t])}),r},t.util.array.remove=function(t,e){var r=t.indexOf(e);r>=0&&t.splice(r,1)},t.xhr=function(){function t(t,e){this.http(t).then(function(t){if("object"==typeof e.successCallback){var r=e.successCallback.fullResponse?t:t.data;return e.successCallback.callback(r)}e.successCallback(t.data)}).catch(function(t){r(e.errorCallback,t.data,t.status)})}function e(t){var e=[].slice.call(t);return e.length<2?{}:{url:(e[0]||[]).join("/"),data:e[3],params:e[4],successCallback:e[1],errorCallback:e[2]}}function r(t,e){if(401!==e.status){if(angular.isFunction(t))return t(e.data,e.status);throw new Error(e)}}return{get:function(){var r=e(arguments),a=this.pathRelative+"jaxrs/v2/"+r.url;t.call(this,{method:"GET",url:a,cache:!1,params:r.params},r)},post:function(){var r=e(arguments),a=this.pathRelative+"jaxrs/v2/"+r.url;t.call(this,{method:"POST",url:a,data:r.data,cache:!1,params:r.params},r)},put:function(){var r=e(arguments),a=this.pathRelative+"jaxrs/v2/"+r.url;t.call(this,{method:"PUT",url:a,data:r.data,cache:!1,params:r.params},r)},delete:function(){var r=e(arguments),a=this.pathRelative+"jaxrs/v2/"+r.url;t.call(this,{method:"DELETE",url:a,cache:!1,params:r.params},r)}}}()}(window.ce),function(t){"use strict";t.services.PortalServices=function(e,r,a,s){t.services.call(this,e,a,s),this.contentServices=r},t.services.PortalServices.prototype=new t.services,t.services.PortalServices.throwError=function(t,e,r){if(angular.isFunction(t)&&401!==r)t(e,r);else if(401!==r)throw new Error({data:e,status:r})},t.services.PortalServices.prototype.loginNative=function(t,e,r){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/user/loginNative",withCredentials:!0,cache:!1,data:t}).then(function(t){200===t.status?e(t.data):r()}).catch(function(t){if(angular.isFunction(r))r(t.data,t.status);else if(401!==t.status)throw new Error(t.data)})},t.services.PortalServices.prototype.login=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/user?_="+Math.random(),withCredentials:!0,cache:!1}).then(function(t){if(200===t.status)angular.isDefined(e)&&e(t.data);else if(204===t.status)document.cookie="JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;",a&&a(t.data);else{if(!angular.isDefined(r))throw new Error(t.data);r(t.data)}}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.loginPing=function(e,r){this.http({method:"GET",url:"jaxrs/v2/user/ping?_="+Math.random(),withCredentials:!0,cache:!1}).then(function(t){if(204!==t.status)throw new Error(t.data)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.logout=function(e,r){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/user/logout",withCredentials:!0,cache:!1}).then(function(t){angular.isDefined(e)&&e(t.data)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.getClient=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e,cache:!1}).then(function(t){if(200===t.status){if(t.data.dynamicTypes&&t.data.dynamicTypes.length>0)for(var e=0;e<t.data.dynamicTypes.length;e++)t.data.dynamicTypes[e].jsonSchema=JSON.parse(t.data.dynamicTypes[e].jsonSchema),t.data.dynamicTypes[e].formDefinition=JSON.parse(t.data.dynamicTypes[e].formDefinition);r(t.data)}else{if(!angular.isDefined(a))throw new Error(t.data);a(t.data)}}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getClients=function(e,r){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/",cache:!1}).then(function(t){if(200===t.status)e(t.data);else{if(!angular.isDefined(r))throw new Error(t.data);r(t.data)}}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.getAllClients=function(e,r){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/all",cache:!1}).then(function(t){if(200===t.status)e(t.data);else{if(!angular.isDefined(r))throw new Error(t.data);r(t.data)}}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.getClientByDomainName=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/domainname/"+e;r&&(i+="?lang="+r.lang),this.http({method:"GET",url:i,cache:!1}).then(function(t){if(200===t.status)a(t.data);else{if(!angular.isDefined(s))throw new Error(t.data);s(t.data)}}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getContentCreators=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contentCreators",cache:!1}).then(function(e){200===e.status?r(e.data):t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.searchMyRoutes=function(e,r,a,s){this.http({method:"GET",url:"jaxrs/v2/users/"+e+"/routes?clientId="+r,withCredentials:!0,cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.printElement=function(e,r,a){this.http({method:"GET",url:e+"/pdf",withCredentials:!0,cache:!1}).then(function(t){}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getRouteEntity=function(e,r,a,s){this.http({method:"GET",url:"jaxrs/clients/"+e+"/routes/"+r,cache:!1}).then(function(e){200===e.data.statusRequest?a(e.data.result):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.deleteRoute=function(e,r,a,s){this.http({method:"DELETE",url:"jaxrs/v2/clients/"+e+"/routes/"+r,cache:!1}).then(function(e){204===e.status?a():t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.removeRouteToUserRoutes=function(e,r,a){this.http({method:"DELETE",url:"jaxrs/v2/user/routes/"+e,cache:!1}).then(function(e){200===e.status?r():t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.addRouteToUserRoutes=function(e,r,a){this.http({method:"PUT",url:"jaxrs/v2/user/routes/"+e,cache:!1}).then(function(e){200===e.status?r():t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.invalidateAll=function(e,r){this.http({method:"GET",url:"jaxrs/caches/invalidateAll",withCredentials:!0,cache:!1}).then(function(t){e()}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.propagateImage=function(e,r,a,s){this.http({method:"POST",url:"jaxrs/v3/images/"+e+"/propagate",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getClientContacts=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contacts",cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.addClientContact=function(e,r,a,s){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contacts",data:r,withCredentials:!0,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getClientContactsGroups=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contactGroups",withCredentials:!0,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.addClientContactGroup=function(e,r,a,s){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contactGroups",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.deleteContact=function(e,r,a,s){this.http({method:"DELETE",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contacts/"+r,cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.deleteContactGroup=function(e,r,a,s){this.http({method:"DELETE",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/contactGroups/"+r,cache:!1}).then(function(e){204===e.status?a():t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getClientUsers=function(e,r,a,s){(s=s||{})._=Math.random();var i=[];for(var o in s)s.hasOwnProperty(o)&&i.push(encodeURIComponent(o)+"="+encodeURIComponent(s[o]));this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/users?"+i.join("&"),cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.updateClientsForUser=function(e,r,a,s){var i={attractions:null,clients:r,roles:null};this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/changeClients",data:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.updateAttractionsForUser=function(e,r,a,s){var i={attractions:r,clients:null,roles:null};this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/changeAttractions",data:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.updateUserAllowEmail=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/changeAllowEmail",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.changeAttractions=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/changeAttractions",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.addUser=function(e,r,a,s){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/users/clients/"+e,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.editUser=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/clients/"+e,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.deleteUser=function(e,r,a){this.http({method:"DELETE",url:this.pathRelative+"jaxrs/v2/users/"+e,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.resetUserPassword=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/clients/"+r+"/resetPassword",cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.resetPassword=function(e,r,a){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/resetPassword",data:e,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.changePassword=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/users/"+e+"/changePassword",data:r,withCredentials:!0,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getUser=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/users/"+e,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getLoyaltyCard=function(e,r,a){t.xhr.get.call(this,["users",e,"loyaltyCard"],r,a)},t.services.PortalServices.prototype.getPrivateUsers=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/privateUsers",withCredentials:!0,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.sendByEmail=function(e,r,a,s,i,o){var c;c="ROUTE"===e?"jaxrs/v2/clients/"+r+"/routes/"+a+"/mail":"jaxrs/v2/clients/"+r+"/attractions/"+a+"/mail",this.http({method:"POST",url:c,data:s,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.sendNewsletter=function(e,r,a,s){var i="jaxrs/v2/clients/"+e+"/newsletter",o=angular.copy(r);angular.forEach(o.selectedContacts,function(t,e){t.id=parseInt(t.id)}),this.http({method:"POST",url:i,data:o,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.saveReservation=function(e,r,a){this.http({method:"POST",url:"jaxrs/v2/reservations/",data:e,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getReservations=function(e,r,a){var s="jaxrs/v2/reservations/client/"+e;this.http({method:"GET",url:this.pathRelative+s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.updateClient=function(e,r,a,s){this.http({method:"PUT",url:this.pathRelative+"jaxrs/v2/clients/"+e,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getImagesSliders=function(e,r,a){var s="jaxrs/v2/clients/"+e+"/imageSlider";this.http({method:"GET",url:this.pathRelative+s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.updateImagesSlider=function(e,r,a,s){var i="jaxrs/v2/clients/"+e+"/imageSlider";this.http({method:"POST",url:this.pathRelative+i,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.createPayment=function(e,r,a){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/payments",data:e,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getPayments=function(e,r){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/payments",cache:!1}).then(function(t){e(t.data)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.messagePush=function(e,r,a,s){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clientGroups/"+e+"/pushMessages",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.addMarketingCampaign=function(e,r,a,s){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/marketingCampaigns",data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getMarketingCampaigns=function(e,r,a){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/marketingCampaigns"}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getMarketingCampaign=function(e,r,a,s){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/marketingCampaigns/"+r}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getCaches=function(e,r){this.http({method:"GET",url:this.pathRelative+"jaxrs/caches"}).then(function(t){e(t.data)}).catch(function(e){t.services.PortalServices.throwError(r,e.data,e.status)})},t.services.PortalServices.prototype.getData=function(e,r,a){this.http({method:"GET",url:e}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getContentWithWeight=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/clients/"+e+"/contentWithWeight";this.http({method:"GET",url:s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getParkings=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/clients/"+e+"/parkings";this.http({method:"GET",url:s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getAllLoyalCustomers=function(e,r,a,s){var i={_:Math.random(),resume:!!s};t.xhr.get.call(this,["clients",e,"loyalCustomers"],r,a,null,i)},t.services.PortalServices.prototype.getLoyalCustomers=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers?_="+Math.random();this.http({method:"GET",url:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getLoyalCustomer=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/"+a+"?_="+Math.random();this.http({method:"GET",url:o,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.addLoyaltyAmount=function(e,r,a,s,i,o){var c=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/"+a+"/loyalty";this.http({method:"POST",url:c,data:s,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.reedem=function(e,r,a,s,i,o){var c=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/"+a+"/redeem",n={amount:s};this.http({method:"POST",url:c,data:n,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.loyalty=function(e,r,a,s,i,o){var c=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/"+a+"/loyalty",n={amount:s};this.http({method:"POST",url:c,data:n,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.loyaltyReminder=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/reminder";this.http({method:"POST",url:o,data:a,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.loyaltyResume=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/resume?_="+Math.random();this.http({method:"GET",url:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getLoyaltyAttactions=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/clients/"+e+"/loyaltyAttractions?_="+Math.random();this.http({method:"GET",url:s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.getEmployees=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/employee?_="+Math.random();this.http({method:"GET",url:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.addCustomer=function(e,r,a,s,i){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/customer",data:a,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.addEmployee=function(e,r,a,s,i){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/employees",data:a,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.deletePurchase=function(e,r,a,s,i,o){this.http({method:"DELETE",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyalCustomers/"+a+"/purchases/"+s,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.getPurchases=function(e,r,a,s){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases?_="+Math.random(),cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.getPurchasesMonthReport=function(e,r,a,s){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchasesMonthReport?_="+Math.random(),cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.purchase=function(e,r,a,s,i){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases/",data:a,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.userPurchase=function(e,r,a,s,i,o,c){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases/"+a+"/user/"+s+"?redeem="+!!i,cache:!1}).then(function(t){o(t.data)}).catch(function(e){t.services.PortalServices.throwError(c,e.data,e.status)})},t.services.PortalServices.prototype.checkPurchase=function(e,r,a,s,i){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases/"+a+"?_="+Math.random(),cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.confirmPurchase=function(e,r,a,s,i){this.http({method:"POST",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases/"+a+"/confirm",cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.getGroups=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/clients/"+e+"/groups";this.http({method:"GET",url:s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.addGroup=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/groups";this.http({method:"POST",url:i,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.updateGroup=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/groups/"+r.id;this.http({method:"PUT",url:i,data:r,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.updateConfiguration=function(e,r,a,s,i){var o=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyaltyConfiguration";this.http({method:"PUT",url:o,data:a,cache:!1}).then(function(t){s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.PortalServices.prototype.getConfiguration=function(e,r,a,s){var i=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/loyaltyConfiguration?_="+Math.random();this.http({method:"GET",url:i,cache:!1}).then(function(t){a(t.data)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.PortalServices.prototype.assignTicket=function(e,r,a,s,i,o){var c=this.pathRelative+"jaxrs/v2/clients/"+e+"/attractions/"+r+"/purchases/"+a+"/ticket";this.http({method:"PUT",url:c,data:s,cache:!1}).then(function(t){i(t.data)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.PortalServices.prototype.decodeQrToken=function(e,r,a){var s=this.pathRelative+"jaxrs/v2/tokens/"+e;this.http({method:"GET",url:s,cache:!1}).then(function(t){r(t.data)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.PortalServices.prototype.associateCard=function(e,r,a,s,i){var o={userId:e,forceReassign:!!a};t.xhr.put.call(this,["tokens",r],s,i,null,o)},t.services.PortalServices.prototype.unifyCustomers=function(e,r,a,s,i){t.xhr.put.call(this,["users",e,"merge",r],s,i,null,a)},t.services.PortalServices.prototype.getGiftVouchers=function(e,r,a,s){var i={attractionId:r,_:Math.random()};t.xhr.get.call(this,["clients",e,"giftVouchers"],a,s,null,i)},t.services.PortalServices.prototype.addGiftVoucher=function(e,r,a,s){t.xhr.post.call(this,["clients",e,"giftVouchers"],a,s,r)},t.services.PortalServices.prototype.getGiftVoucherByCode=function(e,r,a,s){var i={_:Math.random()};t.xhr.get.call(this,["clients",e,"giftVouchers",r],a,s,null,i)},t.services.PortalServices.prototype.activateGiftVoucher=function(e,r,a,s){t.xhr.put.call(this,["clients",e,"giftVouchers",r,"activate"],a,s)},t.services.PortalServices.prototype.deactivateGiftVoucher=function(e,r,a,s){t.xhr.delete.call(this,["clients",e,"giftVouchers",r],a,s)},t.services.PortalServices.prototype.assignGiftVoucher=function(e,r,a,s,i){t.xhr.put.call(this,["clients",e,"giftVouchers",r,"user",a],s,i)},t.services.PortalServices.prototype.simulateLoyalty=function(e,r,a,s,i,o){t.xhr.post.call(this,["clients",e,"attractions",r,"loyalCustomers",a,"loyalty","simulate"],i,o,s)},t.services.PortalServices.prototype.getClientStatistics=function(e,r,a){var s={_:Math.random()};t.xhr.get.call(this,["clients",e,"loyaltyStatistics"],r,a,null,s)}}(window.ce),function(t){"use strict";t.services.ContentServices=function(e,r,a,s){t.services.call(this,e,a),this.configurationServices=r,this.config=s},t.services.ContentServices.prototype=new t.services,t.services.ContentServices.prototype.getContent=function(e,r,a,s,i,o){var c=e.selectedClient.id,n=e.mapCategories,u=a.privateCreator,h=a.publicCreator,l=this,v="jaxrs/v2/clients/"+c+"/content";v+=!0===u?"?privateCreator=true":"?privateCreator=false",(h||u)&&(v+="&time="+(new Date).getTime()),r&&(v+="&lang="+r.lang),this.http({method:"GET",url:this.pathRelative+v,cache:!1}).then(function(r){if(200===r.status){var c=l.fillIdsbyElements(r.data,u,h,s,n,e,a),v=0;e.mapCategories1={},e.mapCategories2={};var d=0;for(var p in n)n[p].numResults>0&&d++;var f=Math.ceil(d/2);for(var g in n)n[g].numResults>0&&(v++,d<13||d>=13&&v<=f?e.mapCategories1[g]=n[g]:e.mapCategories2[g]=n[g]);e.categoriesUsed=d,i(c)}else t.services.PortalServices.throwError(o,r.data,r.status)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.ContentServices.prototype.getPrivateContent=function(e,r,a,s,i){var o=e.selectedClient.id,c=e.mapCategories,n="jaxrs/v2/clients/"+o+"/content/pending?date="+(new Date).getTime();r&&(n+="&lang="+r.lang);var u=this;this.http({method:"GET",url:n,cache:!1}).then(function(r){if(200===r.status){var o=u.fillIdsbyElements(r.data,!1,!0,null,null,e,a);angular.forEach(c,function(t,e){t.numResults=0,angular.forEach(o,function(e,r){e.idCategory===t.id&&(t.numResults=t.numResults+1)})}),s(o)}else t.services.PortalServices.throwError(i,r.data,r.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ContentServices.prototype.getArchivedContent=function(e,r,a,s,i){var o=e.selectedClient.id,c=e.mapCategories,n="jaxrs/v2/clients/"+o+"/content/archived?date="+(new Date).getTime();r&&(n+="&lang="+r.lang);var u=this;this.http({method:"GET",url:n,cache:!1}).then(function(r){if(200===r.status){var o=u.fillIdsbyElements(r.data,!1,!0,null,null,e,a);angular.forEach(c,function(t,e){t.numResults=0,angular.forEach(o,function(e,r){e.idCategory===t.id&&(t.numResults=t.numResults+1)})}),s(o)}else t.services.PortalServices.throwError(i,r.data,r.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ContentServices.prototype.getInfoSharedElement=function(e,r,a,s,i){var o=this,c="jaxrs/v2/shared";if(r)c+="?routeId="+r;else{if(!a)return null;c+="?attractionId="+a}e&&(c+="&userId="+e),this.http({method:"GET",url:c,cache:!1}).then(function(t){o.setMarkers(t.data.sharedElement),"ROUTE"===t.data.sharedElement.type&&t.data.sharedElement.route.attractions&&t.data.sharedElement.route.attractions.length>0&&angular.forEach(t.data.sharedElement.route.attractions,function(t,e){o.setMarkers(t)}),s(t.data)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.ContentServices.prototype.fillIdsbyElements=function(e,r,a,s,i,o,c){var n=Date.today(),u=Date.today().add(1).day(),h=(Date.today().add(1).day(),Date.today().add(7).day()),l=Date.today().add({months:1}).day(),v=t.util.getWeekFriday(n),d=t.util.getNextSunday(n);d.setHours(23);t.util.getWeekMonday(n);var p=n.getFullYear(),f=n.getMonth();new Date(p,f,1),new Date(p,f+1,0);angular.forEach(i,function(t){t.numResults=0});var g=this;return angular.forEach(e,function(o){if(g.setMarkers(o),i&&void 0!==o.idCategory&&5!==o.status&&i[o.idCategory]&&(i[o.idCategory].numResults=i[o.idCategory].numResults+1),i&&void 0!==o.idCategory2&&5!==o.status&&o.idCategory!==o.idCategory2&&i[o.idCategory2]&&(i[o.idCategory2].numResults=i[o.idCategory2].numResults+1),a?3===o.status?o.statusIcon="watch":5===o.status?(o.statusIcon="tool",s.building++):6===o.status&&(o.statusIcon="folder"):r&&(1===o.status?o.statusIcon="check":3===o.status?o.statusIcon="watch":5===o.status?(o.statusIcon="tool",s.building++):6===o.status&&(o.statusIcon="folder")),"ATTRACTION"===o.type&&(o.attraction.activities=g.completeActivitiesElements(o.attraction.activitiesUrls,e),null===s||1!==o.status||o.attraction.comercio||s.attractions++,null!==s&&1===o.status&&o.attraction.comercio&&s.shops++,o.wrapperPriceRange=o.attraction.priceRange),"ACTIVITY"===o.type){var p=g.completeAttractionElement(o.activity.attractionParentUrl,e,!1);p&&(o.activity.attractionParent=p),null!==s&&1===o.status&&(s.activities=s.activities+1);var f=new Date(o.activity.startTime);f.setHours(0);var m=new Date(o.activity.endTime);if(m.setHours(0),o.today=n.between(f,m),o.tomorrow=u.between(f,m),o.thisWeekend=f<d&&m>v,o.thisWeek=f<h&&m>n,o.thisMonth=f<l&&m>n,o.today){o.today=!1;for(var y=0,S=n.getDay();y<o.activity.schedules.length&&!o.today;){var E=o.activity.schedules[y];o.today=0===S&&E.sunday||1===S&&E.monday||2===S&&E.tuesday||3===S&&E.wednesday||4===S&&E.thursday||5===S&&E.friday||6===S&&E.saturday,y++}}if(o.tomorrow){o.tomorrow=!1;for(var P=0,w=u.getDay();P<o.activity.schedules.length&&!o.tomorrow;){var C=o.activity.schedules[P];o.tomorrow=0===w&&C.sunday||1===w&&C.monday||2===w&&C.tuesday||3===w&&C.wednesday||4===w&&C.thursday||5===w&&C.friday||6===w&&C.saturday,P++}}if(o.thisWeekend){o.thisWeekend=!1;var T=0;for(n.getDay();T<o.activity.schedules.length&&!o.thisWeekend;){var R=o.activity.schedules[T];o.thisWeekend=R.sunday||R.saturday,T++}}null!==s&&1===o.status&&(o.today&&s.today++,o.tomorrow&&s.tomorrow++,o.thisWeekend&&s.thisWeekend++,o.thisWeek&&s.thisWeek++,o.thisMonth&&s.thisMonth++),o.wrapperPriceRange=o.activity.priceRange}if("ROUTE"===o.type)o.route.attractions=g.completeAttractionElements(o.route.attractionsUrls,e),null!==s&&s.routes++;else if("OFFER"===o.type&&null!==o.offer.parentUrl&&void 0!==o.offer.parentUrl){if("ATTRACTION"===o.offer.parentType){var x=o.offer.parentUrl.substring(o.offer.parentUrl.lastIndexOf("/")+1);if(o.offer.parentId=x,o.offer.parent=g.completeAttractionElement(o.offer.parentUrl,e,!1),o.offer.parent){if(o.offer.parent.activity){var j=g.completeAttractionElement(o.offer.parent.activity.attractionParentUrl,e,!1);j&&(o.offer.parent.activity.attractionParent=j)}}else console.log("Cannot find parent for offer "+o.id+" for parent "+o.offer.parentUrl)}null!==s&&5!==o.status&&s.offers++}else"DYNAMIC"===o.type&&null!==s&&s.dynamic++;o.canModify=t.security.userCanModify(c.globalCreator,c.publicCreator,c.privateCreator,c.userLoged,o)}),e},t.services.ContentServices.prototype.setImagesUrls=function(e,r){t.util.isSet(e)?t.util.isSet(e.urls)?(t.util.isSet(e.urls.m_web)||(t.util.isSet(e.urls.thumb2)?e.urls.m_web=e.urls.thumb2:e.urls.m_web="assets/img/defaultImages/neutral-image.jpg"),t.util.isSet(e.urls.l_web)||(t.util.isSet(e.urls.thumb1)?e.urls.l_web=e.urls.thumb1:e.urls.l_web="assets/img/defaultImages/neutral-image.jpg")):e.urls={m_web:"assets/img/defaultImages/neutral-image.jpg",l_web:"assets/img/defaultImages/neutral-image.jpg"}:e={urls:{m_web:"assets/img/defaultImages/neutral-image.jpg",l_web:"assets/img/defaultImages/neutral-image.jpg"}}},t.services.ContentServices.prototype.setMarkers=function(e){if(t.services.ContentServices.prototype.setImagesUrls(e.mainImage,e.idClient),e.images&&e.images.length>0&&angular.forEach(e.images,function(r,a){t.services.ContentServices.prototype.setImagesUrls(r,e.idClient)}),"ROUTE"===e.type)e.iconActive=this.config.staticPath+"img/pins/pin-m-active-route.png",e.iconInactive=this.config.staticPath+"img/pins/pin-m-inactive-route.png",e.categoryIcon="assets/img/icon-route-green.png",e.categoryName="";else if("OFFER"===e.type)e.iconActive=this.config.staticPath+"img/pins/pin-m-active-deal1.png",e.iconInactive=this.config.staticPath+"img/pins/pin-m-inactive-deal1.png";else{var r=null,a=this.configurationServices.searchCategory(e.idCategory,"SERVICE"===e.type);if(a)if(null!==e.idSubCategory&&""!==e.idSubCategory){var s=this.configurationServices.searchSubCategory(a,e.idSubCategory,"SERVICE"===e.type);r=s?s.icons:a.icons}else r=a.icons;null!==r?(e.iconActive=r.pin_m_active,e.iconInactive=r.pin_m_inactive,e.categoryIcon=r.icon_m,e.categoryName=a.name):(e.iconActive=this.config.staticPath+"img/pins/default-icon.jpg",e.iconInactive=this.config.staticPath+"img/pins/default-icon.jpg",e.categoryIcon=null,e.categoryName="")}},t.services.ContentServices.prototype.completeOfferElements=function(t,e){var r=this;if(t&&t.length>0){var a=[];return angular.forEach(t,function(t,s){var i=t.substring(t.lastIndexOf("/")+1),o=r.findElementInResultSearch("OFFER",i,e);o&&a.push(angular.copy(o))}),a}return[]},t.services.ContentServices.prototype.completeAttractionElements=function(t,e){var r=this;if(t&&t.length>0){var a=[];return angular.forEach(t,function(t,s){var i=r.completeAttractionElement(t,e,!0);if(i){if("ACTIVITY"===i.type){var o=r.completeAttractionElement(i.activity.attractionParentUrl,e,!1);o&&(i.activity.attractionParent=o)}r.setMarkers(i),a.push(i)}}),a}return[]},t.services.ContentServices.prototype.completeActivitiesElements=function(t,e){var r=this;if(t&&t.length>0){var a=[];return angular.forEach(t,function(t,s){var i=t.substring(t.lastIndexOf("/")+1),o=r.findElementInResultSearch("ACTIVITY",i,e);o&&(r.setMarkers(o),a.push(angular.copy(o)))}),a}return[]},t.services.ContentServices.prototype.completeAttractionElement=function(t,e,r){var a=this,s=t.substring(t.lastIndexOf("/")+1),i=a.findElementInResultSearch("ATTRACTION",s,e);if(i)r&&(i.attraction.activities=a.completeActivitiesElements(i.attraction.activitiesUrls,e)),i=i;else{var o=a.findElementInResultSearch("ACTIVITY",s,e);o&&(i=o)}return angular.copy(i)},t.services.ContentServices.prototype.findElementInResultSearch=function(t,e,r){for(var a=0,s=null;a<r.length&&null===s;)r[a].type===t&&r[a].id===parseInt(e)&&(s=r[a]),a+=1;return s}}(window.ce),function(t){"use strict";window.ce.services.MapService=function(){function t(t){var e=a.filter(function(e){return e.contentCE.id===t.id&&e.contentCE.type===t.type});if(e.length)return e[0]}function e(t){var e=s.filter(function(e){return e.contentCE.id===t.id&&e.contentCE.type===t.type});if(e.length)return e[0]}var r=[],a=[],s=[];this.map=null,this.mapOptions={mapTypeId:google.maps.MapTypeId.ROADMAP,zoomControlOptions:{style:google.maps.ZoomControlStyle.SMALL},center:new google.maps.LatLng(37.2621077,-5.5455903),zoom:15,styles:[{featureType:"poi.business",stylers:[{visibility:"off"}]}]},this.createMarker=function(t,s,i,o){if(r.indexOf(t)>-1)console.log("elemento ya añadido");else{r.push(t);var c,n;if("ROUTE"===t.type&&t.route?(c=t.route.latitude,n=t.route.longitude):"ACTIVITY"===t.type&&t.activity&&t.activity.attractionParent&&t.activity.attractionParent.attraction?(c=t.activity.attractionParent.attraction.latitude,n=parseFloat(t.activity.attractionParent.attraction.longitude)+6e-5):"OFFER"===t.type&&t.offer&&t.offer.parent?t.offer.parent.attraction?(c=t.offer.parent.attraction.latitude,n=t.offer.parent.attraction.longitude-6e-5):t.offer.parent.activity&&t.offer.parent.activity.attractionParent?(c=t.offer.parent.activity.attractionParent.attraction.latitude,n=t.offer.parent.activity.attractionParent.attraction.longitude-6e-5):console.log("error en parent type for offer",t.id,"- ",t.offer.parentUrl):t.attraction?(c=t.attraction.latitude,n=t.attraction.longitude):console.log("No se ha encontrado coordenadas para el contenido",t.id),c&&n){c=(""+c).split(",").join(""),n=(""+n).split(",").join("");var u=new google.maps.Marker({contentCE:t,iconActive:t.iconActive,iconInactive:t.iconInactive,icon:t.iconInactive,map:this.map,position:new google.maps.LatLng(c,n),title:t.name});u.setZIndex("ROUTE"===t.type?1.5:0),a.push(u),google.maps.event.addListener(u,"mouseover",function(){if(o(u.contentCE,!0),u.setIcon(u.iconActive),"ROUTE"===u.contentCE.type&&u.contentCE.route.attractions&&u.contentCE.route.attractions.length){u.contentCE.route.attractions.forEach(function(t){h.setMarkerStatus(t,!0)});var t=e(u.contentCE);t&&t.setOptions({strokeOpacity:1})}}),google.maps.event.addListener(u,"mouseout",function(){if(o(u.contentCE,!1),u.setIcon(u.iconInactive),"ROUTE"===u.contentCE.type&&u.contentCE.route.attractions&&u.contentCE.route.attractions.length){u.contentCE.route.attractions.forEach(function(t){h.setMarkerStatus(t,!1)});var t=e(u.contentCE);t&&t.setOptions({strokeOpacity:.1})}}),google.maps.event.addListener(u,"click",function(){s(u.contentCE)}),google.maps.event.addListener(u,"dblclick",function(){i(u.contentCE)});var h=this}}},this.createRoute=function(t,e,r,a){var i;if(t&&t.route&&t.route.overviewPolyline){var o=google.maps.geometry.encoding.decodePath(t.route.overviewPolyline),c={path:google.maps.SymbolPath.FORWARD_CLOSED_ARROW};(i=new google.maps.Polyline({path:o,strokeColor:"#FF0000",strokeOpacity:.1,strokeWeight:2,contentCE:t,icons:[{icon:c,repeat:"25%",offset:"100%"}]})).setOptions({strokeOpacity:.1}),s.push(i),google.maps.event.addListener(i,"mouseover",function(){a(i.contentCE,!0),i.setOptions({strokeOpacity:1})}),google.maps.event.addListener(i,"mouseout",function(){a(i.contentCE,!1),i.setOptions({strokeOpacity:.1})}),google.maps.event.addListener(i,"click",function(){e(i.contentCE)}),google.maps.event.addListener(i,"dblclick",function(){r(i.contentCE)})}},this.clearMap=function(){a.forEach(function(t){t.setMap(null)}),a.splice(0),r.splice(0),s.forEach(function(t){t.setMap(null)}),s.splice(0)},this.resizeMap=function(){google.maps.event.trigger(this.map,"resize")},this.initMap=function(t,e,r,i){if(this.map){var o=this;if(s.forEach(function(t){t.setMap(o.map)}),a.length){var c=new google.maps.LatLngBounds;a.forEach(function(t){c.extend(t.position)}),this.map.fitBounds(c),google.maps.event.addListenerOnce(this.map,"idle",function(){o.map.getZoom()>i&&o.map.setZoom(i)})}else t&&e&&(this.map.setCenter(new google.maps.LatLng(t,e)),this.map.setZoom(r))}},this.setMarkerStatus=function(r,a){var s=t(r);if(s&&(s.setIcon(a?s.iconActive:s.iconInactive),s.setZIndex("ROUTE"===r.type?1.5:a?1:0)),"ROUTE"===r.type){var i=e(r);i&&i.setOptions({strokeOpacity:a?1:.1}),r.route.attractions&&r.route.attractions.length&&r.route.attractions.filter(function(e){var r=t(e);r&&(r.setIcon(a?r.iconActive:r.iconInactive),r.setZIndex(a?1:0))})}}}}(),function(t){"use strict";t.services.AttractionServices=function(e,r){t.services.call(this,e,r)},t.services.AttractionServices.prototype=new t.services,t.services.AttractionServices.prototype.deleteAttraction=function(e,r,a,s){this.http({method:"DELETE",url:"jaxrs/v2/clients/"+e+"/attractions/"+r,cache:!1}).then(function(t){if(204===t.status)a();else{if(!angular.isDefined(s))throw new Error(t.data);s(t.data)}}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.addAttraction=function(e,r,a,s,i,o){this.http({method:"POST",url:"jaxrs/clients/"+e+"/attractions?translate="+a+"&publish="+s,data:r,cache:!1}).then(function(e){200===e.status&&200===e.data.statusRequest?i(e.data):t.services.PortalServices.throwError(o,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(o,e.data,e.status)})},t.services.AttractionServices.prototype.addAttractionToRoute=function(e,r,a,s){var i="jaxrs/v2/user/routes/"+e.id+"/attractions/"+r;this.http({method:"PUT",url:i,data:e,cache:!1}).then(function(e){200===e.status?a():t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.deleteAttractionToRoute=function(e,r,a,s){var i="jaxrs/v2/user/routes/"+e+"/attractions/"+r;this.http({method:"DELETE",url:i,cache:!1}).then(function(e){200===e.status?a():t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.getAttractionCE=function(e,r,a,s){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+r+"/attractions/"+e,cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.getAttractionEntity=function(e,r,a,s){this.http({method:"GET",url:"jaxrs/clients/"+e+"/attractions/"+r,cache:!1}).then(function(e){200===e.data.statusRequest?a(e.data.result):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.acceptAttraction=function(e,r,a,s){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/attractions/"+r+"/accept",cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.acceptDynamic=function(e,r,a,s,i){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/"+r+"/documents/"+a+"/accept",cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.publishAttraction=function(e,r,a,s){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/attractions/"+r+"/publish",cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.rejectAttraction=function(e,r,a,s){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/attractions/"+r+"/reject",cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.transferAttraction=function(e,r,a,s,i){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/attractions/"+r+"/transfer/"+a,cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.getOfferEntity=function(e,r,a,s){var i="jaxrs/v2/clients/"+r+"/offers/"+e;this.http({method:"GET",url:i,cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.AttractionServices.prototype.deleteOffer=function(e,r,a,s,i){var o="jaxrs/v2/clients/"+a+"/attractions/"+r+"/offers/"+e;this.http({method:"DELETE",url:o,cache:!1}).then(function(e){204===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.addOffer=function(e,r,a,s,i){var o="jaxrs/v2/clients/"+r+"/attraction/"+e+"/offers/";this.http({method:"POST",url:o,data:a,cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.archiveDynamic=function(e,r,a,s,i){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/"+r+"/documents/"+a+"/archive",cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.archiveContent=function(e,r,a,s,i){var o="jaxrs/v2/clients/"+e+"/"+r+"/"+a+"/archive";this.http({method:"PUT",url:o,data:{},cache:!1}).then(function(e){if(200===e.status)return s(e.data);t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.archiveOffer=function(e,r,a,s,i){var o="jaxrs/v2/clients/"+e+"/attractions/"+r+"/offers/"+a+"/archive";this.http({method:"PUT",url:o,data:{},cache:!1}).then(function(e){if(200===e.status)return s(e.data);t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.publishOffer=function(e,r,a,s,i){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/attractions/"+r+"/offers/"+a+"/publish",cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.AttractionServices.prototype.publishDocument=function(e,r,a,s,i){this.http({method:"PUT",url:"jaxrs/v2/clients/"+e+"/"+r+"/documents/"+a+"/publish",cache:!1}).then(function(e){200===e.status?s(e.data):t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})}}(window.ce),function(t){"use strict";t.services.RouteServices=function(e,r,a){t.services.call(this,e,a),this.contentServices=r},t.services.RouteServices.prototype=new t.services,t.services.RouteServices.prototype.searchMyRoutes=function(e,r,a,s,i){var o=this;this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/users/"+e+"/routes?clientId="+r,withCredentials:!0,cache:!1}).then(function(e){if(200===e.status){var r=e.data,c=[];angular.forEach(r,function(t,e){var r=null;if(angular.isObject(t))r=t;else if(angular.isString(t)){var s=t.substring(t.lastIndexOf("/")+1);r=o.contentServices.findElementInResultSearch("ROUTE",s,a)}r&&(r.route.attractions=o.contentServices.completeAttractionElements(r.route.attractionsUrls,a),o.contentServices.setMarkers(r),c.push(r))}),s(c)}else t.services.PortalServices.throwError(i,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})},t.services.RouteServices.prototype.getRouteEntity=function(e,r,a,s){this.http({method:"GET",url:"jaxrs/clients/"+e+"/routes/"+r,cache:!1}).then(function(e){200===e.data.statusRequest?a(e.data.result):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.RouteServices.prototype.deleteRoute=function(e,r,a,s){this.http({method:"DELETE",url:"jaxrs/v2/clients/"+e+"/routes/"+r,cache:!1}).then(function(e){204===e.status?a():t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.RouteServices.prototype.removeRouteToUserRoutes=function(e,r,a){this.http({method:"DELETE",url:"jaxrs/v2/user/routes/"+e,cache:!1}).then(function(e){200===e.status?r():t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.RouteServices.prototype.addRouteToUserRoutes=function(e,r,a){this.http({method:"PUT",url:"jaxrs/v2/user/routes/"+e,cache:!1}).then(function(e){200===e.status?r():t.services.PortalServices.throwError(a,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(a,e.data,e.status)})},t.services.RouteServices.prototype.getRouteCE=function(e,r,a,s){this.http({method:"GET",url:this.pathRelative+"jaxrs/v2/clients/"+e+"/routes/"+r,cache:!1}).then(function(e){200===e.status?a(e.data):t.services.PortalServices.throwError(s,e.data,e.status)}).catch(function(e){t.services.PortalServices.throwError(s,e.data,e.status)})},t.services.RouteServices.prototype.addRoute=function(e,r,a,s,i){var o="jaxrs/clients/"+e+"/routes?translate="+a;this.http({method:"POST",url:o,data:r,cache:!1}).then(function(t){200===t.status?200===t.data.statusRequest?s(t.data.element):i(400===t.data.statusRequest?"Errores de validacion":"Error creando una ruta"):i("Error creando una ruta")}).catch(function(e){t.services.PortalServices.throwError(i,e.data,e.status)})}}(window.ce),function(t){"use strict";t.security=t.security||{},t.security.isUserCreatorForClient=function(t,e){var r={isCreator:!1,globalCreator:!1,privateCreator:!1,publicCreator:!1,isAdmin:!1,loyalty:!1,loyaltyEmployee:!1,loyaltyCustomerService:!1};if(t&&t.roles&&t.clients){var a=t.roles.split("--"),s=t.clients.split("--"),i=!1,o=!1,c=!1;angular.forEach(a,function(t,e){"creator"===t?(i=!0,r.publicCreator=!0):"privateCreator"===t?(o=!0,r.privateCreator=!0):"admin"===t?c=!0:"loyalty"===t?r.loyalty=!0:"loyaltyEmployee"===t?r.loyaltyEmployee=!0:"loyaltyCustomerService"===t&&(r.loyaltyCustomerService=!0)}),r.isAdmin=c,(i||o)&&angular.forEach(s,function(t,a){"0"===t&&i?(r.isCreator=!0,r.isAdmin=!0,r.globalCreator=!0):t===""+e.id&&(r.isCreator=!0)})}return r},t.security.userCanModify=function(t,e,r,a,s){return!(!s||!a)&&(!!t||("ROUTE"!==s.type||s.route.publicRoute?r?"OFFER"===s.type?!!(s.offer&&s.offer.parent&&s.offer.parent.idUser)&&s.offer.parent.idUser===a.id:s.idUser===a.id:e:s.idUser===a.id))}}(window.ce),function(t){"use strict";t.session=function(){this.loginData={username:null,password:null},this.globalCreator=!1,this.publicCreator=!1,this.privateCreator=!1,this.isCreator=!1,this.userLoged=null,this.isLogged=!1},t.session.prototype.login=function(e,r){if(this.userLoged=e,this.isLogged=!0,r){var a=t.security.isUserCreatorForClient(this.userLoged,r);this.globalCreator=a.globalCreator,this.privateCreator=a.privateCreator,this.isCreator=a.isCreator,this.publicCreator=a.publicCreator}},t.session.prototype.logout=function(){document.cookie="JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;",this.resetValues()},t.session.prototype.resetValues=function(){this.loginData={username:null,password:null},this.userLoged=null,this.isLogged=!1,this.globalCreator=!1,this.publicCreator=!1,this.privateCreator=!1,this.isCreator=!1}}(window.ce),function(t){"use strict";t.repository=t.repository||{},t.repository.ContentRepository=function(t,e,r,a){this.contentServices=t,this.session=e,this.CEConfiguration=a,this.routeServices=r,this.content=[],this.counter={attractions:0,shops:0,activities:0,offers:0,routes:0,today:0,tomorrow:0,thisWeekend:0,thisMonth:0,thisWeek:0,building:0,dynamic:0},this.userRoutes=[]},t.repository.ContentRepository.prototype.getContent=function(t,e,r,a){var s=this;t&&this.session.publicCreator?this.contentServices.getPrivateContent(this.CEConfiguration,r,this.session,function(t){s.initCounter(),s.content=t}):e&&this.session.publicCreator?this.contentServices.getArchivedContent(this.CEConfiguration,r,this.session,function(t){s.initCounter(),s.content=t}):(this.initCounter(),this.contentServices.getContent(this.CEConfiguration,r,this.session,this.counter,function(t){s.content=t,s.getUserRoutes(s.CEConfiguration.selectedClient),a&&a()},function(t){console.log("Error searching content")}))},t.repository.ContentRepository.prototype.getUserRoutes=function(t){if(this.userRoutes=[],this.session.isLogged){var e=this;this.routeServices.searchMyRoutes(this.session.userLoged.id,t.id,this.content,function(t){e.userRoutes=t,angular.forEach(e.userRoutes,function(t,e){t.route.publicRoute?t.canModify=!1:t.canModify=!0})},function(t){console.log("Error searching user routes")})}},t.repository.ContentRepository.prototype.initCounter=function(){this.counter={attractions:0,shops:0,activities:0,offers:0,routes:0,today:0,tomorrow:0,thisWeekend:0,thisMonth:0,thisWeek:0,building:0,dynamic:0}}}(window.ce);