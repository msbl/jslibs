/**
 * dirPagination - AngularJS module for paginating (almost) anything.
 *
 *
 * Credits
 * =======
 *
 * Daniel Tabuenca: https://groups.google.com/d/msg/angular/an9QpzqIYiM/r8v-3W1X5vcJ
 * for the idea on how to dynamically invoke the ng-repeat directive.
 *
 * I borrowed a couple of lines and a few attribute names from the AngularUI Bootstrap project:
 * https://github.com/angular-ui/bootstrap/blob/master/src/pagination/pagination.js
 *
 * Copyright 2014 Michael Bromley <michael@michaelbromley.co.uk>
 */

(function () {

	/**
	 * Config
	 */
	var moduleName = 'angularUtils.directives.dirPagination';
	var DEFAULT_ID = '__default';

	/**
	 * Module
	 */
	var module;
	try {
		module = angular.module(moduleName);
	} catch (err) {
		// named module does not exist, so create one
		module = angular.module(moduleName, []);
	}

	module
		.directive('dirPaginate', ['$compile', '$parse', 'paginationService', dirPaginateDirective])
		.directive('dirPaginateNoCompile', noCompileDirective)
		.directive('dirPaginationControls', ['paginationService', dirPaginationControlsDirective])
		.filter('itemsPerPage', ['paginationService', itemsPerPageFilter])
		.service('paginationService', paginationService);

	function dirPaginateDirective($compile, $parse, paginationService) {

		return {
			terminal: true,
			multiElement: true,
			compile: dirPaginationCompileFn
		};

		function dirPaginationCompileFn(tElement, tAttrs) {

			var expression = tAttrs.dirPaginate;
			// regex taken directly from https://github.com/angular/angular.js/blob/master/src/ng/directive/ngRepeat.js#L211
			var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

			var filterPattern = /\|\s*itemsPerPage\s*:[^|]*/;
			if (match[2].match(filterPattern) === null) {
				throw 'pagination directive: the \'itemsPerPage\' filter must be set.';
			}
			var itemsPerPageFilterRemoved = match[2].replace(filterPattern, '');
			var collectionGetter = $parse(itemsPerPageFilterRemoved);

			addNoCompileAttributes(tElement);

			// If any value is specified for paginationId, we register the un-evaluated expression at this stage for the benefit of any
			// dir-pagination-controls directives that may be looking for this ID.
			var rawId = tAttrs.paginationId || DEFAULT_ID;
			paginationService.registerInstance(rawId);

			return function dirPaginationLinkFn(scope, element, attrs) {

				// Now that we have access to the `scope` we can interpolate any expression given in the paginationId attribute and
				// potentially register a new ID if it evaluates to a different value than the rawId.
				var paginationId = $parse(attrs.paginationId)(scope) || attrs.paginationId || DEFAULT_ID;
				paginationService.registerInstance(paginationId);

				var repeatExpression = getRepeatExpression(expression, paginationId);
				addNgRepeatToElement(element, attrs, repeatExpression);

				removeTemporaryAttributes(element);
				var compiled = $compile(element);

				var currentPageGetter = makeCurrentPageGetterFn(scope, attrs, paginationId);
				paginationService.setCurrentPageParser(paginationId, currentPageGetter, scope);

				if (typeof attrs.totalItems !== 'undefined') {
					paginationService.setAsyncModeTrue(paginationId);
					scope.$watch(function () {
						return $parse(attrs.totalItems)(scope);
					}, function (result) {
						if (0 <= result) {
							paginationService.setCollectionLength(paginationId, result);
						}
					});
				} else {
					scope.$watchCollection(function () {
						return collectionGetter(scope);
					}, function (collection) {
						if (collection) {
							paginationService.setCollectionLength(paginationId, collection.length);
						}
					});
				}

				// Delegate to the link function returned by the new compilation of the ng-repeat
				compiled(scope);
			};
		}

		/**
		 * If a pagination id has been specified, we need to check that it is present as the second argument passed to
		 * the itemsPerPage filter. If it is not there, we add it and return the modified expression.
		 *
		 * @param expression
		 * @param paginationId
		 * @returns {*}
		 */
		function getRepeatExpression(expression, paginationId) {
			var repeatExpression,
				idDefinedInFilter = !!expression.match(/(\|\s*itemsPerPage\s*:[^|]*:[^|]*)/);

			if (paginationId !== DEFAULT_ID && !idDefinedInFilter) {
				repeatExpression = expression.replace(/(\|\s*itemsPerPage\s*:[^|]*)/, "$1 : '" + paginationId + "'");
			} else {
				repeatExpression = expression;
			}

			return repeatExpression;
		}

		/**
		 * Adds the ng-repeat directive to the element. In the case of multi-element (-start, -end) it adds the
		 * appropriate multi-element ng-repeat to the first and last element in the range.
		 * @param element
		 * @param attrs
		 * @param repeatExpression
		 */
		function addNgRepeatToElement(element, attrs, repeatExpression) {
			if (element[0].hasAttribute('dir-paginate-start') || element[0].hasAttribute('data-dir-paginate-start')) {
				// using multiElement mode (dir-paginate-start, dir-paginate-end)
				attrs.$set('ngRepeatStart', repeatExpression);
				element.eq(element.length - 1).attr('ng-repeat-end', true);
			} else {
				attrs.$set('ngRepeat', repeatExpression);
			}
		}

		/**
		 * Adds the dir-paginate-no-compile directive to each element in the tElement range.
		 * @param tElement
		 */
		function addNoCompileAttributes(tElement) {
			angular.forEach(tElement, function (el) {
				if (el.nodeType === Node.ELEMENT_NODE) {
					angular.element(el).attr('dir-paginate-no-compile', true);
				}
			});
		}

		/**
		 * Removes the variations on dir-paginate (data-, -start, -end) and the dir-paginate-no-compile directives.
		 * @param element
		 */
		function removeTemporaryAttributes(element) {
			angular.forEach(element, function (el) {
				if (el.nodeType === Node.ELEMENT_NODE) {
					angular.element(el).removeAttr('dir-paginate-no-compile');
				}
			});
			element.eq(0).removeAttr('dir-paginate-start').removeAttr('dir-paginate').removeAttr('data-dir-paginate-start').removeAttr('data-dir-paginate');
			element.eq(element.length - 1).removeAttr('dir-paginate-end').removeAttr('data-dir-paginate-end');
		}

		/**
		 * Creates a getter function for the current-page attribute, using the expression provided or a default value if
		 * no current-page expression was specified.
		 *
		 * @param scope
		 * @param attrs
		 * @param paginationId
		 * @returns {*}
		 */
		function makeCurrentPageGetterFn(scope, attrs, paginationId) {
			var currentPageGetter;
			if (attrs.currentPage) {
				currentPageGetter = $parse(attrs.currentPage);
			} else {
				// if the current-page attribute was not set, we'll make our own
				var defaultCurrentPage = paginationId + '__currentPage';
				scope[defaultCurrentPage] = 1;
				currentPageGetter = $parse(defaultCurrentPage);
			}
			return currentPageGetter;
		}
	}

	/**
	 * This is a helper directive that allows correct compilation when in multi-element mode (ie dir-paginate-start, dir-paginate-end).
	 * It is dynamically added to all elements in the dir-paginate compile function, and it prevents further compilation of
	 * any inner directives. It is then removed in the link function, and all inner directives are then manually compiled.
	 */
	function noCompileDirective() {
		return {
			priority: 5000,
			terminal: true
		};
	}

	function dirPaginationControlsDirective(paginationService) {

		var numberRegex = /^\d+$/;

		return {
			restrict: 'AE',
			templateUrl: function (elem, attrs) {
				return attrs.templateUrl;
			},
			scope: {
				maxSize: '=?',
				onPageChange: '&?',
				paginationId: '=?'
			},
			link: dirPaginationControlsLinkFn
		};

		function dirPaginationControlsLinkFn(scope, element, attrs) {

			// rawId is the un-interpolated value of the pagination-id attribute. This is only important when the corresponding dir-paginate directive has
			// not yet been linked (e.g. if it is inside an ng-if block), and in that case it prevents this controls directive from assuming that there is
			// no corresponding dir-paginate directive and wrongly throwing an exception.
			var rawId = attrs.paginationId || DEFAULT_ID;
			var paginationId = scope.paginationId || attrs.paginationId || DEFAULT_ID;

			if (!paginationService.isRegistered(paginationId) && !paginationService.isRegistered(rawId)) {
				var idMessage = (paginationId !== DEFAULT_ID) ? ' (id: ' + paginationId + ') ' : ' ';
				throw 'pagination directive: the pagination controls' + idMessage + 'cannot be used without the corresponding pagination directive.';
			}

			if (!scope.maxSize) { scope.maxSize = 9; }
			scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : true;
			scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : false;

			var paginationRange = Math.max(scope.maxSize, 5);
			scope.pages = [];
			scope.pagination = {
				last: 1,
				current: 1
			};
			scope.range = {
				lower: 1,
				upper: 1,
				total: 1
			};

			scope.$watch(function () {
				return (paginationService.getCollectionLength(paginationId) + 1) * paginationService.getItemsPerPage(paginationId);
			}, function (length) {
				if (0 < length) {
					generatePagination();
				}
			});

			scope.$watch(function () {
				return (paginationService.getItemsPerPage(paginationId));
			}, function (current, previous) {
				if (current != previous && typeof previous !== 'undefined') {
					goToPage(scope.pagination.current);
				}
			});

			scope.$watch(function () {
				return paginationService.getCurrentPage(paginationId);
			}, function (currentPage, previousPage) {
				if (currentPage != previousPage) {
					goToPage(currentPage);
				}
			});

			scope.setCurrent = function (num) {
				if (isValidPageNumber(num)) {
					num = parseInt(num, 10);
					paginationService.setCurrentPage(paginationId, num);
				}
			};

			function goToPage(num) {
				if (isValidPageNumber(num)) {
					scope.pages = generatePagesArray(num, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
					scope.pagination.current = num;
					updateRangeValues();

					// if a callback has been set, then call it with the page number as an argument
					if (scope.onPageChange) {
						scope.onPageChange({ newPageNumber: num });
					}
				}
			}

			function generatePagination() {
				var page = parseInt(paginationService.getCurrentPage(paginationId)) || 1;

				scope.pages = generatePagesArray(page, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
				scope.pagination.current = page;
				scope.pagination.last = scope.pages[scope.pages.length - 1];
				if (scope.pagination.last < scope.pagination.current) {
					scope.setCurrent(scope.pagination.last);
				} else {
					updateRangeValues();
				}
			}

			/**
			 * This function updates the values (lower, upper, total) of the `scope.range` object, which can be used in the pagination
			 * template to display the current page range, e.g. "showing 21 - 40 of 144 results";
			 */
			function updateRangeValues() {
				var currentPage = paginationService.getCurrentPage(paginationId),
					itemsPerPage = paginationService.getItemsPerPage(paginationId),
					totalItems = paginationService.getCollectionLength(paginationId);

				scope.range.lower = (currentPage - 1) * itemsPerPage + 1;
				scope.range.upper = Math.min(currentPage * itemsPerPage, totalItems);
				scope.range.total = totalItems;
			}

			function isValidPageNumber(num) {
				return (numberRegex.test(num) && (0 < num && num <= scope.pagination.last));
			}
		}

		/**
		 * Generate an array of page numbers (or the '...' string) which is used in an ng-repeat to generate the
		 * links used in pagination
		 *
		 * @param currentPage
		 * @param rowsPerPage
		 * @param paginationRange
		 * @param collectionLength
		 * @returns {Array}
		 */
		function generatePagesArray(currentPage, collectionLength, rowsPerPage, paginationRange) {
			var pages = [];
			var totalPages = Math.ceil(collectionLength / rowsPerPage);
			var halfWay = Math.ceil(paginationRange / 2);
			var position;

			if (currentPage <= halfWay) {
				position = 'start';
			} else if (totalPages - halfWay < currentPage) {
				position = 'end';
			} else {
				position = 'middle';
			}

			var ellipsesNeeded = paginationRange < totalPages;
			var i = 1;
			while (i <= totalPages && i <= paginationRange) {
				var pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);

				var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
				var closingEllipsesNeeded = (i === paginationRange - 1 && (position === 'middle' || position === 'start'));
				if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
					pages.push('...');
				} else {
					pages.push(pageNumber);
				}
				i++;
			}
			return pages;
		}

		/**
		 * Given the position in the sequence of pagination links [i], figure out what page number corresponds to that position.
		 *
		 * @param i
		 * @param currentPage
		 * @param paginationRange
		 * @param totalPages
		 * @returns {*}
		 */
		function calculatePageNumber(i, currentPage, paginationRange, totalPages) {
			var halfWay = Math.ceil(paginationRange / 2);
			if (i === paginationRange) {
				return totalPages;
			} else if (i === 1) {
				return i;
			} else if (paginationRange < totalPages) {
				if (totalPages - halfWay < currentPage) {
					return totalPages - paginationRange + i;
				} else if (halfWay < currentPage) {
					return currentPage - halfWay + i;
				} else {
					return i;
				}
			} else {
				return i;
			}
		}
	}

	/**
	 * This filter slices the collection into pages based on the current page number and number of items per page.
	 * @param paginationService
	 * @returns {Function}
	 */
	function itemsPerPageFilter(paginationService) {

		return function (collection, itemsPerPage, paginationId) {
			if (typeof (paginationId) === 'undefined') {
				paginationId = DEFAULT_ID;
			}
			if (!paginationService.isRegistered(paginationId)) {
				throw 'pagination directive: the itemsPerPage id argument (id: ' + paginationId + ') does not match a registered pagination-id.';
			}
			var end;
			var start;
			if (collection instanceof Array) {
				itemsPerPage = parseInt(itemsPerPage) || 9999999999;
				if (paginationService.isAsyncMode(paginationId)) {
					start = 0;
				} else {
					start = (paginationService.getCurrentPage(paginationId) - 1) * itemsPerPage;
				}
				end = start + itemsPerPage;
				paginationService.setItemsPerPage(paginationId, itemsPerPage);

				return collection.slice(start, end);
			} else {
				return collection;
			}
		};
	}

	/**
	 * This service allows the various parts of the module to communicate and stay in sync.
	 */
	function paginationService() {

		var instances = {};
		var lastRegisteredInstance;

		this.registerInstance = function (instanceId) {
			if (typeof instances[instanceId] === 'undefined') {
				instances[instanceId] = {
					asyncMode: false
				};
				lastRegisteredInstance = instanceId;
			}
		};

		this.isRegistered = function (instanceId) {
			return (typeof instances[instanceId] !== 'undefined');
		};

		this.getLastInstanceId = function () {
			return lastRegisteredInstance;
		};

		this.setCurrentPageParser = function (instanceId, val, scope) {
			instances[instanceId].currentPageParser = val;
			instances[instanceId].context = scope;
		};
		this.setCurrentPage = function (instanceId, val) {
			instances[instanceId].currentPageParser.assign(instances[instanceId].context, val);
		};
		this.getCurrentPage = function (instanceId) {
			var parser = instances[instanceId].currentPageParser;
			return parser ? parser(instances[instanceId].context) : 1;
		};

		this.setItemsPerPage = function (instanceId, val) {
			instances[instanceId].itemsPerPage = val;
		};
		this.getItemsPerPage = function (instanceId) {
			return instances[instanceId].itemsPerPage;
		};

		this.setCollectionLength = function (instanceId, val) {
			instances[instanceId].collectionLength = val;
		};
		this.getCollectionLength = function (instanceId) {
			return instances[instanceId].collectionLength;
		};

		this.setAsyncModeTrue = function (instanceId) {
			instances[instanceId].asyncMode = true;
		};

		this.isAsyncMode = function (instanceId) {
			return instances[instanceId].asyncMode;
		};
	}
})();

window.ce = window.ce || {};
//Closure
(function(ce) {
	'use strict';

	ce.services = function(http, pathRelative, api){
		this.http = http;
		this.pathRelative = pathRelative || '';
		this.api = api;

		this.HTTP_OK = 200;
		this.NO_RESPONSE = 204;
		this.UNAUTHORIZED = 402;
		this.NOT_FOUND = 404;
		this.INTERNAL_ERROR = 500;
		this.VALIDATION_ERROR = 400;
	};

	ce.services.ConfigurationService = function(http, pathRelative) {
		ce.services.call(this,http,pathRelative);

		this.categories = null;
		this.services = null;

	};
	ce.services.ConfigurationService.prototype = new ce.services();

	ce.services.ConfigurationService.prototype.getCategories = function(sucessFnCallback, errorFnCallback){
		var scope = this;

		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/categories', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data;
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getCategoriesForClient = function(clientId, sucessFnCallback, errorFnCallback){
		var scope = this;
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/categoriesAvailable';

		this.http({method: 'GET', url: url , withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.assignCategoryToClient = function(clientId, categoryId, assigned, sucessFnCallback, errorFnCallback){
		var scope = this;
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/categories/'+categoryId+'/assign';

		this.http({method: 'PUT', url: url, withCredentials: true, cache:false, data: assigned})
		.then(function(response) {
			if (response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};



	ce.services.ConfigurationService.prototype.getMessages = function(lang, sucessFnCallback, errorFnCallback){
		var serviceUrl = this.pathRelative +'jaxrs/messages';
		if (lang){
			serviceUrl += '?lang='+lang.lang;
		}

		this.http({method: 'GET', url: serviceUrl , withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.ConfigurationService.prototype.getConfiguration = function(clientId, lang, sucessFnCallback, errorFnCallback){
		// Load the configuration
		// all=true -> load all sub-categories and services
		var serviceUrl = this.pathRelative +'jaxrs/v2/configuration?all=true';
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		// clientId=n -> load only categories associated with the given client
		if (clientId !== null) {
			serviceUrl += '&clientId=' + clientId;
		}

		serviceUrl += '&_=' + Math.random();

		var scope = this;
		this.http({method: 'GET', url: serviceUrl, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data.categories;
				scope.services = response.data.services;
				scope.mapCategories = {};

				//init numResults categories to 0
				angular.forEach(scope.categories, function(category, i){
					category.numResults = 0;
					scope.mapCategories[category.id] = category;
				});
				//init numResults services to 0
				angular.forEach(scope.services, function(service, i){
					service.numResults = 0;
				});

				sucessFnCallback(scope.categories, scope.mapCategories, scope.services, response.data.initLanguage, response.data.clients, response.data.pricesTypes, response.data.user);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.searchCategory = function(idCategory, service){
		var category;
		var elements = this.categories;
		if (service){
			elements = this.services;
		}
		angular.forEach(elements, function(cat, i){
			if (cat.id === idCategory){
				category = cat;
			}
		});
		return category;
	};
	ce.services.ConfigurationService.prototype.searchSubCategory = function(parentCategory, idSubCategory, service){
		var subCategory = null;
		if (parentCategory){
			angular.forEach(parentCategory.subCategories, function(cat, i){
				if (cat.id === idSubCategory){
					subCategory = cat;
				}
			});
		}
		return subCategory;
	};

	ce.services.ConfigurationService.prototype.getClientGroups = function(sucessFnCallback, errorFnCallback){
		var scope = this;
		
		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/clientGroups', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				scope.categories = response.data;
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getClientGroup = function(clientId, sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clientGroups/'+clientId, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.updateClientGroup = function(clientId, clientGroup, sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/clientGroups/'+clientId, withCredentials: true, cache:false, data: clientGroup})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getClientGroupByDomainName = function(domainName, sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clientGroups/domainname/'+domainName, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status === 204 || response.status === 304){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getDynamicType = function(dynamicTypeId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes/'+dynamicTypeId;
		
		this.http({method: 'GET', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.getDocumentToDynamicType = function(clientId, collectionName, documentId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+ clientId +'/'+ collectionName + '/' + documentId;
		
		this.http({method: 'GET', url: url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.addDocumentToDynamicType = function(collectionName, clientId, document, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/'+collectionName;
		
		this.http({method: 'POST', url:  url, withCredentials: true, cache:false, data: document})
		.then(function(response) {
			if (response.status === 200 || response.status ===  204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.deleteDocumentToDynamicType = function(collectionName, clientId, documentId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/clients/'+clientId+'/'+collectionName+'/'+documentId;
		
		this.http({method: 'DELETE', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 || response.status ===  204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.ConfigurationService.prototype.deleteDynamicType = function(dynamicTypeId, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes/'+dynamicTypeId;
		
		this.http({method: 'DELETE', url:  url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200 ||response.status === 204){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.getDynamicTypes = function(sucessFnCallback, errorFnCallback){		
		this.http({method: 'GET', url:  this.pathRelative +'jaxrs/v2/dynamicTypes', withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.getDynamicTypesForClient = function(clientId, sucessFnCallback, errorFnCallback){		
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes?clientId='+clientId;
		this.http({method: 'GET', url: url, withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.ConfigurationService.prototype.addDynamicType = function(dynamicType, sucessFnCallback, errorFnCallback){
		var url = this.pathRelative +'jaxrs/v2/dynamicTypes';
		
		this.http({method: 'POST', url: url, withCredentials: true, cache:false, data:dynamicType})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	//	End Closure
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.util = ce.util || {};
	ce.util.lang = ce.util.lang || {};
	ce.util.array = ce.util.array || {};

	ce.util.regexForDecimals = new RegExp('^(-)?[0-9]+([\,\.][0-9]+)?$');

	ce.util.getClientGroupDomainFromLacation = function(host, absUrl){
		var clientGroupDomain = null;

		//get the client by the domain name
		var firstPoint = host.indexOf('.');

		if (host.indexOf('www.') === -1){
			//no www
			clientGroupDomain = host.substring(0, firstPoint);
		}else{
			//www
			var secondPoint = host.indexOf('.', firstPoint+1);
			clientGroupDomain = host.substring(firstPoint+1, secondPoint);
		}

		if (!clientGroupDomain || !isNaN(parseInt(clientGroupDomain, 10)) || ['int', 'pre', 'cityexperience'].indexOf(clientGroupDomain) > -1){
			clientGroupDomain = 'arahal';
		}

		return clientGroupDomain;
	};


	ce.util.getClientDomainFromLacation = function(host, absUrl){
		var clientDomain = null;

		if(/\/#!\/\w+\//.test(absUrl)){

			var splitted = absUrl.split('/');
			var indexHashtag = splitted.indexOf('#!');
			clientDomain = splitted[indexHashtag + 1];

		}else{
			//get the client by the domain name
			var firstPoint = host.indexOf('.');

			if (host.indexOf('www.') === -1){
				//no www
				clientDomain = host.substring(0, firstPoint);
			}else{
				//www
				var secondPoint = host.indexOf('.', firstPoint+1);
				clientDomain = host.substring(firstPoint+1, secondPoint);
			}
		}

		if (!clientDomain || !isNaN(parseInt(clientDomain, 10)) || ['int', 'pre', 'cityexperience'].indexOf(clientDomain) > -1){
			clientDomain = 'arahal';
		}

		return clientDomain;
	};

	ce.util.noCache = function () {
		return '?_=' + Math.random();
	};

	ce.util.getNextSunday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date;
			break;
		case 1:
			d = date.add(6).day();
			break;
		case 2:
			d = date.add(5).day();
			break;
		case 3:
			d = date.add(4).day();
			break;
		case 4:
			d = date.add(3).day();
			break;
		case 5:
			d = date.add(2).day();
			break;
		case 6:
			d = date.add(1).day();
			break;
		default:
			break;
		}

		return d;
	};
	ce.util.getWeekFriday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date.add(-2).day();
			break;
		case 1:
			d = date.add(4).day();
			break;
		case 2:
			d = date.add(3).day();
			break;
		case 3:
			d = date.add(2).day();
			break;
		case 4:
			d = date.add(1).day();
			break;
		case 5:
			d = date;
			break;
		case 6:
			d = date.add(-11).day();
			break;
		default:
			break;
		}

		return d;
	};
	ce.util.getWeekMonday = function(dateInput){
		var date = new Date(dateInput.getTime());
		var currentDay = date.getDay();
		var d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);

		switch (currentDay) {
		case 0:
			d = date.add(-6).day();
			break;
		case 1:
			d = date;
			break;
		case 2:
			d = date.add(-1).day();
			break;
		case 3:
			d = date.add(-2).day();
			break;
		case 4:
			d = date.add(-3).day();
			break;
		case 5:
			d = date.add(-4).day();
			break;
		case 6:
			d = date.add(-5).day();
			break;
		default:
			break;
		}

		return d;
	};

	ce.util.isSet = function(obj){
		return (obj !== undefined && obj !== null && obj !== '');
	};

	ce.util.calculateScheduleResume = function(schedule, $scope){
		var resume = '';
		if (schedule !== undefined && schedule !== null){
			if (schedule.monday){
				resume += $scope.msg('common.monday')+ ', ';
			}
			if (schedule.tuesday){
				resume += $scope.msg('common.tuesday')+ ', ';
			}
			if (schedule.wednesday){
				resume += $scope.msg('common.wednesday')+ ', ';
			}
			if (schedule.thursday){
				resume += $scope.msg('common.thrusday')+ ', ';
			}
			if (schedule.friday){
				resume += $scope.msg('common.friday')+ ', ';
			}
			if (schedule.saturday){
				resume += $scope.msg('common.saturday')+ ', ';
			}
			if (schedule.sunday){
				resume += $scope.msg('common.sunday')+ ', ';
			}
			if (resume.charAt(resume.length-2) === ','){
				resume = resume.substr(0, resume.length-2);
			}
		}
		schedule.resume = resume;
	};

	/**
	 * Return a string with two digits from a int number
	 */
	ce.util.twoDigitsString = function(number){
		if (number === undefined || number === null){
			return '';
		} else if ((number+'').length === 2){
			return ''+number;
		} else {
			return '0'+number;
		}
	};



	//LANG FUNCTIONS
	ce.util.lang.getCurrentLocale = function(absUrl, languages){
		var currentLanguage = '';
		var langParamPosition = absUrl.indexOf('lang=');
		if (langParamPosition !== -1){
			var lang = absUrl.substring(langParamPosition +5 ,langParamPosition +7);
			currentLanguage = languages[lang];
		}else{
			//get the browser language
			var browserLang = window.navigator.userLanguage || window.navigator.language;
			currentLanguage = languages[browserLang];
		}

		return currentLanguage;
	};

	ce.util.lang.getArrayLanguages = function(){
		var languages = [];
		languages['es'] = { name: 'Español', icon: 'assets/img/flags/flag-spain.png' , lang: 'es'};
		languages['en'] = { name: 'English', icon: 'assets/img/flags/flag-uk.png', lang: 'en' };
		languages['ca'] = { name: 'Català', icon: 'assets/img/flags/flag-catalonia.png', lang: 'ca' };
		languages['it'] = { name: 'Italiano', icon: 'assets/img/flags/flag-italy.png', lang: 'it' };
		languages['pt'] = { name: 'Português', icon: 'assets/img/flags/flag-brazil.png', lang: 'pt' };
		languages['ru'] = { name: 'Руccкий', icon: 'assets/img/flags/flag-russia.png', lang: 'ru' };
		languages['fr'] = { name: 'Français', icon: 'assets/img/flags/flag-france.png', lang: 'fr' };
		languages['de'] = { name: 'Deutsch', icon: 'assets/img/flags/flag-germany.png', lang: 'de' };
		languages['zh'] = { name: '中文', icon: 'assets/img/flags/flag-china.png', lang: 'zh' };
//		{ name: 'हिनॿदी', icon: 'assets/img/flags/flag-india.png' }, // Hindi
//		{ name: '日本語', icon: 'assets/img/flags/flag-japan.png' }, // Nihongo
//		{ name: 'Nederlands', icon: 'assets/img/flags/flag-netherlands.png' }, // Dutch
//		{ name: 'العربية', icon: 'assets/img/flags/flag-saudiarabia.png' } // Arabic
//		];
		return languages;
	};

	ce.util.lang.getLocalesAvailablesInClient = function(client, languages){
		var langs = [];
		if (client !== null && client.languages !== null){
			angular.forEach(client.languages, function(language, i){
				if (languages[language] !== undefined){
					langs.push(languages[language]);
				}
			});
		}
		return langs;
	};

	//Utility function to remove an element from an array
	ce.util.array.remove = function(array, object) {
		var i = array.indexOf(object);
		if (i >= 0) {
			array.splice(i, 1);
		}
	};

	// XHR helper
	// All request must to have follow structure: ce.xhr.method(url, data, successCallback, errorCallback)
	ce.xhr = (function() {
		var apiVersion = 'jaxrs/v2/';

		return {
			get: function xhrGet() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'GET', url: url, cache: false, params: args.params }, args);
			},
			post: function xhrPost() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'POST', url: url, data: args.data, cache: false, params: args.params }, args);
			},
			put: function xhrPut() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'PUT', url: url, data: args.data, cache: false, params: args.params }, args);
			},
			delete: function xhrDelete() {
				var args = formatRequestArguments(arguments);
				var url = this.pathRelative + apiVersion + args.url;
				xhrRequest.call(this, { method: 'DELETE', url: url, cache: false, params: args.params }, args);
			}
		};

		///////

		function xhrRequest(config, args) {
			this
				.http(config)
				.then(function resolve(res) {
					if (typeof args.successCallback === 'object') {
						var response = args.successCallback.fullResponse ? res : res.data;
						return args.successCallback.callback(response);
					}
					args.successCallback(res.data);
				})
				.catch(function reject(res) { throwError(args.errorCallback, res.data, res.status); });
		}

		function formatRequestArguments(requestArguments) {
			var args = [].slice.call(requestArguments);
			if (args.length < 2) { return {}; }

			return {
				url: (args[0] || []).join('/'),
				data: args[3],
				params: args[4],
				successCallback: args[1],
				errorCallback: args[2]
			};
		}

		function throwError(errorCallback, res) {
			if (res.status === 401) { return; }
			if (angular.isFunction(errorCallback)) { return errorCallback(res.data, res.status); }
			throw new Error(res);
		}

	})();

//	End Closure
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.services.PortalServices = function(http, contentServices, pathRelative, api) {
		ce.services.call(this,http, pathRelative, api);
		this.contentServices = contentServices;
	};
	ce.services.PortalServices.prototype = new ce.services();

	ce.services.PortalServices.throwError = function(errorFnCallback, data, status) {
		if (angular.isFunction(errorFnCallback) && status !== 401) {
			errorFnCallback(data, status);
		} else if(status !== 401) {
			throw new Error({data: data, status: status});
		}
	};

	ce.services.PortalServices.prototype.loginNative = function(userData, sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative + 'jaxrs/v2/user/loginNative', withCredentials: true, cache:false, data: userData})
		.then(function(response) {
			if (response.status === 200){
				sucessFnCallback(response.data);
			}else{
				errorFnCallback();
			}
		}).catch(function(response) {
			if (angular.isFunction(errorFnCallback)) {
				errorFnCallback(response.data, response.status);
			} else if(response.status !== 401) {
				throw new Error(response.data);
			}
		});
	};



	/**
	*
	*/
	ce.services.PortalServices.prototype.login = function(sucessFnCallback, errorFnCallback, nologinCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/user?_=' + Math.random(), withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status === 200) {
				if (angular.isDefined(sucessFnCallback)){
					sucessFnCallback(response.data);
				}
			} else if (response.status === 204){
				//do nothing
				document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
				if (nologinCallback){
					nologinCallback(response.data);
				}
			} else {
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.loginPing = function(sucessFnCallback, errorFnCallback){
		this.http({method: 'GET', url: 'jaxrs/v2/user/ping?_=' + Math.random(), withCredentials: true, cache:false})
		.then(function(response) {
			if (response.status !== 204) {
				throw new Error(response.data);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.logout = function(sucessFnCallback, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/user/logout', withCredentials: true, cache:false})
		.then(function(response) {
			if (angular.isDefined(sucessFnCallback)){
				sucessFnCallback(response.data);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getClient = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+clientId, cache:false}).then(function(response) {
			if (response.status === 200){
				if (response.data.dynamicTypes && response.data.dynamicTypes.length > 0){
					for(var i = 0; i<response.data.dynamicTypes.length; i++){
						response.data.dynamicTypes[i].jsonSchema = JSON.parse(response.data.dynamicTypes[i].jsonSchema);
						response.data.dynamicTypes[i].formDefinition = JSON.parse(response.data.dynamicTypes[i].formDefinition);
					}
				}
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClients = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/', cache:false}).then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.getAllClients = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/all', cache:false}).then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.getClientByDomainName = function(clientDomain, lang, successFnCallBack, errorFnCallback){
		var serviceUrl = this.pathRelative +'jaxrs/v2/clients/domainname/' + clientDomain;
		if (lang){
			serviceUrl += '?lang='+lang.lang;
		}

		this.http({method: 'GET', url:  serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				//$log.error('Error getting client by domain name');
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getContentCreators = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/contentCreators', cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};





	/**
	*
	*/
	ce.services.PortalServices.prototype.searchMyRoutes = function(userId, clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: 'jaxrs/v2/users/' + userId + '/routes?clientId='+ clientId, withCredentials : true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.printElement = function(contentUrl, successFnCallBack, errorFnCallback){
		this.http({method : 'GET',url : contentUrl + '/pdf', withCredentials : true, cache:false})
		.then(function(response) {

		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.getRouteEntity = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.deleteRoute = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.removeRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	/**
	*
	*/
	ce.services.PortalServices.prototype.addRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.invalidateAll = function(successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/caches/invalidateAll', withCredentials : true, cache:false})
		.then(function(response) {
			successFnCallBack();
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.propagateImage = function(imageId, imageInfo, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: 'jaxrs/v3/images/'+ imageId + '/propagate', data: imageInfo, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClientContacts = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contacts', cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addClientContact = function(clientId, contact, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contacts', data: contact, withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.getClientContactsGroups = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contactGroups', withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addClientContactGroup = function(clientId, contactGroup, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/contactGroups', data: contactGroup, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.PortalServices.prototype.deleteContact = function(clientId, contactId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : this.pathRelative +'jaxrs/v2/clients/' + clientId + '/contacts/' + contactId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.deleteContactGroup = function(clientId, groupId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : this.pathRelative +'jaxrs/v2/clients/' + clientId + '/contactGroups/' + groupId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getClientUsers = function(clientId, successFnCallBack, errorFnCallback, params){
		params = params || {};
		params['_'] = Math.random();
		
		var paramsArray = [];
		for(var key in params){
			if(params.hasOwnProperty(key)) {
				paramsArray.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
			}
		}
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId + '/users?' + paramsArray.join('&'), cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateClientsForUser = function(userId, clients, successFnCallBack, errorFnCallback){
		var data = {
			attractions: null, 
			clients: clients,
			roles:null
		};

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeClients', 
			data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateAttractionsForUser = function(userId, attractions, successFnCallBack, errorFnCallback){
		var data = {
			attractions: attractions, 
			clients: null,
			roles:null
		};

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAttractions', 
			data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateUserAllowEmail = function(userId, allowEmail, successFnCallBack, errorFnCallback){

		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAllowEmail', 
			data: allowEmail, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.changeAttractions = function(userId, config, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+ userId + '/changeAttractions', 
			data: config, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addUser = function(clientId, userNew, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative +'jaxrs/v2/users/clients/'+ clientId, data: userNew, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
    ce.services.PortalServices.prototype.editUser = function(clientId, user, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/clients/'+ clientId, data: user, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.deleteUser = function(userId, successFnCallBack, errorFnCallback){
		this.http({method: 'DELETE', url: this.pathRelative +'jaxrs/v2/users/'+ userId, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.resetUserPassword = function(userId, clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+userId+'/clients/'+ clientId+'/resetPassword', cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.resetPassword = function(data, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/resetPassword', data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.changePassword = function(userId, data, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative +'jaxrs/v2/users/'+userId+'/changePassword', data: data, withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.getUser = function(userId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/users/'+ userId, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.PortalServices.prototype.getLoyaltyCard = function(userId, successFnCallBack, errorFnCallback) {
		ce.xhr.get.call(this, ['users', userId, 'loyaltyCard'], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.getPrivateUsers = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative +'jaxrs/v2/clients/'+ clientId+'/privateUsers', withCredentials: true, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.sendByEmail = function(type, clientId, elementId, mailInfo, successFnCallBack, errorFnCallback){
		var postUrl;
		if(type === 'ROUTE'){
			postUrl = 'jaxrs/v2/clients/'+ clientId +'/routes/'+ elementId +'/mail';
		}else{
			postUrl = 'jaxrs/v2/clients/'+ clientId +'/attractions/'+ elementId +'/mail';
		}

		this.http({method: 'POST', url: postUrl, data: mailInfo, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.sendNewsletter = function(clientId, mailInfo, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+ clientId +'/newsletter';

		//remove letters for contacts from elements
		var mailInfoCopied = angular.copy(mailInfo);
		angular.forEach(mailInfoCopied.selectedContacts,function(contact, i) {
			contact.id = parseInt(contact.id);
		});

		this.http({method: 'POST', url: postUrl, data: mailInfoCopied, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.saveReservation = function(reservation, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/reservations/';

		this.http({method: 'POST', url: postUrl, data: reservation, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getReservations = function(clientId, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/reservations/client/'+clientId;

		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateClient = function(clientId, clientCE, successFnCallBack, errorFnCallback){
		this.http({method: 'PUT', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId, data: clientCE, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.getImagesSliders = function(clientId, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+clientId+'/imageSlider';

		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateImagesSlider = function(clientId, imageSlider, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/clients/'+clientId+'/imageSlider';
		this.http({method: 'POST', url: this.pathRelative + postUrl, data: imageSlider, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.createPayment = function(payment, successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/payments';
		this.http({method: 'POST', url: this.pathRelative + postUrl, data: payment, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPayments = function(successFnCallBack, errorFnCallback){
		var postUrl = 'jaxrs/v2/payments';
		this.http({method: 'GET', url: this.pathRelative + postUrl, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.messagePush = function(clientGroupId, messagePush, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative + 'jaxrs/v2/clientGroups/'+ clientGroupId +'/pushMessages', data: messagePush, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addMarketingCampaign = function(clientId, marketingCampaign, successFnCallBack, errorFnCallback){
		this.http({method: 'POST', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns', data: marketingCampaign, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getMarketingCampaigns = function(clientId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns'})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getMarketingCampaign = function(clientId, mcId, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/clients/'+ clientId+'/marketingCampaigns/'+ mcId})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getCaches = function(successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/caches'})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getData = function(url, successFnCallBack, errorFnCallback){
		this.http({method: 'GET', url: url})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getContentWithWeight = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/'+ clientId +'/contentWithWeight';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getParkings = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/'+ clientId +'/parkings';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) { successFnCallBack(response.data); })
		.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.getAllLoyalCustomers = function(clientId, successFnCallBack, errorFnCallback, resume){
		var params = { '_': Math.random(), resume: !!resume };
		ce.xhr.get.call(this, ['clients', clientId, 'loyalCustomers'], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.getLoyalCustomers = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getLoyalCustomer = function(clientId, attractionId, customerId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addLoyaltyAmount = function(clientId, attractionId, customerId, data, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId + '/loyalty';

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.reedem = function(clientId, attractionId, customerId, amount, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'/redeem';

		var data = {
			amount: amount
		};

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.PortalServices.prototype.loyalty = function(clientId, attractionId, customerId, amount, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/' + customerId+'/loyalty';

		var data = {
			amount: amount
		};

		this.http({method: 'POST', url: url, data: data, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.loyaltyReminder = function(clientId, attractionId, customerIds, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/reminder';

		this.http({method: 'POST', url: url, data: customerIds, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.loyaltyResume = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/loyalCustomers/resume?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getLoyaltyAttactions = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/loyaltyAttractions?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.getEmployees = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + 
		'/attractions/' + attractionId + '/employee?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.addCustomer = function(clientId, attractionId, userNew, successFnCallBack, errorFnCallback){
		this.http({
            method: 'POST', 
            url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/customer', 
            data: userNew, 
            cache:false
        })
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

    ce.services.PortalServices.prototype.addEmployee = function(clientId, attractionId, userNew, successFnCallBack, errorFnCallback){
		this.http({
            method: 'POST', 
            url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/employees', 
            data: userNew, 
            cache:false
        })
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.deletePurchase = function(clientId, attractionId, customerId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'DELETE', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/loyalCustomers/'+customerId+'/purchases/'+purchaseId, 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPurchases = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getPurchasesMonthReport = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchasesMonthReport?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.purchase = function(clientId, attractionId, purchase, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/', 
			data: purchase,
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.userPurchase = function(clientId, attractionId, purchaseId, userId, redeem, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/user/'+userId + '?redeem=' + !!redeem, 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.checkPurchase = function(clientId, attractionId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'GET', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'?_=' + Math.random(), 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.confirmPurchase = function(clientId, attractionId, purchaseId, successFnCallBack, errorFnCallback){
		this.http({
			method: 'POST', 
			url: this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/confirm', 
			cache: false
		})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getGroups = function(clientId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups';

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.addGroup = function(clientId, group, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups';

		this.http({method: 'POST', url: url, data: group, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateGroup = function(clientId, group, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/groups/' + group.id;

		this.http({method: 'PUT', url: url, data: group, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.updateConfiguration = function(clientId, attractionId, configuration, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/loyaltyConfiguration';

		this.http({method: 'PUT', url: url, data: configuration, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.getConfiguration = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		var url = this.pathRelative + 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/loyaltyConfiguration?_=' + Math.random();

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.PortalServices.prototype.assignTicket = function(clientId, attractionId, purchaseId, ticket, successFnCallBack, errorFnCallback){

		var url = this.pathRelative+'jaxrs/v2/clients/'+clientId+'/attractions/'+attractionId+'/purchases/'+purchaseId+'/ticket';

		this.http({method: 'PUT', url: url, data: ticket, cache: false})
			.then(function(response) { successFnCallBack(response.data); })
			.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.decodeQrToken = function(token, successFnCallBack, errorFnCallback){

		var url = this.pathRelative + 'jaxrs/v2/tokens/' + token;

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) { successFnCallBack(response.data); })
		.catch(function(response) { ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status); });
	};

	ce.services.PortalServices.prototype.associateCard = function(userId, token, forceReassign, successFnCallBack, errorFnCallback){
		var params = { 'userId': userId, 'forceReassign': !!forceReassign };
		ce.xhr.put.call(this, ['tokens', token], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.unifyCustomers = function(idUserToMerge, idUserMerged, params, successFnCallBack, errorFnCallback){
		ce.xhr.put.call(this, ['users', idUserToMerge, 'merge', idUserMerged], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.getGiftVouchers = function(clientId, attractionId, successFnCallBack, errorFnCallback) {
		var params = { attractionId: attractionId, '_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'giftVouchers'], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.addGiftVoucher = function(clientId, data, successFnCallBack, errorFnCallback) {
		ce.xhr.post.call(this, ['clients', clientId, 'giftVouchers'], successFnCallBack, errorFnCallback, data);
	};

	ce.services.PortalServices.prototype.getGiftVoucherByCode = function(clientId, code, successFnCallBack, errorFnCallback) {
		var params = {'_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'giftVouchers', code], successFnCallBack, errorFnCallback, null, params);
	};

	ce.services.PortalServices.prototype.activateGiftVoucher = function(clientId, code, successFnCallBack, errorFnCallback) {
		ce.xhr.put.call(this, ['clients', clientId, 'giftVouchers', code, 'activate'], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.deactivateGiftVoucher = function(clientId, code, successFnCallBack, errorFnCallback) {
		ce.xhr.delete.call(this, ['clients', clientId, 'giftVouchers', code], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.assignGiftVoucher = function(clientId, code, userId, successFnCallBack, errorFnCallback) {
		ce.xhr.put.call(this, ['clients', clientId, 'giftVouchers', code, 'user', userId], successFnCallBack, errorFnCallback);
	};

	ce.services.PortalServices.prototype.simulateLoyalty = function(clientId, attractionId, userId, data, successFnCallBack, errorFnCallback) {
		ce.xhr.post.call(this, ['clients', clientId, 'attractions', attractionId, 'loyalCustomers', userId, 'loyalty', 'simulate'],
			successFnCallBack, errorFnCallback, data);
	};

	ce.services.PortalServices.prototype.getClientStatistics = function(clientId, successFnCallBack, errorFnCallback) {
		var params = {'_': Math.random() };
		ce.xhr.get.call(this, ['clients', clientId, 'loyaltyStatistics'], successFnCallBack, errorFnCallback, null, params);
	};

	//	End Closure
})(window.ce);

(function(ce) {
	'use strict';

	ce.services.ContentServices = function(http, configurationServices,pathRelative, config) {
		ce.services.call(this,http, pathRelative);
		this.configurationServices = configurationServices;
		this.config = config;
	};
	ce.services.ContentServices.prototype = new ce.services();
	/**
	*
	*/
	ce.services.ContentServices.prototype.getContent = function(CEConfiguration, lang, session, counter, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;
		var privateCreator = session.privateCreator;
		var publicCreator = session.publicCreator;

		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/content';
		if (privateCreator === true){
			url += '?privateCreator=true';
		}else{
			url += '?privateCreator=false';
		}

		if (publicCreator || privateCreator){
			url += '&time='+new Date().getTime();
		}
		if (lang){
			url += '&lang='+lang.lang;
		}

		this.http({method: 'GET', url: this.pathRelative + url, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, privateCreator, publicCreator, counter, categories, CEConfiguration, session);

				var count = 0;
				CEConfiguration.mapCategories1 = {};
				CEConfiguration.mapCategories2 = {};


				var totalCategoriesNotEmpty = 0;
				for (var prop in categories) {
					if (categories[prop].numResults > 0){
						totalCategoriesNotEmpty++;
					}
				}
				var middle = Math.ceil(totalCategoriesNotEmpty / 2);

				for (var property in categories) {
					if (categories[property].numResults > 0){
						count++;
						if (totalCategoriesNotEmpty < 13 || (totalCategoriesNotEmpty >= 13 && count <= middle)){
							CEConfiguration.mapCategories1[property] = categories[property];
						}else{
							CEConfiguration.mapCategories2[property] = categories[property];
						}
					}
				}
				CEConfiguration.categoriesUsed = totalCategoriesNotEmpty;

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.ContentServices.prototype.getPrivateContent = function(CEConfiguration, lang, session, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;

		var serviceUrl =  'jaxrs/v2/clients/' + clientId + '/content/pending?date='+new Date().getTime();
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		var scope = this;
		this.http({method: 'GET', url:serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, false, true, null, null, CEConfiguration, session);
				angular.forEach(categories, function(category, i){
					category.numResults = 0;
					angular.forEach(resultSearchElements, function(contentCE, j){
						if (contentCE.idCategory === category.id){
							category.numResults = category.numResults +1;
						}
					});
				});

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};


	ce.services.ContentServices.prototype.getArchivedContent = function(CEConfiguration, lang, session, successFnCallBack, errorFnCallback){
		var clientId = CEConfiguration.selectedClient.id;
		var categories = CEConfiguration.mapCategories;

		var serviceUrl =  'jaxrs/v2/clients/' + clientId + '/content/archived?date='+new Date().getTime();
		if (lang){
			serviceUrl += '&lang='+lang.lang;
		}

		var scope = this;
		this.http({method: 'GET', url:serviceUrl, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var resultSearchElements = scope.fillIdsbyElements(response.data, false, true, null, null, CEConfiguration, session);
				angular.forEach(categories, function(category, i){
					category.numResults = 0;
					angular.forEach(resultSearchElements, function(contentCE, j){
						if (contentCE.idCategory === category.id){
							category.numResults = category.numResults +1;
						}
					});
				});

				successFnCallBack(resultSearchElements);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	/**
	*
	*/
	ce.services.ContentServices.prototype.getInfoSharedElement = function(userId, routeId, attractionId, successFnCallBack, errorFnCallback){
		var scope = this;
		var httpUrl = 'jaxrs/v2/shared';
		if (routeId) {
			httpUrl += '?routeId=' + routeId;
		} else if (attractionId){
			httpUrl += '?attractionId=' + attractionId;
		} else {
			return null;
		}
		if (userId) {
			httpUrl += '&userId=' + userId;
		}

		this.http({method: 'GET', url: httpUrl , cache:false})
		.then(function(response) {
			scope.setMarkers(response.data.sharedElement);
			if (response.data.sharedElement.type === 'ROUTE' && response.data.sharedElement.route.attractions && response.data.sharedElement.route.attractions.length > 0){
				angular.forEach(response.data.sharedElement.route.attractions, function(attraction, i){
					scope.setMarkers(attraction);
				});
			}
			successFnCallBack(response.data);
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	* Routes have an array with attractions id, replace then with an attractions array
	* Attractions have an array with activities id, replace them with activities array
	* Activities hava an parent attraction id, replace it for attraction entity
	*/
	ce.services.ContentServices.prototype.fillIdsbyElements = function(resultSearchElements, privateCreator, publicCreator, counter, categories, CEConfiguration, session){
		var today = Date.today();
		var tomorrow = Date.today().add(1).day();
		var afterTomorrow = Date.today().add(1).day();
		var sevenDays = Date.today().add(7).day();
		var oneMonth = Date.today().add({months: 1}).day();
		var friday = ce.util.getWeekFriday(today);
		var sunday = ce.util.getNextSunday(today);
		sunday.setHours(23);
		var monday = ce.util.getWeekMonday(today);
		var y = today.getFullYear(), m = today.getMonth();
		var firstDay = new Date(y, m, 1);
		var lastDay = new Date(y, m + 1, 0);

		//reset the categories value
		angular.forEach(categories, function(category){
			category.numResults = 0;
		});

		//iterate over result search elements
		var scope = this;
		angular.forEach(resultSearchElements, function(element) {
			//first, set the icon active, inactive and category icon
			scope.setMarkers(element);

			if(categories && element.idCategory !== undefined && element.status !== 5){
				if (categories[element.idCategory]){
					categories[element.idCategory].numResults = categories[element.idCategory].numResults + 1;
				}

			}
			if(categories && element.idCategory2 !== undefined && element.status !== 5 && element.idCategory !== element.idCategory2){
				if (categories[element.idCategory2]){
					categories[element.idCategory2].numResults = categories[element.idCategory2].numResults + 1;
				}
			}

			if (publicCreator){
				if (element.status === 3){
					element.statusIcon = 'watch';
				}else if (element.status === 5){
					element.statusIcon = 'tool';
					counter.building++;
				}else if (element.status === 6){
					element.statusIcon = 'folder';
				}
			}else if (privateCreator){
				if (element.status === 1){
					element.statusIcon = 'check';
				}else if (element.status === 3){
					element.statusIcon = 'watch';
				}else if (element.status === 5){
					element.statusIcon = 'tool';
					counter.building++;
				}else if (element.status === 6){
					element.statusIcon = 'folder';
				}
			}

			if(element.type === 'ATTRACTION'){
				//for attractions, replace activities ids for activity entity
				element.attraction.activities = scope.completeActivitiesElements(element.attraction.activitiesUrls, resultSearchElements);
				if (counter !== null && element.status === 1 && !element.attraction.comercio){
					counter.attractions++;
				}
				if (counter !== null && element.status === 1 && element.attraction.comercio){
					counter.shops++;
				}
				element.wrapperPriceRange = element.attraction.priceRange;
			}
		//});

		//angular.forEach(resultSearchElements, function(element) {
			if(element.type === 'ACTIVITY'){
				//for activities, replace parentAttractionId for Attraction entity
				var attractionParent = scope.completeAttractionElement(element.activity.attractionParentUrl, resultSearchElements, false);
				if (attractionParent){
					element.activity.attractionParent = attractionParent;
				}
				if (counter !== null && element.status === 1){
					counter.activities = counter.activities + 1;
				}

				var start = new Date(element.activity.startTime);
				start.setHours(0);
				var end = new Date(element.activity.endTime);
				end.setHours(0);

				element.today = today.between(start, end);
				element.tomorrow = tomorrow.between(start, end);
				element.thisWeekend = (start < sunday && end > friday);
				element.thisWeek = (start < sevenDays && end > today);
				element.thisMonth = (start < oneMonth && end > today);

				if (element.today){
					element.today = false;
					var i = 0;
					var dayToday = today.getDay();
					while(i < element.activity.schedules.length && !element.today){
						var schedule = element.activity.schedules[i];
						element.today = (dayToday === 0 && schedule.sunday || dayToday === 1 && schedule.monday || dayToday === 2 && schedule.tuesday ||
							dayToday === 3 && schedule.wednesday || dayToday === 4 && schedule.thursday || dayToday === 5 && schedule.friday || dayToday === 6 && schedule.saturday);
						i++;
					}
				}
				if (element.tomorrow){
					element.tomorrow = false;
					var j = 0;
					var dayTomorrow = tomorrow.getDay();
					while(j < element.activity.schedules.length && !element.tomorrow){
						var schedule1 = element.activity.schedules[j];
						element.tomorrow = (dayTomorrow === 0 && schedule1.sunday || dayTomorrow === 1 && schedule1.monday || dayTomorrow === 2 && schedule1.tuesday ||
							dayTomorrow === 3 && schedule1.wednesday || dayTomorrow === 4 && schedule1.thursday || dayTomorrow === 5 && schedule1.friday || dayTomorrow === 6 && schedule1.saturday);
						j++;
					}
				}
				if (element.thisWeekend){
					element.thisWeekend = false;
					var k = 0;
					var day = today.getDay();
					while(k < element.activity.schedules.length && !element.thisWeekend){
						var schedule2 = element.activity.schedules[k];
						element.thisWeekend = (schedule2.sunday || schedule2.saturday);
						k++;
					}
				}


				if (counter !== null && element.status === 1){
					if (element.today){
						counter.today++;
					}
					if (element.tomorrow){
						counter.tomorrow++;
					}
					if (element.thisWeekend){
						counter.thisWeekend++;
					}
					if (element.thisWeek){
						counter.thisWeek++;
					}
					if (element.thisMonth){
						counter.thisMonth++;
					}
				}

				element.wrapperPriceRange = element.activity.priceRange;
			}
		//});

		//angular.forEach(resultSearchElements, function(element) {
			if (element.type === 'ROUTE'){
				//for routes, replace attractions url for attraction entity
				element.route.attractions = scope.completeAttractionElements(element.route.attractionsUrls, resultSearchElements);

				if (counter !== null){
					counter.routes++;
				}
			}else if(element.type === 'OFFER' && element.offer.parentUrl !== null && element.offer.parentUrl !== undefined){
				if (element.offer.parentType === 'ATTRACTION'){
					var attractionId = element.offer.parentUrl.substring(element.offer.parentUrl.lastIndexOf('/')+1);
					element.offer.parentId = attractionId;
					element.offer.parent = scope.completeAttractionElement(element.offer.parentUrl, resultSearchElements, false);
					
					if (element.offer.parent){
						if (element.offer.parent.activity){
							//load the attraction parent for the activity
							var attractionParentForOffer = scope.completeAttractionElement(element.offer.parent.activity.attractionParentUrl, resultSearchElements, false);
							if (attractionParentForOffer){
								element.offer.parent.activity.attractionParent = attractionParentForOffer;
							}
						}
					}else{
						console.log('Cannot find parent for offer '+element.id + ' for parent ' + element.offer.parentUrl);
					}
					
				}/*else if (element.parentType === 'ROUTE'){
					//:TODO
				}*/
				if (counter !== null && element.status !== 5){
					counter.offers++;
				}
			}else if (element.type === 'DYNAMIC'){
				if (counter !== null){
					counter.dynamic++;
				}
			}

			element.canModify = ce.security.userCanModify(session.globalCreator, session.publicCreator, session.privateCreator, session.userLoged, element);
		});

		return resultSearchElements;
	};
	/**
	* Define default images
	*/
	ce.services.ContentServices.prototype.setImagesUrls = function(image, clientId){
		if (!ce.util.isSet(image)){
			image = {
				urls: {
					m_web : 'assets/img/defaultImages/neutral-image.jpg',
					l_web: 'assets/img/defaultImages/neutral-image.jpg'
				}
			};
		}else if (!ce.util.isSet(image.urls)){
			image.urls = {
				m_web : 'assets/img/defaultImages/neutral-image.jpg',
				l_web: 'assets/img/defaultImages/neutral-image.jpg'
			};
		}else{
			if (!ce.util.isSet(image.urls.m_web)){
				if (ce.util.isSet(image.urls.thumb2)){
					image.urls.m_web = image.urls.thumb2;
				}else{
					image.urls.m_web = 'assets/img/defaultImages/neutral-image.jpg';
				}
			}
			if (!ce.util.isSet(image.urls.l_web)){
				if (ce.util.isSet(image.urls.thumb1)){
					image.urls.l_web = image.urls.thumb1;
				}else{
					image.urls.l_web = 'assets/img/defaultImages/neutral-image.jpg';
				}
			}
		}
	};
	ce.services.ContentServices.prototype.setMarkers = function(contentCE){
		//define default images
		ce.services.ContentServices.prototype.setImagesUrls(contentCE.mainImage, contentCE.idClient);
		if (contentCE.images && contentCE.images.length > 0){
			angular.forEach(contentCE.images, function(img, i) {
				ce.services.ContentServices.prototype.setImagesUrls(img, contentCE.idClient);
			});
		}

		if (contentCE.type === 'ROUTE'){
			//select the route pin
			contentCE.iconActive = this.config.staticPath+'img/pins/pin-m-active-route.png';
			contentCE.iconInactive = this.config.staticPath+'img/pins/pin-m-inactive-route.png';
			contentCE.categoryIcon = 'assets/img/icon-route-green.png';
			contentCE.categoryName = '';
		} else if (contentCE.type === 'OFFER'){
			contentCE.iconActive = this.config.staticPath+'img/pins/pin-m-active-deal1.png';
			contentCE.iconInactive = this.config.staticPath+'img/pins/pin-m-inactive-deal1.png';
		}else{
			var icons = null;
			var category = this.configurationServices.searchCategory(contentCE.idCategory, contentCE.type==='SERVICE');
			if (category){
				if (contentCE.idSubCategory !== null && contentCE.idSubCategory !== ''){
					var subCategory = this.configurationServices.searchSubCategory(category, contentCE.idSubCategory, contentCE.type==='SERVICE');
					if (subCategory){
						icons = subCategory.icons;
					}else{
						//cannot found subcategory into category
						icons = category.icons;
					}
				}else{
					icons = category.icons;
				}
			}
			if (icons !== null){
				contentCE.iconActive = icons.pin_m_active;
				contentCE.iconInactive = icons.pin_m_inactive;
				contentCE.categoryIcon = icons.icon_m;
				contentCE.categoryName = category.name;
			}else{
				contentCE.iconActive = this.config.staticPath+'img/pins/default-icon.jpg';
				contentCE.iconInactive = this.config.staticPath+'img/pins/default-icon.jpg';
				contentCE.categoryIcon = null;
				contentCE.categoryName = '';
			}
		}
	};
	ce.services.ContentServices.prototype.completeOfferElements = function(offersUrls, resultSearchElements){
		var scope = this;
		if (offersUrls && offersUrls.length > 0){
			var offers = [];
			angular.forEach(offersUrls, function(offerUrl, i) {
				var offerId = offerUrl.substring(offerUrl.lastIndexOf('/')+1);
				//find attraction in resultSearchElements
				var offer = scope.findElementInResultSearch('OFFER', offerId, resultSearchElements);
				if (offer){
					offers.push(angular.copy(offer));
				}
			});
			//replace array of ids by attractions array
			return offers;
		}else{
			return [];
		}
	};
	ce.services.ContentServices.prototype.completeAttractionElements = function(attractionUrls, resultSearchElements){
		var scope = this;
		if (attractionUrls && attractionUrls.length > 0){
			var attractions = [];
			angular.forEach(attractionUrls, function(attractionUrl, i) {
				var attraction = scope.completeAttractionElement(attractionUrl, resultSearchElements, true);
				if (attraction){
					if (attraction.type === 'ACTIVITY'){
						var attractionParent = scope.completeAttractionElement(attraction.activity.attractionParentUrl, resultSearchElements, false);
						if (attractionParent){
							attraction.activity.attractionParent = attractionParent;
						}
					}
					scope.setMarkers(attraction);
					attractions.push(attraction);
				}
			});
			//replace array of ids by attractions array
			return attractions;
		}else{
			return [];
		}
	};
	/**
	* Replace activities urls for entities
	*/
	ce.services.ContentServices.prototype.completeActivitiesElements = function(activitiesUrls, resultSearchElements){
		var scope = this;
		if (activitiesUrls && activitiesUrls.length > 0){
			var activities = [];
			angular.forEach(activitiesUrls, function(activityUrl, i) {
				var activityId =  activityUrl.substring(activityUrl.lastIndexOf('/')+1);
				var activity = scope.findElementInResultSearch('ACTIVITY', activityId, resultSearchElements);
				if (activity){
					scope.setMarkers(activity);
					activities.push(angular.copy(activity));
				}
			});
			return activities;
		}else{
			return [];
		}
	};
	ce.services.ContentServices.prototype.completeAttractionElement = function(attractionUrl, resultSearchElements, loadDependencies){
		var scope = this;
		var attractionId = attractionUrl.substring(attractionUrl.lastIndexOf('/')+1);
		//find attraction in resultSearchElements
		var attraction = scope.findElementInResultSearch('ATTRACTION', attractionId, resultSearchElements);
		if (attraction){
			if (loadDependencies){
				attraction.attraction.activities = scope.completeActivitiesElements(attraction.attraction.activitiesUrls, resultSearchElements);
			}
			attraction = attraction;
		}else{
			var activity = scope.findElementInResultSearch('ACTIVITY', attractionId, resultSearchElements);
			if (activity){
				attraction = activity;
			}
		}
		return angular.copy(attraction);
	};
	/**
	* Search an attraction entity in result search elements by its id
	*/
	ce.services.ContentServices.prototype.findElementInResultSearch = function(type, id, resultSearchElements){
		var i = 0;
		var element = null;
		while (i < resultSearchElements.length && element === null) {
			if (resultSearchElements[i].type === type && resultSearchElements[i].id === parseInt(id)){
				element = resultSearchElements[i];
			}
			i = i+1;
		}
		return element;
	};
})(window.ce);

(function(ce){
	'use strict';

	ce.services.MapService = function() {
		var markers = [];
		var overlays = [];
		var routesPolylines = [];

		this.map = null;
		this.mapOptions = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControlOptions: { style: google.maps.ZoomControlStyle.SMALL },
			center: new google.maps.LatLng(37.2621077, -5.5455903),
			zoom: 15,
			styles: [
				{
					featureType: 'poi.business',
					stylers: [{ visibility: 'off' }]
				}
			]
		};

		this.createMarker = function(contentCE, onClickMarker, onDblclickMarker, onMouseOverMarker){
			if (markers.indexOf(contentCE) > -1){
				console.log('elemento ya añadido');
				return;
			}

			markers.push(contentCE);

			var latitude, longitude;

			if (contentCE.type === 'ROUTE' && contentCE.route){

				latitude = contentCE.route.latitude;
				longitude = contentCE.route.longitude;

			}else if (contentCE.type === 'ACTIVITY' && contentCE.activity &&
					contentCE.activity.attractionParent && contentCE.activity.attractionParent.attraction){

				latitude = contentCE.activity.attractionParent.attraction.latitude;
				longitude = parseFloat(contentCE.activity.attractionParent.attraction.longitude) + 0.00006;

			}else if (contentCE.type === 'OFFER' && contentCE.offer && contentCE.offer.parent){

				if(contentCE.offer.parent.attraction){
					latitude = contentCE.offer.parent.attraction.latitude;
					longitude = contentCE.offer.parent.attraction.longitude - 0.00006;

				}else if(contentCE.offer.parent.activity && contentCE.offer.parent.activity.attractionParent){
					latitude = contentCE.offer.parent.activity.attractionParent.attraction.latitude;
					longitude = contentCE.offer.parent.activity.attractionParent.attraction.longitude - 0.00006;

				}else{
					console.log('error en parent type for offer', contentCE.id, '- ', contentCE.offer.parentUrl);
				}
			}else if (contentCE.attraction){

				latitude = contentCE.attraction.latitude;
				longitude = contentCE.attraction.longitude;

			}else{
				console.log('No se ha encontrado coordenadas para el contenido', contentCE.id);
			}

			if(!latitude || !longitude) { return; }

			latitude = (''+latitude).split(',').join('');
			longitude = (''+longitude).split(',').join('');

			var marker = new google.maps.Marker({
				contentCE: contentCE,
				iconActive: contentCE.iconActive,
				iconInactive: contentCE.iconInactive,
				icon: contentCE.iconInactive,
				map: this.map,
				position: new google.maps.LatLng(latitude, longitude),
				title: contentCE.name
			});

			marker.setZIndex(contentCE.type === 'ROUTE' ? 1.5 : 0);
			overlays.push(marker);

			google.maps.event.addListener(marker, 'mouseover', onMouseOver);
			google.maps.event.addListener(marker, 'mouseout', onMouseOut);
			google.maps.event.addListener(marker, 'click', onClick);
			google.maps.event.addListener(marker, 'dblclick', onDblclick);

			var self = this;

			function onMouseOver() {
				onMouseOverMarker(marker.contentCE, true);
				marker.setIcon(marker.iconActive);

				//if it's a route, active its elements marker
				if (marker.contentCE.type === 'ROUTE' && 
					marker.contentCE.route.attractions &&
					marker.contentCE.route.attractions.length){

					marker.contentCE.route.attractions.forEach(function(attraction){
						self.setMarkerStatus(attraction, true);
					});
					//change polyline opacity
					var polyline = getPolyline(marker.contentCE);
					if (polyline){
						polyline.setOptions({strokeOpacity:1.0});
					}
				}
			}

			function onMouseOut() {
				onMouseOverMarker(marker.contentCE, false);
				marker.setIcon(marker.iconInactive);

				//if it's a route, desactive its elements marker
				if (marker.contentCE.type === 'ROUTE' && 
					marker.contentCE.route.attractions &&
					marker.contentCE.route.attractions.length){

					marker.contentCE.route.attractions.forEach(function(attraction){
						self.setMarkerStatus(attraction, false);
					});
					//change polyline opacity
					var polyline = getPolyline(marker.contentCE);
					if (polyline){
						polyline.setOptions({strokeOpacity:0.1});
					}
				}
			}

			function onClick() {
				onClickMarker(marker.contentCE);
			}

			function onDblclick(){
				onDblclickMarker(marker.contentCE);
			}
		};

		this.createRoute = function(contentCE, onClickPolyline, onDblclickPolyline, onMouseOverPolyline){
			var polyline;
			if (contentCE && contentCE.route && contentCE.route.overviewPolyline){
				//create polyline
				var decodedSets = google.maps.geometry.encoding.decodePath(contentCE.route.overviewPolyline);
				var iconsetngs = { path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW };
				polyline = new google.maps.Polyline({
					path:decodedSets, 
					strokeColor: '#FF0000', 
					strokeOpacity: 0.1, 
					strokeWeight: 2, 
					contentCE: contentCE,
					icons: [{icon: iconsetngs, repeat:'25%', offset: '100%'}]
				});

				polyline.setOptions({ strokeOpacity: 0.1 });
				routesPolylines.push(polyline);

				google.maps.event.addListener(polyline, 'mouseover', onMouseOver);
				google.maps.event.addListener(polyline, 'mouseout', onMouseOut);
				google.maps.event.addListener(polyline, 'click', onClick);
				google.maps.event.addListener(polyline, 'dblclick', onDblclick);
			}

			function onMouseOver(){
				onMouseOverPolyline(polyline.contentCE, true);
				polyline.setOptions({ strokeOpacity: 1.0 });
			}

			function onMouseOut(){
				onMouseOverPolyline(polyline.contentCE, false);
				polyline.setOptions({ strokeOpacity: 0.1 });
			}

			function onClick(){
				onClickPolyline(polyline.contentCE);
			}

			function onDblclick(){
				onDblclickPolyline(polyline.contentCE);
			}
		};

		this.clearMap = function(){
			overlays.forEach(function(marker){
				marker.setMap(null);
			});
			overlays.splice(0);
			markers.splice(0);

			routesPolylines.forEach(function(polyline){
				polyline.setMap(null);
			});
			routesPolylines.splice(0);
		};

		this.resizeMap = function(){
			google.maps.event.trigger(this.map, 'resize');
		};

		this.initMap = function(latitude, longitude, zoom, maxZoom){
			if (!this.map) { return; }

			var self = this;

			routesPolylines.forEach(function(polyline){
				polyline.setMap(self.map);
			});

			if(overlays.length){
				var bounds = new google.maps.LatLngBounds();
				overlays.forEach(function(marker){
					bounds.extend(marker.position);
				});
				this.map.fitBounds(bounds);

				google.maps.event.addListenerOnce(this.map, 'idle', function() {
					if (self.map.getZoom() > maxZoom){
						self.map.setZoom(maxZoom);
					}
				});
			}else if(latitude && longitude){
				this.map.setCenter(new google.maps.LatLng(latitude, longitude));
				this.map.setZoom(zoom);
			}
		};

		this.setMarkerStatus = function (contentCE, active){
			var marker = getMaker(contentCE);
			//change the marker status
			if (marker){
				marker.setIcon(active ? marker.iconActive : marker.iconInactive);
				marker.setZIndex(contentCE.type === 'ROUTE' ? 1.5 : (active ? 1 : 0));
			}
			//change the opacity of the polyline
			if (contentCE.type === 'ROUTE'){
				var polyline = getPolyline(contentCE);
				if (polyline){
					polyline.setOptions({strokeOpacity: active ? 1.0 : 0.1});
				}
				//change status its attractions
				if(contentCE.route.attractions && contentCE.route.attractions.length){
					contentCE.route.attractions.filter(function(attraction){
						var markerAttraction = getMaker(attraction);
						if (markerAttraction){
							markerAttraction.setIcon(active ? markerAttraction.iconActive : markerAttraction.iconInactive);
							markerAttraction.setZIndex(active ? 1 : 0);
						}
					});
				}
			}
		};

		function getMaker(contentCE){
			var marker = overlays.filter(function(marker){
				return marker.contentCE.id === contentCE.id && marker.contentCE.type === contentCE.type;
			});

			if(marker.length){ return marker[0]; }
		}

		function getPolyline(contentCE){
			var polyline = routesPolylines.filter(function(polyline){
				return polyline.contentCE.id === contentCE.id && polyline.contentCE.type === contentCE.type;
			});

			if(polyline.length){ return polyline[0]; }
		}
	};
})(window.ce);
//Closure
(function(ce) {
	'use strict';

	ce.services.AttractionServices = function(http, pathRelative) {
		ce.services.call(this,http,pathRelative);
	};
	ce.services.AttractionServices.prototype = new ce.services();

	/**
	*
	*/
	ce.services.AttractionServices.prototype.deleteAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			if (response.status === 204) {
				successFnCallBack();
			} else {
				if (angular.isDefined(errorFnCallback)){
					errorFnCallback(response.data);
				}else{
					throw new Error(response.data);
				}
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.AttractionServices.prototype.addAttraction = function(clientId, json, translate, publish, successFnCallBack, errorFnCallback){
		//search user routes
		this.http({method: 'POST', url: 'jaxrs/clients/'+ clientId +'/attractions?translate='+translate+'&publish='+publish, data:json, cache:false})
		.then(function(response) {
			//check if the status code responde is correct
			if (response.status === 200 && response.data.statusRequest === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.addAttractionToRoute = function(route, attractionId, successFnCallBack, errorFnCallback){
		var requestUrl = 'jaxrs/v2/user/routes/' + route.id + '/attractions/' + attractionId;
		this.http({method : 'PUT', url : requestUrl, data : route, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.deleteAttractionToRoute = function(routeId, attractionId, successFnCallBack, errorFnCallback){
		var requestUrl = 'jaxrs/v2/user/routes/' + routeId + '/attractions/' + attractionId;
		this.http({method : 'DELETE', url : requestUrl, cache:false})
		.then(function(response) {
			// check if the status code responde is
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.AttractionServices.prototype.getAttractionCE = function(attractionId, clientId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : this.pathRelative + 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	ce.services.AttractionServices.prototype.getAttractionEntity = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/'+ clientId + '/attractions/' + attractionId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.acceptAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/accept', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.acceptDynamic = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/accept', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.rejectAttraction = function(clientId, attractionId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId+'/reject', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.transferAttraction = function(clientId, attractionId, userId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId + '/transfer/' + userId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.getOfferEntity = function(offerId, clientId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/offers/'+offerId;

		this.http({method: 'GET', url: url, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.deleteOffer = function(offerId, attractionId, clientId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attractions/'+attractionId+'/offers/'+offerId;

		this.http({method: 'DELETE', url: url, cache:false})
		.then(function(response) {
			if (response.status === 204){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.addOffer = function(attractionId, clientId, offer, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attraction/'+attractionId+'/offers/';

		this.http({method: 'POST', url: url, data: offer, cache:false})
		.then(function(response) {
			if (response.status === 200){
				successFnCallBack(response.data);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveDynamic = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/archive', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveContent = function(clientId, contentType, contentId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/' + contentType + '/' + contentId + '/archive';

		this.http({method: 'PUT', url: url, data : {}, cache:false})
		.then(function(response) {
			if (response.status === 200){ return successFnCallBack(response.data); }
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		})
		.catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.archiveOffer = function(clientId, attractionId, offerId, successFnCallBack, errorFnCallback){
		var scope = this;
		var url = 'jaxrs/v2/clients/' + clientId + '/attractions/' + attractionId + '/offers/' + offerId + '/archive';

		this.http({method: 'PUT', url: url, data : {}, cache:false})
		.then(function(response) {
			if (response.status === 200){ return successFnCallBack(response.data); }
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		})
		.catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishOffer = function(clientId, attractionId, offerId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId + '/attractions/' + attractionId + '/offers/' + offerId + '/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	ce.services.AttractionServices.prototype.publishDocument = function(clientId, collectionName, documentId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/clients/'+ clientId +  '/'+ collectionName + '/documents/' + 
			documentId+'/publish', cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};

	//	End Closure
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.services.RouteServices = function(http, contentServices, pathRelative) {
		ce.services.call(this,http,pathRelative);
		this.contentServices = contentServices;
	};
	ce.services.RouteServices.prototype = new ce.services();


	/**
	*
	*/
	ce.services.RouteServices.prototype.searchMyRoutes = function(userId, clientId, resultSearch, successFnCallBack, errorFnCallback){
		var scope = this;
		this.http({method: 'GET', url: this.pathRelative + 'jaxrs/v2/users/' + userId + '/routes?clientId='+ clientId, withCredentials : true, cache:false})
		.then(function(response) {
			if (response.status === 200){
				var userRoutesURL = response.data;

				var routes = [];
				//the ws return an route ids array, replace the ids for entities
				angular.forEach(userRoutesURL, function(routeUrlRoute, i) {
					var route = null;
					if (angular.isObject(routeUrlRoute)){
						route = routeUrlRoute;
					}else if(angular.isString(routeUrlRoute)){
						var routeId = routeUrlRoute.substring(routeUrlRoute.lastIndexOf('/')+1);
						route = scope.contentServices.findElementInResultSearch('ROUTE', routeId, resultSearch);
					}
					if (route){
						route.route.attractions = scope.contentServices.completeAttractionElements(route.route.attractionsUrls, resultSearch);
						scope.contentServices.setMarkers(route);
						routes.push(route);
					}
				});
				successFnCallBack(routes);
			}else{
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.getRouteEntity = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET', url : 'jaxrs/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.data.statusRequest === 200) {
				successFnCallBack(response.data.result);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.deleteRoute = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/clients/' + clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 204) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.removeRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'DELETE', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.addRouteToUserRoutes = function(routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'PUT', url : 'jaxrs/v2/user/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack();
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.getRouteCE = function(clientId, routeId, successFnCallBack, errorFnCallback){
		this.http({method : 'GET',url : this.pathRelative + 'jaxrs/v2/clients/'+ clientId + '/routes/' + routeId, cache:false})
		.then(function(response) {
			// check if the status code responde is correct
			if (response.status === 200) {
				successFnCallBack(response.data);
			} else {
				ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	/**
	*
	*/
	ce.services.RouteServices.prototype.addRoute = function(clientId, routeJson, translate, successFnCallBack, errorFnCallback){
		var request = 'jaxrs/clients/'+ clientId +'/routes?translate='+translate;
		this.http({method: 'POST', url: request, data:routeJson, cache:false})
		.then(function(response) {
			//check if the status code responde is correct
			if (response.status === 200){
				if (response.data.statusRequest === 200){
					successFnCallBack(response.data.element);
				}else if (response.data.statusRequest === 400){
					errorFnCallback('Errores de validacion');
				}else{
					errorFnCallback('Error creando una ruta');
				}
			}else{
				errorFnCallback('Error creando una ruta');
			}
		}).catch(function(response) {
			ce.services.PortalServices.throwError(errorFnCallback, response.data, response.status);
		});
	};
	//	End Closure
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.security = ce.security || {};

	ce.security.isUserCreatorForClient = function(user, selectedClient){
		var secureInfo = {
			isCreator:false, 
			globalCreator:false, 
			privateCreator:false, 
			publicCreator:false, 
			isAdmin:false,
			loyalty: false,
			loyaltyEmployee: false,
			loyaltyCustomerService:false
		};

		var isCreatorAssignSelectedClient = false;
		//check if thce.securityere is any user logged
		if (user && user.roles && user.clients){
			var roles = user.roles.split('--');
			var clientsAssigned = user.clients.split('--');
			var isPublicCreator = false;
			var isPrivateCreator = false;
			var isAdmin = false;

			//chech the user roles
			angular.forEach(roles, function(rol, i){
				if(rol === 'creator'){
					isPublicCreator = true;
					secureInfo.publicCreator = true;
				}else if (rol === 'privateCreator'){
					isPrivateCreator = true;
					secureInfo.privateCreator = true;
				} else if (rol === 'admin'){
					isAdmin = true;
				} else if (rol === 'loyalty'){
					secureInfo.loyalty = true;
				} else if (rol === 'loyaltyEmployee'){
					secureInfo.loyaltyEmployee = true;
				} else if (rol === 'loyaltyCustomerService'){
					secureInfo.loyaltyCustomerService = true;
				}
			});
			secureInfo.isAdmin = isAdmin;

			if (isPublicCreator || isPrivateCreator){
				//check it's assign to client selected, 0 is global
				angular.forEach(clientsAssigned, function(client, i){
					if(client === '0' && isPublicCreator){
						secureInfo.isCreator = true;
						secureInfo.isAdmin = true;
						secureInfo.globalCreator = true;
					}else if (client === ''+selectedClient.id){
						secureInfo.isCreator = true;
					}
				});
			}
		}
		return secureInfo;
	};


	ce.security.userCanModify = function(isGlobalCreator, publicCreator, privateCreator, userLoged, contentCE){
		if (!contentCE || !userLoged){
			return false;
		}
		//check if it's logged a global creator
		if (isGlobalCreator){
			return true;
		}

		//if the route if public, only content creator can modify element
		if (contentCE.type === 'ROUTE' && !contentCE.route.publicRoute){
			return (contentCE.idUser === userLoged.id);
		}else{
			if (privateCreator){
				if (contentCE.type === 'OFFER'){
					if (contentCE.offer && contentCE.offer.parent && contentCE.offer.parent.idUser){
						return contentCE.offer.parent.idUser === userLoged.id;
					}else{
						return false;
					}
				}else{
					return contentCE.idUser === userLoged.id;
				}
			}else{
				return publicCreator;
			}
		}
	};
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.session = function(){
		//loginData for databinding with login form
		this.loginData = {
			username : null,
			password : null
		};

		this.globalCreator = false;
		this.publicCreator = false;
		this.privateCreator = false;
		this.isCreator = false;

		this.userLoged = null;
		this.isLogged = false;
	};

	ce.session.prototype.login = function(userLoged, selectedClient){
		this.userLoged = userLoged;
		this.isLogged = true;
		if (selectedClient){
			var infoSecurity = ce.security.isUserCreatorForClient(this.userLoged, selectedClient);
			this.globalCreator = infoSecurity.globalCreator;
			this.privateCreator = infoSecurity.privateCreator;
			this.isCreator = infoSecurity.isCreator;
			this.publicCreator = infoSecurity.publicCreator;
		}
	};
	ce.session.prototype.logout = function(){
		document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		this.resetValues();
	};

	ce.session.prototype.resetValues = function(){
		this.loginData = {
			username : null,
			password : null
		};

		this.userLoged = null;
		this.isLogged = false;

		this.globalCreator = false;
		this.publicCreator = false;
		this.privateCreator = false;
		this.isCreator = false;
	};
})(window.ce);

//Closure
(function(ce) {
	'use strict';

	ce.repository = ce.repository || {};

	ce.repository.ContentRepository = function(contentServices, session, routeServices, CEConfiguration) {
		this.contentServices = contentServices;
		this.session = session;
		this.CEConfiguration = CEConfiguration;
		this.routeServices = routeServices;
		this.content = [];
		this.counter = {
			attractions : 0,
			shops: 0,
			activities : 0,
			offers : 0,
			routes : 0,
			today: 0,
			tomorrow: 0,
			thisWeekend : 0,
			thisMonth : 0,
			thisWeek: 0,
			building : 0,
			dynamic: 0
		};
		// Array of user routes
		this.userRoutes = [];
	};


	ce.repository.ContentRepository.prototype.getContent = function(showPrivateContent, showArchivedContent, language, successFnCallBack){
		var self = this;
		if (showPrivateContent && this.session.publicCreator){
			this.contentServices.getPrivateContent(this.CEConfiguration, language, this.session, function(data){
				self.initCounter();
				self.content = data;
			});
		}else if (showArchivedContent && this.session.publicCreator){
			this.contentServices.getArchivedContent(this.CEConfiguration, language, this.session, function(data){
				self.initCounter();
				self.content = data;
			});
		} else{
			this.initCounter();
			this.contentServices.getContent(this.CEConfiguration, language, this.session, this.counter, function(data){
				self.content = data;

				// get the user routes
				self.getUserRoutes(self.CEConfiguration.selectedClient);

				if (successFnCallBack){
					successFnCallBack();
				}
				
			}, function(data){
				console.log('Error searching content');
			});
		}

	};

	ce.repository.ContentRepository.prototype.getUserRoutes = function(selectedClient){
		this.userRoutes = [];
		if (this.session.isLogged){
			var self = this;
			this.routeServices.searchMyRoutes(this.session.userLoged.id, selectedClient.id, this.content, function(routes){
				self.userRoutes = routes;

				angular.forEach(self.userRoutes, function(route, i){
					if (!route.route.publicRoute){
						route.canModify = true;
					}else{
						route.canModify = false;
					}
				});

			}, function(data){
				console.log('Error searching user routes');
			});
		}
	};



	ce.repository.ContentRepository.prototype.initCounter = function(){
		this.counter = {
			attractions : 0,
			shops: 0,
			activities : 0,
			offers : 0,
			routes : 0,
			today: 0,
			tomorrow: 0,
			thisWeekend : 0,
			thisMonth : 0,
			thisWeek: 0,
			building : 0,
			dynamic: 0
		};
	};
})(window.ce);
